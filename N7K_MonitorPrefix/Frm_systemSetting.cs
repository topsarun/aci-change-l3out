﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace N7K_MonitorPrefix
{

    public partial class Frm_systemSetting : Form
    {
        public bool isChange = false;
        public string autoMode = "on";
        public Frm_systemSetting()
        {
            InitializeComponent();
        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void bt_submit_Click(object sender, EventArgs e)
        {
            if ((tb_ipSladin01.Text != "") &&
            (tb_ipSladin02.Text != "") &&
             (tb_ipSniper01.Text != "") &&
             (tb_ipSniper02.Text != "") &&
             (tb_communitySladin01.Text != "") &&
             (tb_communitySladin02.Text != "") &&
             (tb_communitySniper01.Text != "") &&
             (tb_communitySniper02.Text != "") &&

             (tb_sladin01_pod_1.Text != "") &&
             (tb_sladin01_pod_2.Text != "") &&
             (tb_sladin01_path_1.Text != "") &&
             (tb_sladin01_path_2.Text != "") &&
             (tb_sladin01_interface_1.Text != "") &&
             (tb_sladin01_interface_2.Text != "") &&
             (tb_sladin02_pod_1.Text != "") &&
             (tb_sladin02_pod_2.Text != "") &&
             (tb_sladin02_path_1.Text != "") &&
             (tb_sladin02_path_2.Text != "") &&
             (tb_sladin02_interface_1.Text != "") &&
             (tb_sladin02_interface_2.Text != "") &&
             (tb_sniper01_pod_1.Text != "") &&
             (tb_sniper01_pod_2.Text != "") &&
             (tb_sniper01_path_1.Text != "") &&
             (tb_sniper01_path_2.Text != "") &&
             (tb_sniper01_interface_1.Text != "") &&
             (tb_sniper01_interface_2.Text != "") &&
             (tb_sniper02_pod_1.Text != "") &&
             (tb_sniper02_pod_2.Text != "") &&
             (tb_sniper02_path_1.Text != "") &&
             (tb_sniper02_path_2.Text != "") &&
             (tb_sniper02_interface_1.Text != "") &&
             (tb_sniper02_interface_2.Text != "") &&
             (autoMode != "") &&
            (tb_apicIp.Text != "") &&
             (tb_userName.Text != "") &&
             (tb_password.Text != "") &&
             (tb_tenants.Text != "") &&
             (tb_l3BTT.Text != "") &&
             (tb_l3TST.Text != "") &&
             (tb_vrfBTT.Text != "") &&
             (tb_vrfTST.Text != "") &&
             (tb_contractsBTT.Text !="") &&
             (tb_contractsTST.Text != ""))
            {

                WriteXML();
                isChange = true;
                MessageBox.Show("อัพเดทข้อมูลเรียบร้อยแล้วค่ะ");
            
        }
            else {

                MessageBox.Show("กรุณากรอกข้อมูลให้ครบทุกช่องค่ะ");
            }
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }
       // string ipSladin01,string ipSladin02,string ipSniper01,string ipSniper02,string comSladin01,string comSladin02,string comSniper01
        //   ,string comSniper02,string apicIP,string username, string password, string tenants,string l3BTT
        //  ,string l3TST,string vrfBTT,string vrfTST,string contractsBTT,string contractsTST
        private void WriteXML()
        {
            DataSet dsXML = new DataSet();
            dsXML.ReadXml(Application.StartupPath + @"\systemSettings.xml");
            if (dsXML.Tables["Settings"].Rows.Count > 1)
            {
                dsXML.Tables["Settings"].Rows[0]["ipSladin01"] = tb_ipSladin01.Text;
                dsXML.Tables["Settings"].Rows[0]["ipSladin02"] = tb_ipSladin02.Text;
                dsXML.Tables["Settings"].Rows[0]["ipSniper01"] = tb_ipSniper01.Text;
                dsXML.Tables["Settings"].Rows[0]["ipSniper02"] = tb_ipSniper02.Text;
                dsXML.Tables["Settings"].Rows[0]["communitySladin01"] = tb_communitySladin01.Text;
                dsXML.Tables["Settings"].Rows[0]["communitySladin02"] = tb_communitySladin02.Text;
                dsXML.Tables["Settings"].Rows[0]["communitySniper01"] = tb_communitySniper01.Text;
                dsXML.Tables["Settings"].Rows[0]["communitySniper02"] = tb_communitySniper02.Text;

                dsXML.Tables["Settings"].Rows[0]["saladin01pod1"] = tb_sladin01_pod_1.Text;
                dsXML.Tables["Settings"].Rows[0]["saladin01pod2"] = tb_sladin01_pod_2.Text;
                dsXML.Tables["Settings"].Rows[0]["saladin01path1"] = tb_sladin01_path_1.Text;
                dsXML.Tables["Settings"].Rows[0]["saladin01path2"] = tb_sladin01_path_2.Text;
                dsXML.Tables["Settings"].Rows[0]["saladin01interface1"] = tb_sladin01_interface_1.Text;
                dsXML.Tables["Settings"].Rows[0]["saladin01interface2"] = tb_sladin01_interface_2.Text;
                dsXML.Tables["Settings"].Rows[0]["saladin02pod1"] = tb_sladin02_pod_1.Text;
                dsXML.Tables["Settings"].Rows[0]["saladin02pod2"] = tb_sladin02_pod_2.Text;
                dsXML.Tables["Settings"].Rows[0]["saladin02path1"] = tb_sladin02_path_1.Text;
                dsXML.Tables["Settings"].Rows[0]["saladin02path2"] = tb_sladin02_path_2.Text;
                dsXML.Tables["Settings"].Rows[0]["saladin02interface1"] = tb_sladin02_interface_1.Text;
                dsXML.Tables["Settings"].Rows[0]["saladin02interface2"] = tb_sladin02_interface_2.Text;
                dsXML.Tables["Settings"].Rows[0]["sniper01pod1"] = tb_sniper01_pod_1.Text;
                dsXML.Tables["Settings"].Rows[0]["sniper01pod2"] = tb_sniper01_pod_2.Text;
                dsXML.Tables["Settings"].Rows[0]["sniper01path1"] = tb_sniper01_path_1.Text;
                dsXML.Tables["Settings"].Rows[0]["sniper01path2"] = tb_sniper01_path_2.Text;
                dsXML.Tables["Settings"].Rows[0]["sniper01interface1"] = tb_sniper01_interface_1.Text;
                dsXML.Tables["Settings"].Rows[0]["sniper01interface2"] = tb_sniper01_interface_2.Text;
                dsXML.Tables["Settings"].Rows[0]["sniper02pod1"] = tb_sniper02_pod_1.Text;
                dsXML.Tables["Settings"].Rows[0]["sniper02pod2"] = tb_sniper02_pod_2.Text;
                dsXML.Tables["Settings"].Rows[0]["sniper02path1"] = tb_sniper02_path_1.Text;
                dsXML.Tables["Settings"].Rows[0]["sniper02path2"] = tb_sniper02_path_2.Text;
                dsXML.Tables["Settings"].Rows[0]["sniper02interface1"] = tb_sniper02_interface_1.Text;
                dsXML.Tables["Settings"].Rows[0]["sniper02interface2"] = tb_sniper02_interface_2.Text;

                dsXML.Tables["Settings"].Rows[0]["automode"] = autoMode;

                dsXML.Tables["Settings"].Rows[0]["apicIp"] = tb_apicIp.Text;
                dsXML.Tables["Settings"].Rows[0]["username"] = tb_userName.Text;
                dsXML.Tables["Settings"].Rows[0]["password"] = tb_password.Text;
                dsXML.Tables["Settings"].Rows[0]["tenants"] = tb_tenants.Text;
                dsXML.Tables["Settings"].Rows[0]["l3BTT"] = tb_l3BTT.Text;
                dsXML.Tables["Settings"].Rows[0]["l3TST"] = tb_l3TST.Text;
                dsXML.Tables["Settings"].Rows[0]["vrfBTT"] = tb_vrfBTT.Text;
                dsXML.Tables["Settings"].Rows[0]["vrfTST"] = tb_vrfTST.Text;
                dsXML.Tables["Settings"].Rows[0]["contractsBTT"] = tb_contractsBTT.Text;
                dsXML.Tables["Settings"].Rows[0]["contractsTST"] = tb_contractsTST.Text;
            }
            else {
                dsXML.Tables["Settings"].Rows.Add(tb_ipSladin01.Text, tb_ipSladin02.Text, tb_ipSniper01.Text, tb_ipSniper02.Text
                    , tb_communitySladin01.Text , tb_communitySladin02.Text , tb_communitySniper01.Text , tb_communitySniper02.Text
                    , tb_sladin01_pod_1.Text , tb_sladin01_pod_2.Text , tb_sladin01_path_1.Text , tb_sladin01_path_2.Text
                    , tb_sladin01_interface_1.Text , tb_sladin01_interface_2.Text , tb_sladin02_pod_1.Text , tb_sladin02_pod_2.Text
                    , tb_sladin02_path_1.Text, tb_sladin02_path_2.Text , tb_sladin02_interface_1.Text , tb_sladin02_interface_2.Text
                    , tb_sniper01_pod_1.Text , tb_sniper01_pod_2.Text , tb_sniper01_path_1.Text , tb_sniper01_path_2.Text
                    , tb_sniper01_interface_1.Text, tb_sniper01_interface_2.Text, tb_sniper02_pod_1.Text, tb_sniper02_pod_2.Text
                    , tb_sniper02_path_1.Text, tb_sniper02_path_2.Text, tb_sniper02_interface_1.Text, tb_sniper02_interface_2.Text , autoMode
                    , tb_apicIp.Text , tb_userName.Text , tb_password.Text , tb_tenants.Text , tb_l3TST.Text , tb_l3BTT.Text , tb_vrfTST.Text , tb_vrfBTT.Text
                    , tb_contractsTST.Text, tb_contractsBTT);
            }
            dsXML.WriteXml(Application.StartupPath + @"\systemSettings.xml");
            dsXML.Dispose();
        }
        private void Frm_systemSetting_Load(object sender, EventArgs e)
        {

            DataSet dsXML = new DataSet();
            dsXML.ReadXml(Application.StartupPath + @"\systemSettings.xml");
            //string[,] prefixList = new string[dsXML.Tables["Prefix"].Rows.Count, 2];
            if (dsXML.Tables.Count > 0)
            {
                if (dsXML.Tables["Settings"].Rows.Count > 0)
                {
                    tb_ipSladin01.Text = dsXML.Tables["Settings"].Rows[0]["ipSladin01"].ToString();
                    tb_ipSladin02.Text = dsXML.Tables["Settings"].Rows[0]["ipSladin02"].ToString();
                    tb_ipSniper01.Text = dsXML.Tables["Settings"].Rows[0]["ipSniper01"].ToString();
                    tb_ipSniper02.Text = dsXML.Tables["Settings"].Rows[0]["ipSniper02"].ToString();
                    tb_communitySladin01.Text = dsXML.Tables["Settings"].Rows[0]["communitySladin01"].ToString();
                    tb_communitySladin02.Text = dsXML.Tables["Settings"].Rows[0]["communitySladin02"].ToString();
                    tb_communitySniper01.Text = dsXML.Tables["Settings"].Rows[0]["communitySniper01"].ToString();
                    tb_communitySniper02.Text = dsXML.Tables["Settings"].Rows[0]["communitySniper02"].ToString();

                    tb_sladin01_pod_1.Text = dsXML.Tables["Settings"].Rows[0]["saladin01pod1"].ToString();
                    tb_sladin01_pod_2.Text = dsXML.Tables["Settings"].Rows[0]["saladin01pod2"].ToString();
                    tb_sladin01_path_1.Text = dsXML.Tables["Settings"].Rows[0]["saladin01path1"].ToString();
                    tb_sladin01_path_2.Text = dsXML.Tables["Settings"].Rows[0]["saladin01path2"].ToString();
                    tb_sladin01_interface_1.Text = dsXML.Tables["Settings"].Rows[0]["saladin01interface1"].ToString();
                    tb_sladin01_interface_2.Text = dsXML.Tables["Settings"].Rows[0]["saladin01interface2"].ToString();
                    tb_sladin02_pod_1.Text = dsXML.Tables["Settings"].Rows[0]["saladin02pod1"].ToString();
                    tb_sladin02_pod_2.Text = dsXML.Tables["Settings"].Rows[0]["saladin02pod2"].ToString();
                    tb_sladin02_path_1.Text = dsXML.Tables["Settings"].Rows[0]["saladin02path1"].ToString();
                    tb_sladin02_path_2.Text = dsXML.Tables["Settings"].Rows[0]["saladin02path2"].ToString();
                    tb_sladin02_interface_1.Text = dsXML.Tables["Settings"].Rows[0]["saladin02interface1"].ToString();
                    tb_sladin02_interface_2.Text = dsXML.Tables["Settings"].Rows[0]["saladin02interface2"].ToString();
                    tb_sniper01_pod_1.Text = dsXML.Tables["Settings"].Rows[0]["sniper01pod1"].ToString();
                    tb_sniper01_pod_2.Text = dsXML.Tables["Settings"].Rows[0]["sniper01pod2"].ToString();
                    tb_sniper01_path_1.Text = dsXML.Tables["Settings"].Rows[0]["sniper01path1"].ToString();
                    tb_sniper01_path_2.Text = dsXML.Tables["Settings"].Rows[0]["sniper01path2"].ToString();
                    tb_sniper01_interface_1.Text = dsXML.Tables["Settings"].Rows[0]["sniper01interface1"].ToString();
                    tb_sniper01_interface_2.Text = dsXML.Tables["Settings"].Rows[0]["sniper01interface2"].ToString();
                    tb_sniper02_pod_1.Text = dsXML.Tables["Settings"].Rows[0]["sniper02pod1"].ToString();
                    tb_sniper02_pod_2.Text = dsXML.Tables["Settings"].Rows[0]["sniper02pod2"].ToString();
                    tb_sniper02_path_1.Text = dsXML.Tables["Settings"].Rows[0]["sniper02path1"].ToString();
                    tb_sniper02_path_2.Text = dsXML.Tables["Settings"].Rows[0]["sniper02path2"].ToString();
                    tb_sniper02_interface_1.Text = dsXML.Tables["Settings"].Rows[0]["sniper02interface1"].ToString();
                    tb_sniper02_interface_2.Text = dsXML.Tables["Settings"].Rows[0]["sniper02interface2"].ToString();

                    if (dsXML.Tables["Settings"].Rows[0]["automode"].ToString() == "on")
                    {
                        checkBox1.Checked = true;
                        checkBox1.Text = "AUTO MODE ON";
                    }
                    else
                    {
                        checkBox1.Checked = false;
                        checkBox1.Text = "AUTO MODE OFF";
                    }

                    tb_apicIp.Text= dsXML.Tables["Settings"].Rows[0]["apicIp"].ToString();
                    tb_userName.Text = dsXML.Tables["Settings"].Rows[0]["username"].ToString();
                    tb_password.Text = dsXML.Tables["Settings"].Rows[0]["password"].ToString();
                    tb_tenants.Text = dsXML.Tables["Settings"].Rows[0]["tenants"].ToString();
                    tb_l3BTT.Text = dsXML.Tables["Settings"].Rows[0]["l3BTT"].ToString();
                    tb_l3TST.Text = dsXML.Tables["Settings"].Rows[0]["l3TST"].ToString();
                    tb_vrfBTT.Text = dsXML.Tables["Settings"].Rows[0]["vrfBTT"].ToString();
                    tb_vrfTST.Text = dsXML.Tables["Settings"].Rows[0]["vrfTST"].ToString();
                    tb_contractsBTT.Text = dsXML.Tables["Settings"].Rows[0]["contractsBTT"].ToString();
                    tb_contractsTST.Text = dsXML.Tables["Settings"].Rows[0]["contractsTST"].ToString();
                }
            }
            dsXML.Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Size = new Size(1314,760);
            bt_submit.Location = new Point(1211,688);
            button1.Visible = false;
            button2.Visible = true;
            panel4.Visible = true;
            panel6.Visible = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Size = new Size(633, 760);
            bt_submit.Location = new Point(503, 688);
            button1.Visible = true;
            button2.Visible = false;
            panel4.Visible = false;
            panel6.Visible = false;
        }
        
        private void checkBox1_CheckedChanged_1(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                Console.WriteLine("on");
                checkBox1.Text = "AUTO MODE ON";
                autoMode = "on";
                
            }
            else
            {
                Console.WriteLine("off");
                checkBox1.Text = "AUTO MODE OFF";
                autoMode = "off";
            }
        }
    }
}
