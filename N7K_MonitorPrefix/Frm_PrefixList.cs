﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
namespace N7K_MonitorPrefix
{
    public partial class Frm_PrefixList : Form
    {
        public Frm_PrefixList()
        {
         
            InitializeComponent();
            dtg_prefixList.ColumnCount = 2;
            dtg_prefixList.Columns[0].Name = "Customer Name";
            dtg_prefixList.Columns[1].Name = "Prefix";
        }

        public bool DeletePrefix(string prefix)
        {
            bool canDel = false;
            XmlDocument doc = new XmlDocument();
            doc.Load(Application.StartupPath + @"\PrefixList.xml");

       //     XmlNode node = doc.SelectSingleNode("/NewDataSet/Prefix[@PrefixID='"+prefix+"']");
            XmlElement el = (XmlElement)doc.SelectSingleNode("/NewDataSet/Prefix[PrefixID='" + prefix + "']");
            
            if (el != null)
            {
                el.ParentNode.RemoveChild(el);
                doc.Save(Application.StartupPath + @"\PrefixList.xml");
            }
            
           
            return canDel;

        }

        public string[,] GetXmlPrefixList() {
            
            DataSet dsXML = new DataSet();
            dsXML.ReadXml(Application.StartupPath + @"\PrefixList.xml");
            string[,] prefixList = new string[dsXML.Tables["Prefix"].Rows.Count,2];
            
            for (int i = 0; i < dsXML.Tables["Prefix"].Rows.Count; i++)
            {
                prefixList[i, 0] = dsXML.Tables["Prefix"].Rows[i]["Customer_Name"].ToString();
                prefixList[i, 1] = dsXML.Tables["Prefix"].Rows[i]["Prefix"].ToString();
            }
            return prefixList;
        }
        public void UpdatePrefixList()
        {

            DataSet dsXML = new DataSet();
            dsXML.ReadXml(Application.StartupPath + @"\PrefixList.xml");
            dtg_prefixList.Rows.Clear();
            //    MessageBox.Show(dsXML.Tables["Prefix"].Rows[0]["Customer_Name"].ToString());
            if (dsXML.Tables.Count > 0)
            {
                for (int i = 0; i < dsXML.Tables["Prefix"].Rows.Count; i++)
                {
                    dtg_prefixList.Rows.Add(dsXML.Tables["Prefix"].Rows[i]["CustomerName"].ToString(), dsXML.Tables["Prefix"].Rows[i]["PrefixID"].ToString());

                }
            }
            dsXML.Dispose();
        }

        public string GetXmlPrefix(int row)
        {
            DataSet dsXML = new DataSet();
            dsXML.ReadXml(Application.StartupPath + @"\PrefixList.xml");
            return dsXML.Tables["Prefix"].Rows[row]["Prefix"].ToString();
        }

        public int GetXmlCustomerName(int row)
        {
            DataSet dsXML = new DataSet();
            dsXML.ReadXml(Application.StartupPath + @"\PrefixList.xml");
            return Convert.ToInt32(dsXML.Tables["Prefix"].Rows[row]["CustomerName"].ToString());
        }

        public void EditXmlPrefix(string CustomerName, string Prefix, int row)
        {
            DataSet dsXML = new DataSet();
            dsXML.ReadXml(Application.StartupPath + @"\PrefixList.xml");
            dsXML.Tables["Prefix"].Rows[row]["PrefixID"] = Prefix;
            dsXML.Tables["Prefix"].Rows[row]["CustomerName"] = CustomerName;
            dsXML.WriteXml(Application.StartupPath + @"\PrefixList.xml");

            return;
        }

        public void AddXmlPrefix(string CustomerName, string Prefix)
        {
            DataSet dsXML = new DataSet();
            dsXML.ReadXml(Application.StartupPath + @"\PrefixList.xml");
            dsXML.Tables["PrefixID"].Rows.Add(CustomerName, Prefix);
            dsXML.WriteXml(Application.StartupPath + @"\PrefixList.xml");
        }

        private void Frm_addPrefix_Load(object sender, EventArgs e)
        {

            UpdatePrefixList();

        }

        private void bt_add_Click(object sender, EventArgs e)
        {
            Frm_AddPrefix frm_addPrefix = new Frm_AddPrefix();
            frm_addPrefix.ShowDialog();
            UpdatePrefixList();

        }

        private void bt_edit_Click(object sender, EventArgs e)
        {
            if (dtg_prefixList.SelectedRows != null)
            {
               bool canDel = DeletePrefix(dtg_prefixList.SelectedRows[0].Cells[1].Value.ToString());
                if (canDel) {
                    MessageBox.Show("ลบข้อมูลสำเร็จ");
                    
                }
                UpdatePrefixList();

            }
            else {
                MessageBox.Show("กรุณาเลือกข้อมูลที่ต้องการจะลบด้วยค่ะ!");
        }
        }
    }
}
