﻿namespace N7K_MonitorPrefix
{
    partial class Frm_systemSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.tb_communitySniper02 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.tb_communitySniper01 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tb_communitySladin02 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tb_communitySladin01 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tb_ipSniper02 = new System.Windows.Forms.TextBox();
            this.tb_ipSniper01 = new System.Windows.Forms.TextBox();
            this.tb_ipSladin02 = new System.Windows.Forms.TextBox();
            this.tb_ipSladin01 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.tb_contractsBTT = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.tb_contractsTST = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.tb_vrfBTT = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.tb_vrfTST = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.tb_l3BTT = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.tb_l3TST = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.tb_tenants = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.tb_password = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.tb_userName = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.tb_apicIp = new System.Windows.Forms.TextBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.bt_submit = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label22 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label35 = new System.Windows.Forms.Label();
            this.tb_sniper02_interface_2 = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.tb_sniper02_path_2 = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.tb_sniper02_pod_2 = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label29 = new System.Windows.Forms.Label();
            this.tb_sniper02_interface_1 = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.tb_sniper02_path_1 = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.tb_sniper02_pod_1 = new System.Windows.Forms.TextBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.label38 = new System.Windows.Forms.Label();
            this.tb_sniper01_interface_2 = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.tb_sniper01_path_2 = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.tb_sniper01_pod_2 = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label26 = new System.Windows.Forms.Label();
            this.tb_sniper01_interface_1 = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.tb_sniper01_path_1 = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.tb_sniper01_pod_1 = new System.Windows.Forms.TextBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.label41 = new System.Windows.Forms.Label();
            this.tb_sladin02_interface_2 = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.tb_sladin02_path_2 = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.tb_sladin02_pod_2 = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label23 = new System.Windows.Forms.Label();
            this.tb_sladin02_interface_1 = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.tb_sladin02_path_1 = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.tb_sladin02_pod_1 = new System.Windows.Forms.TextBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.label44 = new System.Windows.Forms.Label();
            this.tb_sladin01_interface_2 = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.tb_sladin01_path_2 = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.tb_sladin01_pod_2 = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label30 = new System.Windows.Forms.Label();
            this.tb_sladin01_interface_1 = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.tb_sladin01_path_1 = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.tb_sladin01_pod_1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.AliceBlue;
            this.panel1.Controls.Add(this.tb_communitySniper02);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.tb_communitySniper01);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.tb_communitySladin02);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.tb_communitySladin01);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.tb_ipSniper02);
            this.panel1.Controls.Add(this.tb_ipSniper01);
            this.panel1.Controls.Add(this.tb_ipSladin02);
            this.panel1.Controls.Add(this.tb_ipSladin01);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Location = new System.Drawing.Point(18, 77);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(560, 162);
            this.panel1.TabIndex = 0;
            // 
            // tb_communitySniper02
            // 
            this.tb_communitySniper02.Location = new System.Drawing.Point(377, 115);
            this.tb_communitySniper02.Name = "tb_communitySniper02";
            this.tb_communitySniper02.Size = new System.Drawing.Size(156, 20);
            this.tb_communitySniper02.TabIndex = 15;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(280, 118);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(91, 13);
            this.label11.TabIndex = 14;
            this.label11.Text = "Community String:";
            // 
            // tb_communitySniper01
            // 
            this.tb_communitySniper01.Location = new System.Drawing.Point(377, 85);
            this.tb_communitySniper01.Name = "tb_communitySniper01";
            this.tb_communitySniper01.Size = new System.Drawing.Size(156, 20);
            this.tb_communitySniper01.TabIndex = 13;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(280, 88);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(91, 13);
            this.label10.TabIndex = 12;
            this.label10.Text = "Community String:";
            // 
            // tb_communitySladin02
            // 
            this.tb_communitySladin02.Location = new System.Drawing.Point(377, 55);
            this.tb_communitySladin02.Name = "tb_communitySladin02";
            this.tb_communitySladin02.Size = new System.Drawing.Size(156, 20);
            this.tb_communitySladin02.TabIndex = 11;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(280, 58);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(91, 13);
            this.label9.TabIndex = 10;
            this.label9.Text = "Community String:";
            // 
            // tb_communitySladin01
            // 
            this.tb_communitySladin01.Location = new System.Drawing.Point(377, 24);
            this.tb_communitySladin01.Name = "tb_communitySladin01";
            this.tb_communitySladin01.Size = new System.Drawing.Size(156, 20);
            this.tb_communitySladin01.TabIndex = 9;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(280, 27);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(91, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Community String:";
            // 
            // tb_ipSniper02
            // 
            this.tb_ipSniper02.Location = new System.Drawing.Point(104, 115);
            this.tb_ipSniper02.Name = "tb_ipSniper02";
            this.tb_ipSniper02.Size = new System.Drawing.Size(156, 20);
            this.tb_ipSniper02.TabIndex = 7;
            // 
            // tb_ipSniper01
            // 
            this.tb_ipSniper01.Location = new System.Drawing.Point(104, 85);
            this.tb_ipSniper01.Name = "tb_ipSniper01";
            this.tb_ipSniper01.Size = new System.Drawing.Size(156, 20);
            this.tb_ipSniper01.TabIndex = 6;
            // 
            // tb_ipSladin02
            // 
            this.tb_ipSladin02.Location = new System.Drawing.Point(104, 55);
            this.tb_ipSladin02.Name = "tb_ipSladin02";
            this.tb_ipSladin02.Size = new System.Drawing.Size(156, 20);
            this.tb_ipSladin02.TabIndex = 5;
            // 
            // tb_ipSladin01
            // 
            this.tb_ipSladin01.Location = new System.Drawing.Point(104, 25);
            this.tb_ipSladin01.Name = "tb_ipSladin01";
            this.tb_ipSladin01.Size = new System.Drawing.Size(156, 20);
            this.tb_ipSladin01.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(28, 118);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 13);
            this.label7.TabIndex = 3;
            this.label7.Text = "IP (Sniper02):";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(28, 88);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "IP (Sniper01):";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(28, 58);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "IP (Sladin02):";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(28, 27);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "IP (Sladin01):";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.AliceBlue;
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Location = new System.Drawing.Point(18, 270);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(560, 412);
            this.panel2.TabIndex = 1;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.tb_contractsBTT);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.tb_contractsTST);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.tb_vrfBTT);
            this.groupBox2.Controls.Add(this.label21);
            this.groupBox2.Controls.Add(this.tb_vrfTST);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.tb_l3BTT);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.tb_l3TST);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.tb_tenants);
            this.groupBox2.Location = new System.Drawing.Point(25, 154);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(508, 248);
            this.groupBox2.TabIndex = 16;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Properties";
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(32, 215);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(88, 13);
            this.label12.TabIndex = 22;
            this.label12.Text = "Contracts (BTT) :";
            // 
            // tb_contractsBTT
            // 
            this.tb_contractsBTT.Location = new System.Drawing.Point(127, 212);
            this.tb_contractsBTT.Name = "tb_contractsBTT";
            this.tb_contractsBTT.Size = new System.Drawing.Size(324, 20);
            this.tb_contractsBTT.TabIndex = 21;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(32, 125);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(64, 13);
            this.label19.TabIndex = 15;
            this.label19.Text = "VRF (TST) :";
            // 
            // tb_contractsTST
            // 
            this.tb_contractsTST.Location = new System.Drawing.Point(126, 182);
            this.tb_contractsTST.Name = "tb_contractsTST";
            this.tb_contractsTST.Size = new System.Drawing.Size(324, 20);
            this.tb_contractsTST.TabIndex = 20;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(32, 155);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(64, 13);
            this.label20.TabIndex = 16;
            this.label20.Text = "VRF (BTT) :";
            // 
            // tb_vrfBTT
            // 
            this.tb_vrfBTT.Location = new System.Drawing.Point(126, 152);
            this.tb_vrfBTT.Name = "tb_vrfBTT";
            this.tb_vrfBTT.Size = new System.Drawing.Size(324, 20);
            this.tb_vrfBTT.TabIndex = 19;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(32, 185);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(88, 13);
            this.label21.TabIndex = 17;
            this.label21.Text = "Contracts (TST) :";
            // 
            // tb_vrfTST
            // 
            this.tb_vrfTST.Location = new System.Drawing.Point(126, 122);
            this.tb_vrfTST.Name = "tb_vrfTST";
            this.tb_vrfTST.Size = new System.Drawing.Size(324, 20);
            this.tb_vrfTST.TabIndex = 18;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(32, 35);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(52, 13);
            this.label16.TabIndex = 8;
            this.label16.Text = "Tenants :";
            // 
            // tb_l3BTT
            // 
            this.tb_l3BTT.Location = new System.Drawing.Point(126, 92);
            this.tb_l3BTT.Name = "tb_l3BTT";
            this.tb_l3BTT.Size = new System.Drawing.Size(324, 20);
            this.tb_l3BTT.TabIndex = 14;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(32, 65);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(72, 13);
            this.label17.TabIndex = 9;
            this.label17.Text = "L3Out (TST) :";
            // 
            // tb_l3TST
            // 
            this.tb_l3TST.Location = new System.Drawing.Point(126, 62);
            this.tb_l3TST.Name = "tb_l3TST";
            this.tb_l3TST.Size = new System.Drawing.Size(324, 20);
            this.tb_l3TST.TabIndex = 13;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(32, 95);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(72, 13);
            this.label18.TabIndex = 10;
            this.label18.Text = "L3Out (BTT) :";
            // 
            // tb_tenants
            // 
            this.tb_tenants.Location = new System.Drawing.Point(126, 32);
            this.tb_tenants.Name = "tb_tenants";
            this.tb_tenants.Size = new System.Drawing.Size(324, 20);
            this.tb_tenants.TabIndex = 12;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.tb_password);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.tb_userName);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.tb_apicIp);
            this.groupBox1.Location = new System.Drawing.Point(25, 14);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(508, 134);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Connection";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(32, 36);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(50, 13);
            this.label15.TabIndex = 8;
            this.label15.Text = "APIC IP :";
            // 
            // tb_password
            // 
            this.tb_password.Location = new System.Drawing.Point(126, 98);
            this.tb_password.Name = "tb_password";
            this.tb_password.PasswordChar = '*';
            this.tb_password.Size = new System.Drawing.Size(324, 20);
            this.tb_password.TabIndex = 14;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(32, 69);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(61, 13);
            this.label14.TabIndex = 9;
            this.label14.Text = "Username :";
            // 
            // tb_userName
            // 
            this.tb_userName.Location = new System.Drawing.Point(126, 66);
            this.tb_userName.Name = "tb_userName";
            this.tb_userName.Size = new System.Drawing.Size(324, 20);
            this.tb_userName.TabIndex = 13;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(32, 101);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(59, 13);
            this.label13.TabIndex = 10;
            this.label13.Text = "Password :";
            // 
            // tb_apicIp
            // 
            this.tb_apicIp.Location = new System.Drawing.Point(126, 33);
            this.tb_apicIp.Name = "tb_apicIp";
            this.tb_apicIp.Size = new System.Drawing.Size(324, 20);
            this.tb_apicIp.TabIndex = 12;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.DimGray;
            this.panel5.Controls.Add(this.label3);
            this.panel5.Location = new System.Drawing.Point(18, 248);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(560, 28);
            this.panel5.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(9, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "ACI Settings";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.DimGray;
            this.panel3.Controls.Add(this.label2);
            this.panel3.Location = new System.Drawing.Point(18, 52);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(560, 25);
            this.panel3.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(9, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "N7K Settings";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(19, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(143, 25);
            this.label1.TabIndex = 4;
            this.label1.Text = "System Settings";
            // 
            // bt_submit
            // 
            this.bt_submit.BackColor = System.Drawing.Color.AliceBlue;
            this.bt_submit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_submit.Location = new System.Drawing.Point(503, 688);
            this.bt_submit.Name = "bt_submit";
            this.bt_submit.Size = new System.Drawing.Size(75, 23);
            this.bt_submit.TabIndex = 16;
            this.bt_submit.Text = "Submit";
            this.bt_submit.UseVisualStyleBackColor = false;
            this.bt_submit.Click += new System.EventHandler(this.bt_submit_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.DimGray;
            this.panel4.Controls.Add(this.label22);
            this.panel4.Location = new System.Drawing.Point(617, 52);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(669, 28);
            this.panel4.TabIndex = 7;
            this.panel4.Visible = false;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.ForeColor = System.Drawing.Color.White;
            this.label22.Location = new System.Drawing.Point(9, 7);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(76, 13);
            this.label22.TabIndex = 6;
            this.label22.Text = "Fablic Settings";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.AliceBlue;
            this.panel6.Controls.Add(this.groupBox7);
            this.panel6.Controls.Add(this.groupBox6);
            this.panel6.Controls.Add(this.groupBox8);
            this.panel6.Controls.Add(this.groupBox5);
            this.panel6.Controls.Add(this.groupBox9);
            this.panel6.Controls.Add(this.groupBox3);
            this.panel6.Controls.Add(this.groupBox10);
            this.panel6.Controls.Add(this.groupBox4);
            this.panel6.Location = new System.Drawing.Point(617, 77);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(669, 605);
            this.panel6.TabIndex = 17;
            this.panel6.Visible = false;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.label35);
            this.groupBox7.Controls.Add(this.tb_sniper02_interface_2);
            this.groupBox7.Controls.Add(this.label36);
            this.groupBox7.Controls.Add(this.tb_sniper02_path_2);
            this.groupBox7.Controls.Add(this.label37);
            this.groupBox7.Controls.Add(this.tb_sniper02_pod_2);
            this.groupBox7.Location = new System.Drawing.Point(362, 448);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(292, 134);
            this.groupBox7.TabIndex = 18;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Sniper 02";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(32, 36);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(36, 13);
            this.label35.TabIndex = 8;
            this.label35.Text = "POD :";
            // 
            // tb_sniper02_interface_2
            // 
            this.tb_sniper02_interface_2.Location = new System.Drawing.Point(126, 98);
            this.tb_sniper02_interface_2.Name = "tb_sniper02_interface_2";
            this.tb_sniper02_interface_2.Size = new System.Drawing.Size(160, 20);
            this.tb_sniper02_interface_2.TabIndex = 14;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(32, 69);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(35, 13);
            this.label36.TabIndex = 9;
            this.label36.Text = "Path :";
            // 
            // tb_sniper02_path_2
            // 
            this.tb_sniper02_path_2.Location = new System.Drawing.Point(126, 66);
            this.tb_sniper02_path_2.Name = "tb_sniper02_path_2";
            this.tb_sniper02_path_2.Size = new System.Drawing.Size(160, 20);
            this.tb_sniper02_path_2.TabIndex = 13;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(32, 101);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(55, 13);
            this.label37.TabIndex = 10;
            this.label37.Text = "Interface :";
            // 
            // tb_sniper02_pod_2
            // 
            this.tb_sniper02_pod_2.Location = new System.Drawing.Point(126, 33);
            this.tb_sniper02_pod_2.Name = "tb_sniper02_pod_2";
            this.tb_sniper02_pod_2.Size = new System.Drawing.Size(160, 20);
            this.tb_sniper02_pod_2.TabIndex = 12;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label29);
            this.groupBox6.Controls.Add(this.tb_sniper02_interface_1);
            this.groupBox6.Controls.Add(this.label33);
            this.groupBox6.Controls.Add(this.tb_sniper02_path_1);
            this.groupBox6.Controls.Add(this.label34);
            this.groupBox6.Controls.Add(this.tb_sniper02_pod_1);
            this.groupBox6.Location = new System.Drawing.Point(25, 448);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(292, 134);
            this.groupBox6.TabIndex = 16;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Sniper 01";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(32, 36);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(36, 13);
            this.label29.TabIndex = 8;
            this.label29.Text = "POD :";
            // 
            // tb_sniper02_interface_1
            // 
            this.tb_sniper02_interface_1.Location = new System.Drawing.Point(126, 98);
            this.tb_sniper02_interface_1.Name = "tb_sniper02_interface_1";
            this.tb_sniper02_interface_1.Size = new System.Drawing.Size(160, 20);
            this.tb_sniper02_interface_1.TabIndex = 14;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(32, 69);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(35, 13);
            this.label33.TabIndex = 9;
            this.label33.Text = "Path :";
            // 
            // tb_sniper02_path_1
            // 
            this.tb_sniper02_path_1.Location = new System.Drawing.Point(126, 66);
            this.tb_sniper02_path_1.Name = "tb_sniper02_path_1";
            this.tb_sniper02_path_1.Size = new System.Drawing.Size(160, 20);
            this.tb_sniper02_path_1.TabIndex = 13;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(32, 101);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(55, 13);
            this.label34.TabIndex = 10;
            this.label34.Text = "Interface :";
            // 
            // tb_sniper02_pod_1
            // 
            this.tb_sniper02_pod_1.Location = new System.Drawing.Point(126, 33);
            this.tb_sniper02_pod_1.Name = "tb_sniper02_pod_1";
            this.tb_sniper02_pod_1.Size = new System.Drawing.Size(160, 20);
            this.tb_sniper02_pod_1.TabIndex = 12;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.label38);
            this.groupBox8.Controls.Add(this.tb_sniper01_interface_2);
            this.groupBox8.Controls.Add(this.label39);
            this.groupBox8.Controls.Add(this.tb_sniper01_path_2);
            this.groupBox8.Controls.Add(this.label40);
            this.groupBox8.Controls.Add(this.tb_sniper01_pod_2);
            this.groupBox8.Location = new System.Drawing.Point(362, 308);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(292, 134);
            this.groupBox8.TabIndex = 19;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Sniper 01";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(32, 36);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(36, 13);
            this.label38.TabIndex = 8;
            this.label38.Text = "POD :";
            // 
            // tb_sniper01_interface_2
            // 
            this.tb_sniper01_interface_2.Location = new System.Drawing.Point(126, 98);
            this.tb_sniper01_interface_2.Name = "tb_sniper01_interface_2";
            this.tb_sniper01_interface_2.Size = new System.Drawing.Size(160, 20);
            this.tb_sniper01_interface_2.TabIndex = 14;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(32, 69);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(35, 13);
            this.label39.TabIndex = 9;
            this.label39.Text = "Path :";
            // 
            // tb_sniper01_path_2
            // 
            this.tb_sniper01_path_2.Location = new System.Drawing.Point(126, 66);
            this.tb_sniper01_path_2.Name = "tb_sniper01_path_2";
            this.tb_sniper01_path_2.Size = new System.Drawing.Size(160, 20);
            this.tb_sniper01_path_2.TabIndex = 13;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(32, 101);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(55, 13);
            this.label40.TabIndex = 10;
            this.label40.Text = "Interface :";
            // 
            // tb_sniper01_pod_2
            // 
            this.tb_sniper01_pod_2.Location = new System.Drawing.Point(126, 33);
            this.tb_sniper01_pod_2.Name = "tb_sniper01_pod_2";
            this.tb_sniper01_pod_2.Size = new System.Drawing.Size(160, 20);
            this.tb_sniper01_pod_2.TabIndex = 12;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label26);
            this.groupBox5.Controls.Add(this.tb_sniper01_interface_1);
            this.groupBox5.Controls.Add(this.label27);
            this.groupBox5.Controls.Add(this.tb_sniper01_path_1);
            this.groupBox5.Controls.Add(this.label28);
            this.groupBox5.Controls.Add(this.tb_sniper01_pod_1);
            this.groupBox5.Location = new System.Drawing.Point(25, 308);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(292, 134);
            this.groupBox5.TabIndex = 16;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Sniper 01";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(32, 36);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(36, 13);
            this.label26.TabIndex = 8;
            this.label26.Text = "POD :";
            // 
            // tb_sniper01_interface_1
            // 
            this.tb_sniper01_interface_1.Location = new System.Drawing.Point(126, 98);
            this.tb_sniper01_interface_1.Name = "tb_sniper01_interface_1";
            this.tb_sniper01_interface_1.Size = new System.Drawing.Size(160, 20);
            this.tb_sniper01_interface_1.TabIndex = 14;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(32, 69);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(35, 13);
            this.label27.TabIndex = 9;
            this.label27.Text = "Path :";
            // 
            // tb_sniper01_path_1
            // 
            this.tb_sniper01_path_1.Location = new System.Drawing.Point(126, 66);
            this.tb_sniper01_path_1.Name = "tb_sniper01_path_1";
            this.tb_sniper01_path_1.Size = new System.Drawing.Size(160, 20);
            this.tb_sniper01_path_1.TabIndex = 13;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(32, 101);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(55, 13);
            this.label28.TabIndex = 10;
            this.label28.Text = "Interface :";
            // 
            // tb_sniper01_pod_1
            // 
            this.tb_sniper01_pod_1.Location = new System.Drawing.Point(126, 33);
            this.tb_sniper01_pod_1.Name = "tb_sniper01_pod_1";
            this.tb_sniper01_pod_1.Size = new System.Drawing.Size(160, 20);
            this.tb_sniper01_pod_1.TabIndex = 12;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.label41);
            this.groupBox9.Controls.Add(this.tb_sladin02_interface_2);
            this.groupBox9.Controls.Add(this.label42);
            this.groupBox9.Controls.Add(this.tb_sladin02_path_2);
            this.groupBox9.Controls.Add(this.label43);
            this.groupBox9.Controls.Add(this.tb_sladin02_pod_2);
            this.groupBox9.Location = new System.Drawing.Point(362, 159);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(292, 134);
            this.groupBox9.TabIndex = 20;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Sladin 02";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(32, 36);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(36, 13);
            this.label41.TabIndex = 8;
            this.label41.Text = "POD :";
            // 
            // tb_sladin02_interface_2
            // 
            this.tb_sladin02_interface_2.Location = new System.Drawing.Point(126, 98);
            this.tb_sladin02_interface_2.Name = "tb_sladin02_interface_2";
            this.tb_sladin02_interface_2.Size = new System.Drawing.Size(160, 20);
            this.tb_sladin02_interface_2.TabIndex = 14;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(32, 69);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(35, 13);
            this.label42.TabIndex = 9;
            this.label42.Text = "Path :";
            // 
            // tb_sladin02_path_2
            // 
            this.tb_sladin02_path_2.Location = new System.Drawing.Point(126, 66);
            this.tb_sladin02_path_2.Name = "tb_sladin02_path_2";
            this.tb_sladin02_path_2.Size = new System.Drawing.Size(160, 20);
            this.tb_sladin02_path_2.TabIndex = 13;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(32, 101);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(55, 13);
            this.label43.TabIndex = 10;
            this.label43.Text = "Interface :";
            // 
            // tb_sladin02_pod_2
            // 
            this.tb_sladin02_pod_2.Location = new System.Drawing.Point(126, 33);
            this.tb_sladin02_pod_2.Name = "tb_sladin02_pod_2";
            this.tb_sladin02_pod_2.Size = new System.Drawing.Size(160, 20);
            this.tb_sladin02_pod_2.TabIndex = 12;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label23);
            this.groupBox3.Controls.Add(this.tb_sladin02_interface_1);
            this.groupBox3.Controls.Add(this.label24);
            this.groupBox3.Controls.Add(this.tb_sladin02_path_1);
            this.groupBox3.Controls.Add(this.label25);
            this.groupBox3.Controls.Add(this.tb_sladin02_pod_1);
            this.groupBox3.Location = new System.Drawing.Point(25, 159);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(292, 134);
            this.groupBox3.TabIndex = 16;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Sladin 02";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(32, 36);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(36, 13);
            this.label23.TabIndex = 8;
            this.label23.Text = "POD :";
            // 
            // tb_sladin02_interface_1
            // 
            this.tb_sladin02_interface_1.Location = new System.Drawing.Point(126, 98);
            this.tb_sladin02_interface_1.Name = "tb_sladin02_interface_1";
            this.tb_sladin02_interface_1.Size = new System.Drawing.Size(160, 20);
            this.tb_sladin02_interface_1.TabIndex = 14;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(32, 69);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(35, 13);
            this.label24.TabIndex = 9;
            this.label24.Text = "Path :";
            // 
            // tb_sladin02_path_1
            // 
            this.tb_sladin02_path_1.Location = new System.Drawing.Point(126, 66);
            this.tb_sladin02_path_1.Name = "tb_sladin02_path_1";
            this.tb_sladin02_path_1.Size = new System.Drawing.Size(160, 20);
            this.tb_sladin02_path_1.TabIndex = 13;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(32, 101);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(55, 13);
            this.label25.TabIndex = 10;
            this.label25.Text = "Interface :";
            // 
            // tb_sladin02_pod_1
            // 
            this.tb_sladin02_pod_1.Location = new System.Drawing.Point(126, 33);
            this.tb_sladin02_pod_1.Name = "tb_sladin02_pod_1";
            this.tb_sladin02_pod_1.Size = new System.Drawing.Size(160, 20);
            this.tb_sladin02_pod_1.TabIndex = 12;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.label44);
            this.groupBox10.Controls.Add(this.tb_sladin01_interface_2);
            this.groupBox10.Controls.Add(this.label45);
            this.groupBox10.Controls.Add(this.tb_sladin01_path_2);
            this.groupBox10.Controls.Add(this.label46);
            this.groupBox10.Controls.Add(this.tb_sladin01_pod_2);
            this.groupBox10.Location = new System.Drawing.Point(362, 14);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(292, 134);
            this.groupBox10.TabIndex = 17;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Sladin 01";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(32, 36);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(36, 13);
            this.label44.TabIndex = 8;
            this.label44.Text = "POD :";
            // 
            // tb_sladin01_interface_2
            // 
            this.tb_sladin01_interface_2.Location = new System.Drawing.Point(126, 98);
            this.tb_sladin01_interface_2.Name = "tb_sladin01_interface_2";
            this.tb_sladin01_interface_2.Size = new System.Drawing.Size(160, 20);
            this.tb_sladin01_interface_2.TabIndex = 14;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(32, 69);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(35, 13);
            this.label45.TabIndex = 9;
            this.label45.Text = "Path :";
            // 
            // tb_sladin01_path_2
            // 
            this.tb_sladin01_path_2.Location = new System.Drawing.Point(126, 66);
            this.tb_sladin01_path_2.Name = "tb_sladin01_path_2";
            this.tb_sladin01_path_2.Size = new System.Drawing.Size(160, 20);
            this.tb_sladin01_path_2.TabIndex = 13;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(32, 101);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(55, 13);
            this.label46.TabIndex = 10;
            this.label46.Text = "Interface :";
            // 
            // tb_sladin01_pod_2
            // 
            this.tb_sladin01_pod_2.Location = new System.Drawing.Point(126, 33);
            this.tb_sladin01_pod_2.Name = "tb_sladin01_pod_2";
            this.tb_sladin01_pod_2.Size = new System.Drawing.Size(160, 20);
            this.tb_sladin01_pod_2.TabIndex = 12;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label30);
            this.groupBox4.Controls.Add(this.tb_sladin01_interface_1);
            this.groupBox4.Controls.Add(this.label31);
            this.groupBox4.Controls.Add(this.tb_sladin01_path_1);
            this.groupBox4.Controls.Add(this.label32);
            this.groupBox4.Controls.Add(this.tb_sladin01_pod_1);
            this.groupBox4.Location = new System.Drawing.Point(25, 14);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(292, 134);
            this.groupBox4.TabIndex = 15;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Sladin 01";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(32, 36);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(36, 13);
            this.label30.TabIndex = 8;
            this.label30.Text = "POD :";
            // 
            // tb_sladin01_interface_1
            // 
            this.tb_sladin01_interface_1.Location = new System.Drawing.Point(126, 98);
            this.tb_sladin01_interface_1.Name = "tb_sladin01_interface_1";
            this.tb_sladin01_interface_1.Size = new System.Drawing.Size(160, 20);
            this.tb_sladin01_interface_1.TabIndex = 14;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(32, 69);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(35, 13);
            this.label31.TabIndex = 9;
            this.label31.Text = "Path :";
            // 
            // tb_sladin01_path_1
            // 
            this.tb_sladin01_path_1.Location = new System.Drawing.Point(126, 66);
            this.tb_sladin01_path_1.Name = "tb_sladin01_path_1";
            this.tb_sladin01_path_1.Size = new System.Drawing.Size(160, 20);
            this.tb_sladin01_path_1.TabIndex = 13;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(32, 101);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(55, 13);
            this.label32.TabIndex = 10;
            this.label32.Text = "Interface :";
            // 
            // tb_sladin01_pod_1
            // 
            this.tb_sladin01_pod_1.Location = new System.Drawing.Point(126, 33);
            this.tb_sladin01_pod_1.Name = "tb_sladin01_pod_1";
            this.tb_sladin01_pod_1.Size = new System.Drawing.Size(160, 20);
            this.tb_sladin01_pod_1.TabIndex = 12;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(584, 320);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(27, 34);
            this.button1.TabIndex = 18;
            this.button1.Text = ">>";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(584, 320);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(27, 35);
            this.button2.TabIndex = 19;
            this.button2.Text = "<<";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::N7K_MonitorPrefix.Properties.Resources.icons8_toggle_on_50;
            this.pictureBox1.Location = new System.Drawing.Point(71, 418);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 24);
            this.pictureBox1.TabIndex = 21;
            this.pictureBox1.TabStop = false;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Checked = true;
            this.checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox1.Location = new System.Drawing.Point(49, 688);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(109, 17);
            this.checkBox1.TabIndex = 20;
            this.checkBox1.Text = "AUTO MOVE ON";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged_1);
            // 
            // Frm_systemSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(617, 721);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.bt_submit);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "Frm_systemSetting";
            this.Text = "System Settings";
            this.Load += new System.EventHandler(this.Frm_systemSetting_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tb_ipSniper02;
        private System.Windows.Forms.TextBox tb_ipSniper01;
        private System.Windows.Forms.TextBox tb_ipSladin02;
        private System.Windows.Forms.TextBox tb_ipSladin01;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tb_communitySniper02;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tb_communitySniper01;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tb_communitySladin02;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tb_communitySladin01;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tb_password;
        private System.Windows.Forms.TextBox tb_userName;
        private System.Windows.Forms.TextBox tb_apicIp;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox tb_l3BTT;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox tb_l3TST;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox tb_tenants;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tb_contractsBTT;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox tb_contractsTST;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox tb_vrfBTT;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox tb_vrfTST;
        private System.Windows.Forms.Button bt_submit;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox tb_sniper02_interface_2;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox tb_sniper02_path_2;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox tb_sniper02_pod_2;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox tb_sniper02_interface_1;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox tb_sniper02_path_1;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox tb_sniper02_pod_1;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox tb_sniper01_interface_2;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox tb_sniper01_path_2;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox tb_sniper01_pod_2;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox tb_sniper01_interface_1;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox tb_sniper01_path_1;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox tb_sniper01_pod_1;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox tb_sladin02_interface_2;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox tb_sladin02_path_2;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox tb_sladin02_pod_2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox tb_sladin02_interface_1;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox tb_sladin02_path_1;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox tb_sladin02_pod_1;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox tb_sladin01_interface_2;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox tb_sladin01_path_2;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox tb_sladin01_pod_2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox tb_sladin01_interface_1;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox tb_sladin01_path_1;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox tb_sladin01_pod_1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.CheckBox checkBox1;
    }
}