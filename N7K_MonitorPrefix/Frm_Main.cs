﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SnmpSharpNet;
using System.Threading;
using RestSharp;
using Newtonsoft.Json.Linq;
using System.Security;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Net.NetworkInformation;

namespace N7K_MonitorPrefix
{
    public partial class Frm_Main : Form
    {
        #region<VAR>
        RestClient apicClient;
        string autoMoveL3OUT = "";
        bool state = true;
        bool isFakeSiteTST = false;
        bool isFakeSiteBTT = false;
        public bool saladin1_ping_state = false;
        public bool saladin2_ping_state = false;
        public bool sniper1_ping_state = false;
        public bool sniper2_ping_state = false;
        public string[] oidSladin01 = null;
        public string[] oidSladin02 = null;
        public string[] oidSniper01 = null;
        public string[] oidSniper02 = null;
        public string[] prefixList = null;
        private string ipSladin01 = "";
        private string ipSladin02 = "";
        private string ipSniper01 = "";
        private string ipSniper02 = "";
        private string communitySladin01 = "";
        private string communitySladin02 = "";
        private string communitySniper01 = "";
        private string communitySniper02 = "";
        private int snmpTimeSleep = 5000;
        public string sladin01_pod_1 = "";
        public string sladin01_pod_2 = "";
        public string sladin01_path_1 = "";
        public string sladin01_path_2 = "";
        public string sladin01_interface_1 = "";
        public string sladin01_interface_2 = "";
        public string sladin02_pod_1 = "";
        public string sladin02_pod_2 = "";
        public string sladin02_path_1 = "";
        public string sladin02_path_2 = "";
        public string sladin02_interface_1 = "";
        public string sladin02_interface_2 = "";
        public string sniper01_pod_1 = "";
        public string sniper01_pod_2 = "";
        public string sniper01_path_1 = "";
        public string sniper01_path_2 = "";
        public string sniper01_interface_1 = "";
        public string sniper01_interface_2 = "";
        public string sniper02_pod_1 = "";
        public string sniper02_pod_2 = "";
        public string sniper02_path_1 = "";
        public string sniper02_path_2 = "";
        public string sniper02_interface_1 = "";
        public string sniper02_interface_2 = "";
        string apicUsername = "";
        string apicPassword = "";
        string apicIP = "";
        string oidRoute = ".1.3.6.1.2.1.4.24.7.1.7.1.4.";
        string tenants = "";
        string vrfBTT = "";
        string vrfTST = "";
        string L3BTT = "";
        string L3TST = "";
        string contractsTST = "";
        string contractsBTT = "";
        bool lostConnectSladin01 = false;
        bool lostConnectSladin02 = false;
        bool lostConnectSniper01 = false;
        bool lostConnectSniper02 = false;
        public Dictionary<string, string[]> dictPrefixData = new Dictionary<string, string[]>();
        public Dictionary<string, string> dictPrefixStatusSladin01 = new Dictionary<string, string>();
        public Dictionary<string, string> dictPrefixStatusSladin02 = new Dictionary<string, string>();
        public Dictionary<string, string> dictPrefixStatusSniper01 = new Dictionary<string, string>();
        public Dictionary<string, string> dictPrefixStatusSniper02 = new Dictionary<string, string>();
        public Dictionary<string, string> dictBDPrefix = new Dictionary<string, string>();
        public Dictionary<string, int> dictPrefixIndex = new Dictionary<string, int>();
        public Dictionary<string, string[]> dictBDSiteCode = new Dictionary<string, string[]>();
        DataTable dt_BTT = new DataTable();
        DataTable dt_TST = new DataTable();
        bool isSladin01Changed = false;
        bool isSladin02Changed = false;
        bool isSniper01Changed = false;
        bool isSniper02Changed = false;
        bool isUpdatePrefixList = true;
        public string[,] inputAdminstate = new string[8, 3];
        public string saladin01AdminState1;
        public string saladin01AdminState2;
        public string saladin02AdminState1;
        public string saladin02AdminState2;
        public string sniper01AdminState1;
        public string sniper01AdminState2;
        public string sniper02AdminState1;
        public string sniper02AdminState2;

        string line_token = "JRugqYqiTpzADLNAHpjp7U62W28m70tNtwbTg97Ihlu";
        #endregion

        public Frm_Main()
        {
            InitializeComponent();
            log4net.Config.DOMConfigurator.Configure();
        }

        #region<form load>
        private void Frm_Main_Load(object sender, EventArgs e)
        {
            dtg_BTT.ColumnCount = 6;

            dtg_BTT.Columns[0].Name = "Bridge Domain";
            dtg_BTT.Columns[1].Name = "VRF";
            dtg_BTT.Columns[2].Name = "App Profile";
            dtg_BTT.Columns[3].Name = "EPG";
            dtg_BTT.Columns[4].Name = "Prefix";
            dtg_BTT.Columns[5].Name = "Status";
            dtg_BTT.Columns[1].Visible = false;
            dtg_BTT.Columns[0].Width = 140;
            dtg_BTT.Columns[2].Width = 105;
            dtg_BTT.Columns[3].Width = 105;
            dtg_BTT.Columns[4].Width = 100;
            dtg_BTT.Columns[5].Width = 40;

            dtg_TST.ColumnCount = 6;

            dtg_TST.Columns[0].Name = "Bridge Domain";
            dtg_TST.Columns[1].Name = "VRF";
            dtg_TST.Columns[2].Name = "App Profile";
            dtg_TST.Columns[3].Name = "EPG";
            dtg_TST.Columns[4].Name = "Prefix";
            dtg_TST.Columns[5].Name = "Status";
            dtg_TST.Columns[1].Visible = false;
            dtg_TST.Columns[0].Width = 140;
            dtg_TST.Columns[2].Width = 105;
            dtg_TST.Columns[3].Width = 105;
            dtg_TST.Columns[4].Width = 100;
            dtg_TST.Columns[5].Width = 40;


            dtg_prefixList.ColumnCount = 4;
            dtg_prefixList.Columns[0].Name = "Prefix";
            dtg_prefixList.Columns[1].Name = "Customer Name";
            dtg_prefixList.Columns[2].Name = "Site";
            dtg_prefixList.Columns[3].Name = "Status";
            dtg_prefixList.Columns[0].Width = 115;
            dtg_prefixList.Columns[1].Width = 95;
            dtg_prefixList.Columns[2].Width = 50;
            dtg_prefixList.Columns[3].Width = 40;
            dtg_prefixList.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dtg_prefixList.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

        }
        #endregion

        #region<send line message>
        private void SendMessage(string msg)
        {
            RestRequest line_post;
            RestClient line_client = new RestClient();

            line_client.BaseUrl = new System.Uri("https://notify-api.line.me/api/notify");
            ServicePointManager.ServerCertificateValidationCallback += (RestClient, certificate, chain, sslPolicyErrors) => true;

            line_post = new RestRequest(Method.POST); ;
            line_post.AddHeader("Authorization", string.Format("Bearer " + line_token));
            line_post.AddHeader("content-type", "application/x-www-form-urlencoded");
            line_post.AddParameter("message", msg);

            try
            {
                IRestResponse response = line_client.Execute(line_post);
            }
            catch
            {
                //Status_Connect.Text = "Can't connect to line server !!";
                MessageBox.Show("Can't connect to line server !!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        #region<form show>
        private void Frm_Main_Shown(object sender, EventArgs e)
        {
            Log.log("Debug", "Start Program");
            UpdatePrefixList();
            #region<read setting data from xml>
            DataSet dsXML = new DataSet();
            dsXML.ReadXml(Application.StartupPath + @"\systemSettings.xml");
            //string[,] prefixList = new string[dsXML.Tables["Prefix"].Rows.Count, 2];
            if (dsXML.Tables.Count > 0)
            {
                if (dsXML.Tables["Settings"].Rows.Count > 0)
                {

                    ipSladin01 = dsXML.Tables["Settings"].Rows[0]["ipSladin01"].ToString();
                    ipSladin02 = dsXML.Tables["Settings"].Rows[0]["ipSladin02"].ToString();
                    ipSniper01 = dsXML.Tables["Settings"].Rows[0]["ipSniper01"].ToString();
                    ipSniper02 = dsXML.Tables["Settings"].Rows[0]["ipSniper02"].ToString();
                    communitySladin01 = dsXML.Tables["Settings"].Rows[0]["communitySladin01"].ToString();
                    communitySladin02 = dsXML.Tables["Settings"].Rows[0]["communitySladin02"].ToString();
                    communitySniper01 = dsXML.Tables["Settings"].Rows[0]["communitySniper01"].ToString();
                    communitySniper02 = dsXML.Tables["Settings"].Rows[0]["communitySniper02"].ToString();

                    sladin01_pod_1 = dsXML.Tables["Settings"].Rows[0]["saladin01pod1"].ToString();
                    sladin01_pod_2 = dsXML.Tables["Settings"].Rows[0]["saladin01pod2"].ToString();
                    sladin01_path_1 = dsXML.Tables["Settings"].Rows[0]["saladin01path1"].ToString();
                    sladin01_path_2 = dsXML.Tables["Settings"].Rows[0]["saladin01path2"].ToString();
                    sladin01_interface_1 = dsXML.Tables["Settings"].Rows[0]["saladin01interface1"].ToString();
                    sladin01_interface_2 = dsXML.Tables["Settings"].Rows[0]["saladin01interface2"].ToString();
                    sladin02_pod_1 = dsXML.Tables["Settings"].Rows[0]["saladin02pod1"].ToString();
                    sladin02_pod_2 = dsXML.Tables["Settings"].Rows[0]["saladin02pod2"].ToString();
                    sladin02_path_1 = dsXML.Tables["Settings"].Rows[0]["saladin02path1"].ToString();
                    sladin02_path_2 = dsXML.Tables["Settings"].Rows[0]["saladin02path2"].ToString();
                    sladin02_interface_1 = dsXML.Tables["Settings"].Rows[0]["saladin02interface1"].ToString();
                    sladin02_interface_2 = dsXML.Tables["Settings"].Rows[0]["saladin02interface2"].ToString();
                    sniper01_pod_1 = dsXML.Tables["Settings"].Rows[0]["sniper01pod1"].ToString();
                    sniper01_pod_2 = dsXML.Tables["Settings"].Rows[0]["sniper01pod2"].ToString();
                    sniper01_path_1 = dsXML.Tables["Settings"].Rows[0]["sniper01path1"].ToString();
                    sniper01_path_2 = dsXML.Tables["Settings"].Rows[0]["sniper01path2"].ToString();
                    sniper01_interface_1 = dsXML.Tables["Settings"].Rows[0]["sniper01interface1"].ToString();
                    sniper01_interface_2 = dsXML.Tables["Settings"].Rows[0]["sniper01interface2"].ToString();
                    sniper02_pod_1 = dsXML.Tables["Settings"].Rows[0]["sniper02pod1"].ToString();
                    sniper02_pod_2 = dsXML.Tables["Settings"].Rows[0]["sniper02pod2"].ToString();
                    sniper02_path_1 = dsXML.Tables["Settings"].Rows[0]["sniper02path1"].ToString();
                    sniper02_path_2 = dsXML.Tables["Settings"].Rows[0]["sniper02path2"].ToString();
                    sniper02_interface_1 = dsXML.Tables["Settings"].Rows[0]["sniper02interface1"].ToString();
                    sniper02_interface_2 = dsXML.Tables["Settings"].Rows[0]["sniper02interface2"].ToString();
                    autoMoveL3OUT = dsXML.Tables["Settings"].Rows[0]["automode"].ToString();

                    inputAdminstate[0, 0] = sladin01_pod_1;
                    inputAdminstate[0, 1] = sladin01_path_1;
                    inputAdminstate[0, 2] = sladin01_interface_1;
                    inputAdminstate[1, 0] = sladin01_pod_2;
                    inputAdminstate[1, 1] = sladin01_path_2;
                    inputAdminstate[1, 2] = sladin01_interface_2;
                    inputAdminstate[2, 0] = sladin02_pod_1;
                    inputAdminstate[2, 1] = sladin02_path_1;
                    inputAdminstate[2, 2] = sladin02_interface_1;
                    inputAdminstate[3, 0] = sladin02_pod_2;
                    inputAdminstate[3, 1] = sladin02_path_2;
                    inputAdminstate[3, 2] = sladin02_interface_2;
                    inputAdminstate[4, 0] = sniper01_pod_1;
                    inputAdminstate[4, 1] = sniper01_path_1;
                    inputAdminstate[4, 2] = sniper01_interface_1;
                    inputAdminstate[5, 0] = sniper01_pod_2;
                    inputAdminstate[5, 1] = sniper01_path_2;
                    inputAdminstate[5, 2] = sniper01_interface_2;
                    inputAdminstate[6, 0] = sniper02_pod_1;
                    inputAdminstate[6, 1] = sniper02_path_1;
                    inputAdminstate[6, 2] = sniper02_interface_1;
                    inputAdminstate[7, 0] = sniper02_pod_2;
                    inputAdminstate[7, 1] = sniper02_path_2;
                    inputAdminstate[7, 2] = sniper02_interface_2;

                    apicIP = dsXML.Tables["Settings"].Rows[0]["apicIp"].ToString();
                    apicUsername = dsXML.Tables["Settings"].Rows[0]["username"].ToString();
                    apicPassword = dsXML.Tables["Settings"].Rows[0]["password"].ToString();
                    tenants = dsXML.Tables["Settings"].Rows[0]["tenants"].ToString();
                    L3BTT = dsXML.Tables["Settings"].Rows[0]["l3BTT"].ToString();
                    L3TST = dsXML.Tables["Settings"].Rows[0]["l3TST"].ToString();
                    vrfBTT = dsXML.Tables["Settings"].Rows[0]["vrfBTT"].ToString();
                    vrfTST = dsXML.Tables["Settings"].Rows[0]["vrfTST"].ToString();
                    contractsBTT = dsXML.Tables["Settings"].Rows[0]["contractsBTT"].ToString();
                    contractsTST = dsXML.Tables["Settings"].Rows[0]["contractsTST"].ToString();

                    timerStart.Start();
                }
            }
            dsXML.Dispose();
            #endregion
        }
        #endregion

        #region<start many thread>
        private void timerStart_Tick(object sender, EventArgs e)
        {
            #region<start thread get snmp>
            timerStart.Stop();

            bgw_sladin1Snmp.WorkerReportsProgress = true; bgw_sladin1Snmp.WorkerSupportsCancellation = true;
            if (bgw_sladin1Snmp.IsBusy != true) bgw_sladin1Snmp.RunWorkerAsync();
            bgw_sladin2Snmp.WorkerReportsProgress = true; bgw_sladin2Snmp.WorkerSupportsCancellation = true;
            if (bgw_sladin2Snmp.IsBusy != true) bgw_sladin2Snmp.RunWorkerAsync();
            bgw_sniper1Snmp.WorkerReportsProgress = true; bgw_sniper1Snmp.WorkerSupportsCancellation = true;
            if (bgw_sniper1Snmp.IsBusy != true) bgw_sniper1Snmp.RunWorkerAsync();
            bgw_sniper2Snmp.WorkerReportsProgress = true; bgw_sniper2Snmp.WorkerSupportsCancellation = true;
            if (bgw_sniper2Snmp.IsBusy != true) bgw_sniper2Snmp.RunWorkerAsync();

            bgw_loginApic.WorkerReportsProgress = true; bgw_loginApic.WorkerSupportsCancellation = true;
            if (bgw_loginApic.IsBusy != true) bgw_loginApic.RunWorkerAsync();
            Thread.Sleep(300);
            bgw_monitorPrefix.WorkerReportsProgress = true; bgw_monitorPrefix.WorkerSupportsCancellation = true;
            if (bgw_monitorPrefix.IsBusy != true) bgw_monitorPrefix.RunWorkerAsync();

            #endregion
        }
        #endregion

        #region<update prefix list to variable>
        public void UpdatePrefixList()
        {
            #region<Update Prefix List>

            dictPrefixIndex.Clear();  //use for insert data to datagridview
            dictPrefixStatusSladin01.Clear();
            dictPrefixStatusSladin02.Clear();
            dictPrefixStatusSniper01.Clear();
            dictPrefixStatusSniper02.Clear();

            DataSet dsXML = new DataSet();
            dsXML.ReadXml(Application.StartupPath + @"\PrefixList.xml");
            //string[,] prefixList = new string[dsXML.Tables["Prefix"].Rows.Count, 2];
            if (dsXML.Tables.Count > 0)
            {
                int rowCount = dsXML.Tables["Prefix"].Rows.Count;
                oidSladin01 = null; oidSladin02 = null; oidSniper01 = null; oidSniper02 = null; prefixList = null;
                oidSladin01 = new string[rowCount];
                oidSladin02 = new string[rowCount];
                oidSniper01 = new string[rowCount];
                oidSniper02 = new string[rowCount];
                prefixList = new string[rowCount];
                dtg_prefixList.Rows.Clear();
                isSladin01Changed = false;
                isSladin02Changed = false;
                isSniper01Changed = false;
                isSniper02Changed = false;
                for (int i = 0; i < rowCount; i++)
                {
                    string[] ip = dsXML.Tables["Prefix"].Rows[i]["PrefixID"].ToString().Split('/');

                    string prefix = ip[0].Trim() + "." + ip[1].Trim();
                    oidSladin01[i] = oidRoute + prefix;
                    oidSladin02[i] = oidRoute + prefix;
                    oidSniper01[i] = oidRoute + prefix;
                    oidSniper02[i] = oidRoute + prefix;
                    prefixList[i] = prefix;
                    dtg_prefixList.Rows.Add(ip[0].Trim() + "/" + ip[1].Trim(), dsXML.Tables["Prefix"].Rows[i]["CustomerName"].ToString(), "", "");
                    if (!dictPrefixIndex.ContainsKey(prefix))
                    {
                        dictPrefixIndex.Add(prefix, i);
                    }
                    else
                    {
                        dictPrefixIndex[prefix] = i;
                    }
                }
            }
            dsXML.Dispose();
            #endregion

        }
        #endregion

        private void AddPrefixToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Frm_PrefixList frm_addPrefix = new Frm_PrefixList();
            frm_addPrefix.ShowDialog();
            tb_sladin1Snmp.Text = "Change";
            tb_sladin2Snmp.Text = "Change";
            tb_sniper1Snmp.Text = "Change";
            tb_sniper2Snmp.Text = "Change";
            UpdatePrefixList();
            isUpdatePrefixList = true;
        }

        #region<Get Snmp Prefix Route @N7K>
        #region<get snmp Sladin01 - TST>
        private void bgw_sladin1Snmp_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            while (state && !e.Cancel)
            {
                Console.WriteLine("Ping sladin 1");
                Ping pingSender = new Ping();
                IPAddress address = IPAddress.Parse(ipSladin01);
                PingReply reply = pingSender.Send(address);

                if (reply.Status == IPStatus.Success)
                {
                    tb_saladin01PingConnect.BeginInvoke(new MethodInvoker(delegate
                    {
                        tb_saladin01PingConnect.Text = "Ping saladin01 connected";
                        tb_saladin01PingConnect.Refresh();
                    }));
                    saladin1_ping_state = true;
                }
                else
                {
                    tb_saladin01PingConnect.BeginInvoke(new MethodInvoker(delegate
                    {
                        tb_saladin01PingConnect.Text = "Ping saladin01 time out";
                        tb_saladin01PingConnect.Refresh();
                    }));
                    saladin1_ping_state = false;
                }
                saladin01AdminState1 = AdminState(inputAdminstate[0, 0], inputAdminstate[0, 1], inputAdminstate[0, 2]);
                saladin01AdminState2 = AdminState(inputAdminstate[1, 0], inputAdminstate[1, 1], inputAdminstate[1, 2]);
                Console.WriteLine("adminStateSaladin01" + saladin01AdminState1 + "," + saladin01AdminState2);
                if (saladin01AdminState1 == "up")
                {
                    btn_adminSaladin01_1.FlatStyle = FlatStyle.Flat;
                    btn_adminSaladin01_1.FlatAppearance.BorderSize = 0;
                    btn_adminSaladin01_1.BackColor = Color.Green;
                    tb_adminstatus_saladin.ForeColor = Color.Green;
                    btn_adminSaladin01_1.BeginInvoke(new MethodInvoker(delegate
                    {
                        errorProvider15.Clear();
                        errorProvider7.Clear();
                    }));

                }
                else if (saladin01AdminState1 == "down")
                {
                    btn_adminSaladin01_1.BeginInvoke(new MethodInvoker(delegate
                    {
                        string errorstr = "Admin state " + sladin01_interface_1 + " = down";
                        errorProvider7.SetError(btn_adminSaladin01_1, errorstr);
                        errorProvider7.SetIconAlignment(btn_adminSaladin01_1, ErrorIconAlignment.TopLeft);
                        errorProvider15.Clear();
                    }));
                    btn_adminSaladin01_1.FlatStyle = FlatStyle.Flat;
                    btn_adminSaladin01_1.FlatAppearance.BorderSize = 0;
                    btn_adminSaladin01_1.BackColor = Color.Red;
                    tb_adminstatus_saladin.ForeColor = Color.Green;
                }
                else
                {
                    tb_adminstatus_saladin.BeginInvoke(new MethodInvoker(delegate
                    {
                        errorProvider15.SetError(tb_adminstatus_saladin, "Can't connect ACI");
                        errorProvider15.SetIconAlignment(tb_adminstatus_saladin, ErrorIconAlignment.TopLeft);
                    }));
                    btn_adminSaladin01_1.FlatStyle = FlatStyle.Flat;
                    btn_adminSaladin01_1.FlatAppearance.BorderSize = 0;
                    btn_adminSaladin01_1.BackColor = Color.Red;
                    tb_adminstatus_saladin.ForeColor = Color.Red;
                }
                if (saladin01AdminState2 == "up")
                {
                    btn_adminSaladin01_2.FlatStyle = FlatStyle.Flat;
                    btn_adminSaladin01_2.FlatAppearance.BorderSize = 0;
                    btn_adminSaladin01_2.BackColor = Color.Green;
                    btn_adminSaladin01_2.BeginInvoke(new MethodInvoker(delegate
                    {
                        errorProvider15.Clear();
                        errorProvider8.Clear();
                    }));

                }
                else if (saladin01AdminState2 == "down")
                {
                    btn_adminSaladin01_2.BeginInvoke(new MethodInvoker(delegate
                    {
                        string errorstr = "Admin state " + sladin01_interface_2 + " = down";
                        errorProvider8.SetError(btn_adminSaladin01_2, errorstr);
                        errorProvider8.SetIconAlignment(btn_adminSaladin01_2, ErrorIconAlignment.TopLeft);
                        errorProvider15.Clear();
                    }));
                    btn_adminSaladin01_2.FlatStyle = FlatStyle.Flat;
                    btn_adminSaladin01_2.FlatAppearance.BorderSize = 0;
                    btn_adminSaladin01_2.BackColor = Color.Red;
                    tb_adminstatus_saladin.ForeColor = Color.Green;

                }
                else
                {
                    tb_adminstatus_saladin.BeginInvoke(new MethodInvoker(delegate
                    {
                        errorProvider15.SetError(tb_adminstatus_saladin, "Can't connect ACI");
                        errorProvider15.SetIconAlignment(tb_adminstatus_saladin, ErrorIconAlignment.TopLeft);
                    }));
                    btn_adminSaladin01_2.FlatStyle = FlatStyle.Flat;
                    btn_adminSaladin01_2.FlatAppearance.BorderSize = 0;
                    btn_adminSaladin01_2.BackColor = Color.Red;

                    tb_adminstatus_saladin.ForeColor = Color.Red;
                }
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                    return;
                }

                if (oidSladin01 != null)
                {
                    SimpleSnmp snmp = new SimpleSnmp(ipSladin01, communitySladin01);

                    if (!snmp.Valid)
                    {

                        if (!e.Cancel)
                        {
                            Log.log("Debug", ":sladin01 host name/ip address is invalid.");

                            tb_sladin01Connect.BeginInvoke(new MethodInvoker(delegate
                            {
                                tb_sladin01Connect.Text = "SNMP agent host name/ip address is invalid.";
                                tb_sladin01Connect.Refresh();

                            }));

                        }
                    }
                    else
                    {

                        string resultString = "";
                        int countError = 0;
                        //    bool lostConnect = false;
                        foreach (string _oid in oidSladin01)
                        {
                            Dictionary<Oid, AsnType> result = snmp.Walk(SnmpVersion.Ver2, _oid);
                            snmp.Timeout = 500;
                            if (result == null)
                            {

                            }
                            else
                            {
                                if (!e.Cancel)
                                {
                                    if (result.Count == 0)
                                    {
                                        countError += 1;
                                        if (countError == oidSladin01.Length)
                                        {
                                            SendMessage("Sladin01: Lost Connect to SNMP Service");
                                            Log.log("error", "Sladin01: Lost Connect to SNMP Service");
                                            tb_sladin01Connect.BeginInvoke(new MethodInvoker(delegate
                                            {
                                                tb_sladin01Connect.Text = "No results recieved.";
                                                tb_sladin01Connect.Refresh();
                                            }));
                                            lostConnectSladin01 = true;
                                        }
                                    }
                                    else
                                    {
                                        tb_sladin01Connect.BeginInvoke(new MethodInvoker(delegate {
                                            tb_sladin01Connect.Text = "SNMP agent host name/ip address is valid.";
                                            tb_sladin01Connect.Refresh();
                                        }));
                                        if (lostConnectSladin01)
                                        {
                                            lostConnectSladin01 = false;
                                            SendMessage("Sladin01: Reconnect to SNMP Service Success!");
                                            Log.log("debug", "Sladin01: Reconnect to SNMP Service Success!");
                                        }
                                    }

                                    if (isFakeSiteTST && _oid.Replace(oidRoute, "") == "192.168.1.1.24")
                                    {


                                        resultString += _oid.Replace(oidRoute, "") + ":" + "1" + System.Environment.NewLine;


                                    }

                                    else
                                    {
                                        if (result.Count > 1)
                                        {
                                            resultString += _oid.Replace(oidRoute, "") + ":" + "1" + System.Environment.NewLine;
                                        }
                                        else if (result.Count == 1)
                                        {
                                            resultString += _oid.Replace(oidRoute, "") + ":" + "0" + System.Environment.NewLine;
                                        }
                                        else if (result.Count == 0)
                                        {
                                            resultString += _oid.Replace(oidRoute, "") + ":" + "n" + System.Environment.NewLine;
                                        }
                                    }
                                }

                            }
                        }
                        tb_sladin1Snmp.BeginInvoke(new MethodInvoker(delegate { tb_sladin1Snmp.Text = resultString; }));

                    }
                }

                Thread.Sleep(snmpTimeSleep);
            }

        }
        #endregion
        #region<get snmp sladin02 - TST>

        private void bgw_sladin2Snmp_DoWork(object sender, DoWorkEventArgs e)
        {

            BackgroundWorker worker = sender as BackgroundWorker;
            while (state && !e.Cancel)
            {
                Console.WriteLine("Ping sladin 2");
                Ping pingSender = new Ping();
                //203.150.16.174
                IPAddress address = IPAddress.Parse(ipSladin02);
                PingReply reply = pingSender.Send(address);
                if (reply.Status == IPStatus.Success)
                {
                    tb_saladin02PingConnect.BeginInvoke(new MethodInvoker(delegate
                    {
                        tb_saladin02PingConnect.Text = "Ping saladin02 connected";
                        tb_saladin02PingConnect.Refresh();
                    }));
                    saladin2_ping_state = true;
                }
                else
                {
                    tb_saladin02PingConnect.BeginInvoke(new MethodInvoker(delegate
                    {
                        tb_saladin02PingConnect.Text = "Ping saladin02 time out";
                        tb_saladin02PingConnect.Refresh();
                    }));
                    saladin2_ping_state = false;
                }
                saladin02AdminState1 = AdminState(inputAdminstate[2, 0], inputAdminstate[2, 1], inputAdminstate[2, 2]);
                saladin02AdminState2 = AdminState(inputAdminstate[3, 0], inputAdminstate[3, 1], inputAdminstate[3, 2]);
                if (saladin02AdminState1 == "up")
                {
                    btn_adminSaladin02_1.FlatStyle = FlatStyle.Flat;
                    btn_adminSaladin02_1.FlatAppearance.BorderSize = 0;
                    btn_adminSaladin02_1.BackColor = Color.Green;
                    tb_adminstatus_saladin.ForeColor = Color.Green;

                    btn_adminSaladin02_1.BeginInvoke(new MethodInvoker(delegate
                    {
                        errorProvider15.Clear();
                        errorProvider9.Clear();
                    }));
                }
                else if (saladin02AdminState1 == "down")
                {
                    btn_adminSaladin02_1.BeginInvoke(new MethodInvoker(delegate
                    {
                        string errorstr = "Admin state " + sladin02_interface_1 + " = down";
                        errorProvider9.SetError(btn_adminSaladin02_1, errorstr);
                        errorProvider9.SetIconAlignment(btn_adminSaladin02_1, ErrorIconAlignment.TopLeft);
                        errorProvider15.Clear();
                    }));
                    btn_adminSaladin02_1.FlatStyle = FlatStyle.Flat;
                    btn_adminSaladin02_1.FlatAppearance.BorderSize = 0;
                    btn_adminSaladin02_1.BackColor = Color.Red;
                    tb_adminstatus_saladin.ForeColor = Color.Green;

                }
                else
                {
                    tb_adminstatus_saladin.BeginInvoke(new MethodInvoker(delegate
                    {
                        errorProvider15.SetError(tb_adminstatus_saladin, "Can't connect ACI");
                        errorProvider15.SetIconAlignment(tb_adminstatus_saladin, ErrorIconAlignment.TopLeft);
                    }));
                    btn_adminSaladin02_1.FlatStyle = FlatStyle.Flat;
                    btn_adminSaladin02_1.FlatAppearance.BorderSize = 0;
                    btn_adminSaladin02_1.BackColor = Color.Red;

                    tb_adminstatus_saladin.ForeColor = Color.Red;
                }
                if (saladin02AdminState2 == "up")
                {
                    btn_adminSaladin02_2.FlatStyle = FlatStyle.Flat;
                    btn_adminSaladin02_2.FlatAppearance.BorderSize = 0;
                    btn_adminSaladin02_2.BackColor = Color.Green;
                    tb_adminstatus_saladin.ForeColor = Color.Green;

                    btn_adminSaladin02_2.BeginInvoke(new MethodInvoker(delegate
                    {
                        errorProvider15.Clear();
                        errorProvider10.Clear();
                    }));
                }
                else if (saladin02AdminState2 == "down")
                {
                    btn_adminSaladin02_2.BeginInvoke(new MethodInvoker(delegate
                    {
                        string errorstr = "Admin state " + sladin02_interface_2 + " = down";
                        errorProvider10.SetError(btn_adminSaladin02_2, errorstr);
                        errorProvider10.SetIconAlignment(btn_adminSaladin02_2, ErrorIconAlignment.TopLeft);
                        errorProvider15.Clear();
                    }));
                    btn_adminSaladin02_2.FlatStyle = FlatStyle.Flat;
                    btn_adminSaladin02_2.FlatAppearance.BorderSize = 0;
                    btn_adminSaladin02_2.BackColor = Color.Red;
                    tb_adminstatus_saladin.ForeColor = Color.Green;

                }
                else
                {
                    tb_adminstatus_saladin.BeginInvoke(new MethodInvoker(delegate
                    {
                        errorProvider15.SetError(tb_adminstatus_saladin, "Can't connect ACI");
                        errorProvider15.SetIconAlignment(tb_adminstatus_saladin, ErrorIconAlignment.TopLeft);
                    }));
                    btn_adminSaladin02_2.FlatStyle = FlatStyle.Flat;
                    btn_adminSaladin02_2.FlatAppearance.BorderSize = 0;
                    btn_adminSaladin02_2.BackColor = Color.Red;

                    tb_adminstatus_saladin.ForeColor = Color.Red;
                }
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                    return;
                }
                if (oidSladin02 != null)
                {
                    SimpleSnmp snmp = new SimpleSnmp(ipSladin02, communitySladin02);
                    if (!snmp.Valid)
                    {
                        if (!e.Cancel)
                        {
                            Log.log("Debug", ":sladin02 host name/ip address is invalid.");

                            tb_sladin02Connect.BeginInvoke(new MethodInvoker(delegate
                            {
                                tb_sladin02Connect.Text = "SNMP agent host name/ip address is invalid.";

                                tb_sladin02Connect.Refresh();
                            }));
                        }
                    }
                    else
                    {

                        //tb_sladin02Connect.BeginInvoke(new MethodInvoker(delegate {
                        //    tb_sladin02Connect.Text = "SNMP agent host name/ip address is valid.";
                        //    tb_sladin02Connect.Refresh();
                        //}));
                        string resultString = "";
                        int countError = 0;
                        // bool lostConnect = false;
                        foreach (string _oid in oidSladin02)
                        {
                            Dictionary<Oid, AsnType> result = snmp.Walk(SnmpVersion.Ver2, _oid);
                            snmp.Timeout = 500;
                            if (result == null)
                            {

                            }
                            else
                            {
                                if (result.Count == 0)
                                {
                                    countError += 1;
                                    if (countError == oidSladin02.Length)
                                    {
                                        SendMessage("Sladin02: Lost Connect to SNMP Service");
                                        Log.log("error", "Sladin02: Lost Connect to SNMP Service");
                                        tb_sladin02Connect.BeginInvoke(new MethodInvoker(delegate
                                        {
                                            tb_sladin02Connect.Text = "No results recieved.";
                                            tb_sladin02Connect.Refresh();
                                        }));
                                        lostConnectSladin02 = true;
                                    }
                                }
                                else
                                {
                                    tb_sladin02Connect.BeginInvoke(new MethodInvoker(delegate {
                                        tb_sladin02Connect.Text = "SNMP agent host name/ip address is valid.";
                                        tb_sladin02Connect.Refresh();
                                    }));
                                    if (lostConnectSladin02)
                                    {
                                        lostConnectSladin02 = false;
                                        SendMessage("Sladin02: Reconnect to SNMP Service Success!");
                                        Log.log("debug", "Sladin02: Reconnect to SNMP Service Success!");
                                    }
                                }

                                if (!e.Cancel)
                                {

                                    if (result.Count > 1)
                                    {
                                        resultString += _oid.Replace(oidRoute, "") + ":" + "1" + System.Environment.NewLine;
                                    }
                                    else if (result.Count == 1)
                                    {
                                        resultString += _oid.Replace(oidRoute, "") + ":" + "0" + System.Environment.NewLine;
                                    }
                                    else if (result.Count == 0)
                                    {
                                        resultString += _oid.Replace(oidRoute, "") + ":" + "n" + System.Environment.NewLine;
                                    }

                                }

                            }
                        }
                        tb_sladin2Snmp.BeginInvoke(new MethodInvoker(delegate { tb_sladin2Snmp.Text = resultString; }));
                    }

                }

                Thread.Sleep(snmpTimeSleep);
            }
        }
        #endregion
        #region<get snmp sniper01 - BTT>
        private void bgw_sniper1Snmp_DoWork(object sender, DoWorkEventArgs e)
        {

            BackgroundWorker worker = sender as BackgroundWorker;
            while (state && !e.Cancel)
            {
                Console.WriteLine("Ping sniper 1");
                Ping pingSender = new Ping();
                IPAddress address = IPAddress.Parse(ipSniper01);
                PingReply reply = pingSender.Send(address);

                if (reply.Status == IPStatus.Success)
                {
                    tb_sniper01PingConnect.BeginInvoke(new MethodInvoker(delegate
                    {
                        tb_sniper01PingConnect.Text = "Ping sniper01 connected";
                        tb_sniper01PingConnect.Refresh();
                    }));

                    sniper1_ping_state = true;
                }
                else
                {
                    tb_sniper01PingConnect.BeginInvoke(new MethodInvoker(delegate
                    {
                        tb_sniper01PingConnect.Text = "Ping sniper01 time out";
                        tb_sniper01PingConnect.Refresh();
                    }));
                    sniper1_ping_state = false;
                }
                sniper01AdminState1 = AdminState(inputAdminstate[4, 0], inputAdminstate[4, 1], inputAdminstate[4, 2]);
                sniper01AdminState2 = AdminState(inputAdminstate[5, 0], inputAdminstate[5, 1], inputAdminstate[5, 2]);
                if (sniper01AdminState1 == "up")
                {
                    btn_adminSniper01_1.FlatStyle = FlatStyle.Flat;
                    btn_adminSniper01_1.FlatAppearance.BorderSize = 0;
                    btn_adminSniper01_1.BackColor = Color.Green;
                    tb_adminstatus_sniper.ForeColor = Color.Green;
                    btn_adminSniper01_1.BeginInvoke(new MethodInvoker(delegate
                    {
                        errorProvider11.Clear();
                        errorProvider16.Clear();
                    }));
                }
                else if (sniper01AdminState1 == "down")
                {
                    btn_adminSniper01_1.BeginInvoke(new MethodInvoker(delegate
                    {
                        string errorstr = "Admin state " + sniper01_interface_1 + " = down";
                        errorProvider11.SetError(btn_adminSniper01_1, errorstr);
                        errorProvider11.SetIconAlignment(btn_adminSniper01_1, ErrorIconAlignment.TopLeft);
                        errorProvider16.Clear();
                    }));
                    btn_adminSniper01_1.FlatStyle = FlatStyle.Flat;
                    btn_adminSniper01_1.FlatAppearance.BorderSize = 0;
                    btn_adminSniper01_1.BackColor = Color.Red;
                    tb_adminstatus_sniper.ForeColor = Color.Green;
                }
                else
                {
                    tb_adminstatus_sniper.BeginInvoke(new MethodInvoker(delegate
                    {
                        errorProvider16.SetError(tb_adminstatus_sniper, "Can't connect ACI");
                        errorProvider16.SetIconAlignment(tb_adminstatus_sniper, ErrorIconAlignment.TopLeft);
                    }));
                    btn_adminSniper01_1.FlatStyle = FlatStyle.Flat;
                    btn_adminSniper01_1.FlatAppearance.BorderSize = 0;
                    btn_adminSniper01_1.BackColor = Color.Red;
                    tb_adminstatus_sniper.ForeColor = Color.Red;

                }
                if (sniper01AdminState2 == "up")
                {
                    btn_adminSniper01_2.FlatStyle = FlatStyle.Flat;
                    btn_adminSniper01_2.FlatAppearance.BorderSize = 0;
                    btn_adminSniper01_2.BackColor = Color.Green;
                    tb_adminstatus_sniper.ForeColor = Color.Green;

                    btn_adminSniper01_2.BeginInvoke(new MethodInvoker(delegate
                    {
                        errorProvider16.Clear();
                        errorProvider12.Clear();
                    }));
                }
                else if (sniper01AdminState2 == "down")
                {
                    btn_adminSniper01_2.BeginInvoke(new MethodInvoker(delegate
                    {
                        string errorstr = "Admin state " + sniper01_interface_2 + " = down";
                        errorProvider12.SetError(btn_adminSniper01_2, errorstr);
                        errorProvider12.SetIconAlignment(btn_adminSniper01_2, ErrorIconAlignment.TopLeft);
                        errorProvider16.Clear();
                    }));
                    btn_adminSniper01_2.FlatStyle = FlatStyle.Flat;
                    btn_adminSniper01_2.FlatAppearance.BorderSize = 0;
                    btn_adminSniper01_2.BackColor = Color.Red;
                    tb_adminstatus_sniper.ForeColor = Color.Green;

                }
                else
                {
                    tb_adminstatus_sniper.BeginInvoke(new MethodInvoker(delegate
                    {
                        errorProvider16.SetError(tb_adminstatus_sniper, "Can't connect ACI");
                        errorProvider16.SetIconAlignment(tb_adminstatus_sniper, ErrorIconAlignment.TopLeft);
                    }));
                    btn_adminSniper01_2.FlatStyle = FlatStyle.Flat;
                    btn_adminSniper01_2.FlatAppearance.BorderSize = 0;
                    btn_adminSniper01_2.BackColor = Color.Red;

                    tb_adminstatus_sniper.ForeColor = Color.Red;

                }
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                    return;
                }
                if (oidSniper01 != null)
                {
                    SimpleSnmp snmp = new SimpleSnmp(ipSniper01, communitySniper01);
                    if (!snmp.Valid)
                    {
                        if (!e.Cancel)
                        {
                            Log.log("Debug", ":sniper01 host name/ip address is invalid.");

                            tb_sniper01Connect.BeginInvoke(new MethodInvoker(delegate
                            {
                                tb_sniper01Connect.Text = "SNMP agent host name/ip address is invalid.";
                                tb_sniper01Connect.Refresh();
                            }));
                        }

                    }
                    else
                    {

                        string resultString = "";

                        int countError = 0;
                        foreach (string _oid in oidSniper01)
                        {
                            Dictionary<Oid, AsnType> result = snmp.Walk(SnmpVersion.Ver2, _oid);
                            snmp.Timeout = 500;
                            if (result == null)
                            {

                            }
                            else
                            {
                                if (result.Count == 0)
                                {
                                    countError += 1;
                                    if (countError == oidSniper01.Length)
                                    {
                                        Log.log("error", "Sniper01: Lost Connect to SNMP Service");
                                        SendMessage("Sniper01: Lost Connect to SNMP Service");
                                        tb_sniper01Connect.BeginInvoke(new MethodInvoker(delegate
                                        {
                                            tb_sniper01Connect.Text = "No results recieved.";
                                            tb_sniper01Connect.Refresh();
                                        }));
                                        lostConnectSniper01 = true;
                                    }
                                }
                                else
                                {
                                    tb_sniper01Connect.BeginInvoke(new MethodInvoker(delegate {
                                        tb_sniper01Connect.Text = "SNMP agent host name/ip address is valid.";
                                        tb_sniper01Connect.Refresh();
                                    }));
                                    if (lostConnectSniper01)
                                    {
                                        Log.log("debug", "Sniper01: Reconnect to SNMP Service Success!");
                                        lostConnectSniper01 = false;
                                        SendMessage("Sniper01: Reconnect to SNMP Service Success!");
                                    }
                                }
                                if (!e.Cancel)
                                {
                                    if (isFakeSiteBTT && _oid.Replace(oidRoute, "") == "192.168.1.1.24")
                                    {


                                        resultString += _oid.Replace(oidRoute, "") + ":" + "1" + System.Environment.NewLine;


                                    }
                                    else
                                    {
                                        if (result.Count > 1)
                                        {
                                            resultString += _oid.Replace(oidRoute, "") + ":" + "1" + System.Environment.NewLine;
                                        }
                                        else if (result.Count == 1)
                                        {
                                            resultString += _oid.Replace(oidRoute, "") + ":" + "0" + System.Environment.NewLine;
                                        }
                                        else if (result.Count == 0)
                                        {
                                            resultString += _oid.Replace(oidRoute, "") + ":" + "n" + System.Environment.NewLine;
                                        }
                                    }
                                }

                            }
                        }
                        tb_sniper1Snmp.BeginInvoke(new MethodInvoker(delegate { tb_sniper1Snmp.Text = resultString; }));
                    }



                }

                Thread.Sleep(snmpTimeSleep);
            }
        }
        #endregion
        #region<get snmp sniper02 - BTT>
        private void bgw_sniper2Snmp_DoWork(object sender, DoWorkEventArgs e)
        {

            BackgroundWorker worker = sender as BackgroundWorker;
            while (state && !e.Cancel)
            {
                Console.WriteLine("Ping sniper 2");
                Ping pingSender = new Ping();
                IPAddress address = IPAddress.Parse(ipSniper02);
                PingReply reply = pingSender.Send(address);

                if (reply.Status == IPStatus.Success)
                {
                    tb_sniper02PingConnect.BeginInvoke(new MethodInvoker(delegate
                    {
                        tb_sniper02PingConnect.Text = "Ping sniper02 connected";
                        tb_sniper02PingConnect.Refresh();
                    }));
                    sniper2_ping_state = true;
                }
                else
                {
                    tb_sniper02PingConnect.BeginInvoke(new MethodInvoker(delegate
                    {
                        tb_sniper02PingConnect.Text = "Ping sniper02 time out";
                        tb_sniper02PingConnect.Refresh();
                    }));
                    sniper2_ping_state = false;
                }
                sniper02AdminState1 = AdminState(inputAdminstate[6, 0], inputAdminstate[6, 1], inputAdminstate[6, 2]);
                sniper02AdminState2 = AdminState(inputAdminstate[7, 0], inputAdminstate[7, 1], inputAdminstate[7, 2]);
                if (sniper02AdminState1 == "up")
                {
                    btn_adminSniper02_1.FlatStyle = FlatStyle.Flat;
                    btn_adminSniper02_1.FlatAppearance.BorderSize = 0;
                    btn_adminSniper02_1.BackColor = Color.Green;
                    tb_adminstatus_sniper.ForeColor = Color.Green;

                    btn_adminSniper02_1.BeginInvoke(new MethodInvoker(delegate
                    {
                        errorProvider16.Clear();
                        errorProvider13.Clear();
                    }));
                }
                else if (sniper02AdminState1 == "down")
                {
                    btn_adminSniper02_1.BeginInvoke(new MethodInvoker(delegate
                    {
                        string errorstr = "Admin state " + sniper02_interface_1 + " = down";
                        errorProvider13.SetError(btn_adminSniper02_1, errorstr);
                        errorProvider13.SetIconAlignment(btn_adminSniper02_1, ErrorIconAlignment.TopLeft);
                        errorProvider16.Clear();

                    }));
                    btn_adminSniper02_1.FlatStyle = FlatStyle.Flat;
                    btn_adminSniper02_1.FlatAppearance.BorderSize = 0;
                    btn_adminSniper02_1.BackColor = Color.Red;
                    tb_adminstatus_sniper.ForeColor = Color.Green;
                }
                else
                {
                    tb_adminstatus_sniper.BeginInvoke(new MethodInvoker(delegate
                    {
                        errorProvider16.SetError(tb_adminstatus_sniper, "Can't connect ACI");
                        errorProvider16.SetIconAlignment(tb_adminstatus_sniper, ErrorIconAlignment.TopLeft);
                    }));
                    btn_adminSniper02_1.FlatStyle = FlatStyle.Flat;
                    btn_adminSniper02_1.FlatAppearance.BorderSize = 0;
                    btn_adminSniper02_1.BackColor = Color.Red;

                    tb_adminstatus_sniper.ForeColor = Color.Red;

                }
                if (sniper02AdminState2 == "up")
                {

                    btn_adminSniper02_2.FlatStyle = FlatStyle.Flat;
                    btn_adminSniper02_2.FlatAppearance.BorderSize = 0;
                    btn_adminSniper02_2.BackColor = Color.Green;
                    tb_adminstatus_sniper.ForeColor = Color.Green;
                    btn_adminSniper02_2.BeginInvoke(new MethodInvoker(delegate
                    {
                        errorProvider16.Clear();
                        errorProvider14.Clear();
                    }));

                }
                else if (sniper02AdminState2 == "down")
                {
                    btn_adminSniper02_2.BeginInvoke(new MethodInvoker(delegate
                    {
                        string errorstr = "Admin state " + sniper02_interface_2 + " = down";
                        errorProvider14.SetError(btn_adminSniper02_2, errorstr);
                        errorProvider14.SetIconAlignment(btn_adminSniper02_2, ErrorIconAlignment.TopLeft);
                        errorProvider16.Clear();

                    }));
                    btn_adminSniper02_2.FlatStyle = FlatStyle.Flat;
                    btn_adminSniper02_2.FlatAppearance.BorderSize = 0;
                    btn_adminSniper02_2.BackColor = Color.Red;
                    tb_adminstatus_sniper.ForeColor = Color.Green;

                }
                else
                {
                    tb_adminstatus_sniper.BeginInvoke(new MethodInvoker(delegate
                    {
                        errorProvider16.SetError(tb_adminstatus_sniper, "Can't connect ACI");
                        errorProvider16.SetIconAlignment(tb_adminstatus_sniper, ErrorIconAlignment.TopLeft);
                    }));
                    btn_adminSniper02_2.FlatStyle = FlatStyle.Flat;
                    btn_adminSniper02_2.FlatAppearance.BorderSize = 0;
                    btn_adminSniper02_2.BackColor = Color.Red;

                    tb_adminstatus_sniper.ForeColor = Color.Red;

                }
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                    return;
                }
                if (oidSniper02 != null)
                {
                    SimpleSnmp snmp = new SimpleSnmp(ipSniper02, communitySniper02);
                    if (!snmp.Valid)
                    {
                        if (!e.Cancel)
                        {
                            Log.log("Debug", ":sniper02 host name/ip address is invalid.");
                            tb_sniper02Connect.BeginInvoke(new MethodInvoker(delegate
                            {
                                tb_sniper02Connect.Text = "SNMP agent host name/ip address is invalid.";
                                tb_sniper02Connect.Refresh();
                            }));
                        }


                    }
                    else
                    {
                        string resultString = "";
                        int countError = 0;
                        foreach (string _oid in oidSniper02)
                        {

                            Dictionary<Oid, AsnType> result = snmp.Walk(SnmpVersion.Ver2, _oid);
                            snmp.Timeout = 500;
                            if (result == null)
                            {
                            }
                            else
                            {
                                if (result.Count == 0)
                                {
                                    countError += 1;
                                    if (countError == oidSniper01.Length)
                                    {
                                        SendMessage("Sniper02: Lost Connect to SNMP Service");
                                        Log.log("error", "Sniper02: Lost Connect to SNMP Service");
                                        tb_sniper02Connect.BeginInvoke(new MethodInvoker(delegate
                                        {
                                            tb_sniper02Connect.Text = "No results recieved.";
                                            tb_sniper02Connect.Refresh();
                                        }));
                                        lostConnectSniper02 = true;
                                    }
                                }
                                else
                                {
                                    tb_sniper02Connect.BeginInvoke(new MethodInvoker(delegate
                                    {
                                        tb_sniper02Connect.Text = "SNMP agent host name/ip address is valid.";
                                        tb_sniper02Connect.Refresh();
                                    }));
                                    if (lostConnectSniper02)
                                    {
                                        lostConnectSniper02 = false;
                                        SendMessage("Sniper02: Reconnect to SNMP Service Success!");
                                        Log.log("debug", "Sniper02: Reconnect to SNMP Service Success!");
                                    }
                                }
                                if (!e.Cancel)
                                {
                                    if (result.Count > 1)
                                    {
                                        resultString += _oid.Replace(oidRoute, "") + ":" + "1" + System.Environment.NewLine;
                                    }
                                    else if (result.Count == 1)
                                    {
                                        resultString += _oid.Replace(oidRoute, "") + ":" + "0" + System.Environment.NewLine;
                                    }
                                    else if (result.Count == 0)
                                    {
                                        resultString += _oid.Replace(oidRoute, "") + ":" + "n" + System.Environment.NewLine;
                                    }
                                }

                            }
                        }
                        tb_sniper2Snmp.BeginInvoke(new MethodInvoker(delegate { tb_sniper2Snmp.Text = resultString; }));
                    }
                }

                Thread.Sleep(snmpTimeSleep);
            }
        }
        #endregion
        #endregion

        #region<SNMP Connect @N7K>
        private void Tb_sladinConnect_TextChanged(object sender, EventArgs e)
        {
            if (tb_sladin01Connect.Text == "SNMP agent host name/ip address is invalid.")
            {
                errorProvider18.SetError(tb_sladin01Connect, "Failed");
                errorProvider18.SetIconAlignment(tb_sladin01Connect, ErrorIconAlignment.TopLeft);
                img_sladin01.Image = Properties.Resources.router_grey_small;
                tb_sladin01Connect.ForeColor = Color.Red;
            }
            if (tb_sladin01Connect.Text == "No results recieved.")
            {

                img_sladin01.Image = Properties.Resources.router_grey_small;
                tb_sladin01Connect.ForeColor = Color.Purple;
                errorProvider18.Clear();

            }
            else
            {
                errorProvider18.Clear();
                img_sladin01.Image = Properties.Resources.router_blue_small;
                tb_sladin01Connect.ForeColor = Color.Green;
            }
        }

        private void Tb_sladin02Connect_TextChanged(object sender, EventArgs e)
        {

            if (tb_sladin02Connect.Text == "SNMP agent host name/ip address is invalid.")
            {
                errorProvider17.SetError(tb_sladin02Connect, "Failed");
                errorProvider17.SetIconAlignment(tb_sladin02Connect, ErrorIconAlignment.TopLeft);
                img_sladin02.Image = Properties.Resources.router_grey_small;
                tb_sladin02Connect.ForeColor = Color.Red;
            }
            if (tb_sladin02Connect.Text == "No results recieved.")
            {

                img_sladin02.Image = Properties.Resources.router_grey_small;
                tb_sladin02Connect.ForeColor = Color.Purple;
                errorProvider17.Clear();

            }
            else
            {
                errorProvider17.Clear();
                img_sladin02.Image = Properties.Resources.router_blue_small;
                tb_sladin02Connect.ForeColor = Color.Green;
            }
        }

        private void Tb_sniper01Connect_TextChanged(object sender, EventArgs e)
        {
            if (tb_sniper01Connect.Text == "SNMP agent host name/ip address is invalid.")
            {
                errorProvider2.SetError(tb_sniper01Connect, "Failed");
                errorProvider2.SetIconAlignment(tb_sniper01Connect, ErrorIconAlignment.TopLeft);
                img_sniper01.Image = Properties.Resources.router_grey_small;
                tb_sniper01Connect.ForeColor = Color.Red;
            }
            if (tb_sniper01Connect.Text == "No results recieved.")
            {
                errorProvider2.Clear();
                img_sniper01.Image = Properties.Resources.router_grey_small;
                tb_sladin01Connect.ForeColor = Color.Purple;
            }
            else
            {
                errorProvider2.Clear();
                img_sniper01.Image = Properties.Resources.router_blue_small;
                tb_sniper01Connect.ForeColor = Color.Green;
            }
        }

        private void Tb_sniper02Connect_TextChanged(object sender, EventArgs e)
        {
            if (tb_sniper02Connect.Text == "SNMP agent host name/ip address is invalid.")
            {
                errorProvider1.SetError(tb_sniper02Connect, "Failed");
                errorProvider1.SetIconAlignment(tb_sniper02Connect, ErrorIconAlignment.TopLeft);
                img_sniper02.Image = Properties.Resources.router_grey_small;
                tb_sniper02Connect.ForeColor = Color.Red;
            }
            if (tb_sniper02Connect.Text == "No results recieved.")
            {
                errorProvider1.Clear();

                img_sniper02.Image = Properties.Resources.router_grey_small;
                tb_sladin02Connect.ForeColor = Color.Purple;
            }
            else
            {
                errorProvider1.Clear();
                img_sniper02.Image = Properties.Resources.router_blue_small;
                tb_sniper02Connect.ForeColor = Color.Green;
            }
        }
        #endregion

        private void Tb_saladin01PingConnect_TextChanged(object sender, EventArgs e)
        {
            if (tb_saladin01PingConnect.Text == "Ping saladin01 time out")
            {
                errorProvider5.SetError(tb_saladin01PingConnect, "Time out");
                errorProvider5.SetIconAlignment(tb_saladin01PingConnect, ErrorIconAlignment.TopLeft);
                tb_saladin01PingConnect.ForeColor = Color.Red;
            }
            else
            {
                tb_saladin01PingConnect.ForeColor = Color.Green;
                errorProvider5.Clear();
            }
        }
        private void Tb_saladin02PingConnect_TextChanged(object sender, EventArgs e)
        {
            if (tb_saladin02PingConnect.Text == "Ping saladin02 time out")
            {
                errorProvider6.SetError(tb_saladin02PingConnect, "Time out");
                errorProvider6.SetIconAlignment(tb_saladin02PingConnect, ErrorIconAlignment.TopLeft);
                tb_saladin02PingConnect.ForeColor = Color.Red;
            }
            else
            {
                tb_saladin02PingConnect.ForeColor = Color.Green;
                errorProvider6.Clear();
            }
        }
        private void Tb_sniper01PingConnect_TextChanged(object sender, EventArgs e)
        {
            if (tb_sniper01PingConnect.Text == "Ping sniper01 time out")
            {
                errorProvider3.SetError(tb_sniper01PingConnect, "Time out");
                errorProvider3.SetIconAlignment(tb_sniper01PingConnect, ErrorIconAlignment.TopLeft);
                tb_sniper01PingConnect.ForeColor = Color.Red;
            }
            else
            {
                tb_sniper01PingConnect.ForeColor = Color.Green;
                errorProvider3.Clear();
            }
        }
        private void Tb_sniper02PingConnect_TextChanged(object sender, EventArgs e)
        {
            if (tb_sniper02PingConnect.Text == "Ping sniper02 time out")
            {
                errorProvider4.SetError(tb_sniper02PingConnect, "Time out");
                errorProvider4.SetIconAlignment(tb_sniper02PingConnect, ErrorIconAlignment.TopLeft);
                tb_sniper02PingConnect.ForeColor = Color.Red;
            }
            else
            {
                tb_sniper02PingConnect.ForeColor = Color.Green;
                errorProvider4.Clear();
            }
        }

        #region<Login To APIC>
        private void bgw_loginApic_DoWork(object sender, DoWorkEventArgs e)
        {
            apicClient = new RestClient("https://" + apicIP + "/api/aaaLogin.json");
            apicClient.Timeout = 2000;
            apicClient.CookieContainer = new System.Net.CookieContainer();
            ServicePointManager.ServerCertificateValidationCallback += (RestClient, certificate, chain, sslPolicyErrors) => true;
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json", "{\r\n  \"aaaUser\" : {\r\n    \"attributes\" : {\r\n      \"name\" : \"" + apicUsername + "\",\r\n  " +
                "    \"pwd\" : \"" + apicPassword + "\"\r\n    }\r\n  }\r\n}", ParameterType.RequestBody);
            IRestResponse response = apicClient.Execute(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                GetBDData();
                bgw_apicBDUpdate.WorkerReportsProgress = true; bgw_apicBDUpdate.WorkerSupportsCancellation = true;
                if (bgw_apicBDUpdate.IsBusy != true) bgw_apicBDUpdate.RunWorkerAsync();
            }
            else
            {
                Log.log("Error", "Apic (" + apicIP + ") cannot connect.");
                MessageBox.Show("ไม่สามารถเชื่อมต่อกับ APIC ได้ กรุณาตรวจสอบการเชื่อมต่อด้วยค่ะ");

            }

        }
        private void bgw_adminstate_DoWork(object sender, DoWorkEventArgs e)
        {

        }
        #endregion

        private void GetBDData()
        {
            dictPrefixData.Clear();
            dictBDPrefix.Clear();

            /*find all BD in tenants*/
            apicClient.BaseUrl = new System.Uri("https://" + apicIP + "/api/node/mo/uni/tn-" + tenants + ".json?query-target=children&target-subtree-class=fvBD");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            IRestResponse responseBD = apicClient.Execute(request);
            JObject jsonBD = JObject.Parse(responseBD.Content);
            foreach (JObject _bd in jsonBD["imdata"])
            {
                string Prefix = "";
                string BD = _bd["fvBD"]["attributes"]["name"].ToString();
                Console.WriteLine(BD);
                string[] ip = BD.Split('_');

                /*find prefix in bd name*/
                for (int i = 0; i < ip.Length; i++)
                {
                    IPAddress _ip = null;

                    if (IPAddress.TryParse(ip[i], out _ip))
                    {
                        try
                        {
                            Prefix = ip[i] + "_" + ip[i + 1];
                        }
                        catch { }
                        break;
                    }

                }

                if (dictPrefixData.ContainsKey(Prefix))
                {
                    dictPrefixData[Prefix][0] = BD;


                }
                else
                {

                    dictPrefixData.Add(Prefix, new string[3] { BD, "", "" });
                }
                if (dictBDPrefix.ContainsKey(BD))
                {
                    dictBDPrefix[BD] = Prefix;

                }
                else
                {
                    dictBDPrefix.Add(BD, Prefix);
                }
            }
            /*get application profile of bd*/
            apicClient.BaseUrl = new System.Uri("https://" + apicIP + "/api/node/mo/uni/tn-" + tenants + ".json?query-target=children&target-subtree-class=fvAp");
            request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            IRestResponse responseAP = apicClient.Execute(request);
            JObject jsonAP = JObject.Parse(responseAP.Content);

            foreach (JObject _ap in jsonAP["imdata"])
            {
                string AP = _ap["fvAp"]["attributes"]["name"].ToString();
                Console.WriteLine("ap = " + AP);
                /*get all EPG of Prefix*/
                apicClient.BaseUrl = new System.Uri("https://" + apicIP + "/api/node/mo/uni/tn-" + tenants + "/ap-" + AP + ".json?query-target=subtree&target-subtree-class=fvAEPg");
                request = new RestRequest(Method.GET);
                request.AddHeader("cache-control", "no-cache");
                IRestResponse responseEPG = apicClient.Execute(request);
                JObject jsonEPG = JObject.Parse(responseEPG.Content);
                foreach (JObject _EPG in jsonEPG["imdata"])
                {
                    try
                    {

                        string EPG = _EPG["fvAEPg"]["attributes"]["name"].ToString();
                        Console.WriteLine("--epg=" + EPG);
                        foreach (string _prefix in dictPrefixData.Keys)
                        {
                            if (EPG.Contains(_prefix) == true)
                            {
                                if (dictPrefixData[_prefix][1] != "") //ap
                                {

                                    dictPrefixData[_prefix][1] = dictPrefixData[_prefix][1] + "," + AP;
                                    dictPrefixData[_prefix][2] = dictPrefixData[_prefix][2] + "," + EPG;

                                }
                                else
                                {
                                    dictPrefixData[_prefix][1] = AP;
                                    dictPrefixData[_prefix][2] = EPG;
                                }

                            }
                        }

                    }
                    catch { }
                }
            }



        }
        private void UpdateNewBDData(string bd)
        {
            string prefix = "";
            if (!dictBDPrefix.ContainsKey(bd))
            {

                string[] ip = bd.Split('_');

                /*find prefix in bd name*/
                for (int i = 0; i < ip.Length; i++)
                {
                    IPAddress _ip = null;

                    if (IPAddress.TryParse(ip[i], out _ip))
                    {
                        try
                        {
                            prefix = ip[i] + "_" + ip[i + 1];
                        }
                        catch { }
                        break;
                    }

                }
                dictBDPrefix.Add(bd, prefix);
            }
            else { prefix = dictBDPrefix[bd]; }
            if (!dictPrefixData.ContainsKey(prefix))
            {

                dictPrefixData.Add(prefix, new string[3] { bd, "", "" });
            }
            /*get application profile of bd*/
            apicClient.BaseUrl = new System.Uri("https://" + apicIP + "/api/node/mo/uni/tn-" + tenants + ".json?query-target=children&target-subtree-class=fvAp");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            IRestResponse responseAP = apicClient.Execute(request);
            JObject jsonAP = JObject.Parse(responseAP.Content);

            foreach (JObject _ap in jsonAP["imdata"])
            {
                string AP = _ap["fvAp"]["attributes"]["name"].ToString();
                Console.WriteLine("ap = " + AP);
                /*get all EPG in AP*/
                apicClient.BaseUrl = new System.Uri("https://" + apicIP + "/api/node/mo/uni/tn-" + tenants + "/ap-" + AP + ".json?query-target=subtree&target-subtree-class=fvAEPg");
                request = new RestRequest(Method.GET);
                request.AddHeader("cache-control", "no-cache");
                IRestResponse responseEPG = apicClient.Execute(request);
                JObject jsonEPG = JObject.Parse(responseEPG.Content);
                foreach (JObject _EPG in jsonEPG["imdata"])
                {
                    try
                    {

                        string EPG = _EPG["fvAEPg"]["attributes"]["name"].ToString();
                        Console.WriteLine("--epg=" + EPG);

                        if (EPG.Contains(prefix) == true)
                        {
                            if (dictPrefixData[prefix][1] != "") //ap
                            {
                                dictPrefixData[prefix][1] = dictPrefixData[prefix][1] + "," + AP;
                                dictPrefixData[prefix][2] = dictPrefixData[prefix][2] + "," + EPG;
                            }
                            else
                            {
                                dictPrefixData[prefix][1] = AP;
                                dictPrefixData[prefix][2] = EPG;
                            }

                        }
                    }
                    catch { }
                }

            }
        }
        private bool loginApic()
        {
            apicClient = new RestClient("https://" + apicIP + "/api/aaaLogin.json");
            apicClient.Timeout = 2000;
            apicClient.CookieContainer = new System.Net.CookieContainer();
            ServicePointManager.ServerCertificateValidationCallback += (RestClient, certificate, chain, sslPolicyErrors) => true;
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json", "{\r\n  \"aaaUser\" : {\r\n    \"attributes\" : {\r\n      \"name\" : \"" + apicUsername + "\",\r\n  " +
                "    \"pwd\" : \"" + apicPassword + "\"\r\n    }\r\n  }\r\n}", ParameterType.RequestBody);
            IRestResponse response = apicClient.Execute(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                return true;
            }
            else
            {

                return false;
            }
        }

        #region<update BD L3out apic>
        private void bgw_apicBDUpdate_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                BackgroundWorker worker = sender as BackgroundWorker;
                while (state && !e.Cancel)
                {
                    if (worker.CancellationPending)
                    {
                        e.Cancel = true;
                        return;
                    }
                    apicClient.BaseUrl = new System.Uri("https://" + apicIP + "/api/aaaRefresh.json");
                    apicClient.Timeout = 2000;
                    var request = new RestRequest(Method.GET);
                    request.AddHeader("cache-control", "no-cache");
                    IRestResponse response = apicClient.Execute(request);
                    if (response.StatusDescription == "Forbidden" || response.StatusDescription == null)
                    {
                        bool isLogin = loginApic();

                        if (!isLogin)
                        {
                            Log.log("Error", "Apic (" + apicIP + ") cannot connect.");
                            SendMessage("\U00002757 ACI: Lost Connection\\nIP:" + apicIP);
                            //    MessageBox.Show("ไม่สามารถเชื่อมต่อกับ APIC ได้ กรุณาตรวจสอบการเชื่อมต่อด้วยค่ะ");
                        }
                        else
                        {
                            SendMessage("\U00002705 ACI: Reconnect Success! ");
                        }
                    }
                    else
                    {
                        try
                        {
                            UpdateBDSite();
                        }
                        catch { Log.log("Error", "cannot updateBDsite"); }

                    }
                    Thread.Sleep(snmpTimeSleep);
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message +" : bd update");
            }

        }
        #endregion

        #region<function update bd of each site>
        private void UpdateBDSite()
        {
            try
            {
                apicClient.BaseUrl = new System.Uri("https://" + apicIP + "/api/node/mo/uni/tn-" + tenants + ".json?query-target=children&target-subtree-class=fvBD");
                var request = new RestRequest(Method.GET);
                request.AddHeader("cache-control", "no-cache");
                IRestResponse response1 = apicClient.Execute(request);
                JObject data = JObject.Parse(response1.Content);
                string resultStringBTT = "";
                string resultStringTST = "";
                foreach (JObject test in data["imdata"])
                {
                    string BD = test["fvBD"]["attributes"]["name"].ToString();
                    Console.WriteLine(BD);
                    apicClient.BaseUrl = new System.Uri("https://" + apicIP + "/api/node/mo/uni/tn-" + tenants + "/BD-" + BD + ".json?query-target=children&target-subtree-class=fvRsBDToOut&target-subtree-class=fvRsCtx");
                    request = new RestRequest(Method.GET);
                    request.AddHeader("cache-control", "no-cache");
                    response1 = apicClient.Execute(request);
                    JObject jsonBD = JObject.Parse(response1.Content);
                    string vrf = jsonBD["imdata"][0]["fvRsCtx"]["attributes"]["tnFvCtxName"].ToString();
                    string L3Out = "";
                    if (!dictBDPrefix.ContainsKey(BD))
                    {
                        UpdateNewBDData(BD);
                    }
                    try
                    {



                        L3Out = jsonBD["imdata"][1]["fvRsBDToOut"]["attributes"]["tnL3extOutName"].ToString();
                    }
                    catch { }

                    string prefixBD = dictBDPrefix[BD];
                    string ap = "";
                    string epg = "";

                    string consumeContracts = "";
                    string provideContracts = "";
                    if (dictPrefixData.ContainsKey(prefixBD))
                    {/*dictBDData 0 = BD Name , 1 = AP , 2 = EPG*/
                        ap = dictPrefixData[prefixBD][1];
                        epg = dictPrefixData[prefixBD][2];
                    }
                    /*get contracts*/
                    if (ap != "" && epg != "")
                    {
                        apicClient.BaseUrl = new System.Uri("https://" + apicIP + "/api/node/mo/uni/tn-" + tenants + "/ap-" + ap + "/epg-" + epg + ".json?query-target=subtree&target-subtree-class=fvRsCons");
                        var requestContracts = new RestRequest(Method.GET);
                        request.AddHeader("cache-control", "no-cache");
                        IRestResponse responseConsumeContracts = apicClient.Execute(request);
                        JObject jsonConsumesContracts = JObject.Parse(responseConsumeContracts.Content);

                        try
                        {
                            consumeContracts = jsonConsumesContracts["imdata"][0]["fvRsCons"]["attributes"]["tnVzBrCPName"].ToString();
                        }
                        catch (Exception ex)
                        {

                        }
                        apicClient.BaseUrl = new System.Uri("https://" + apicIP + "/api/node/mo/uni/tn-" + tenants + "/ap-" + ap + "/epg-" + epg + ".json?query-target=subtree&target-subtree-class=fvRsProv");
                        var requestProvideContracts = new RestRequest(Method.GET);

                        request.AddHeader("cache-control", "no-cache");
                        IRestResponse responseProvideContracts = apicClient.Execute(request);
                        JObject jsonProvideContracts = JObject.Parse(responseProvideContracts.Content);

                        try
                        {
                            provideContracts = jsonProvideContracts["imdata"][0]["fvRsProv"]["attributes"]["tnVzBrCPName"].ToString();
                        }
                        catch { }

                    }
                    else
                    {
                        UpdateNewBDData(BD);
                    }

                    #region<code>
                    string code = "";
                    #region<L3out>
                    if (L3Out == L3BTT)
                    {
                        code += "B";

                    }
                    else if (L3Out == L3TST)
                    {
                        code += "T";
                    }
                    else
                    {
                        code += "0";
                    }
                    #endregion
                    #region<VRF>

                    if (vrf == vrfBTT)
                    {
                        code += "B";
                    }
                    else if (vrf == vrfTST)
                    {
                        code += "T";
                    }
                    else
                    {
                        code += "0";
                    }
                    #endregion
                    #region<Provide Contract>
                    if (provideContracts == contractsBTT)
                    {
                        code += "B";
                    }
                    else if (provideContracts == contractsTST)
                    {
                        code += "T";
                    }
                    else
                    {
                        code += "0";
                    }
                    #endregion
                    #region<Consume Contract>
                    if (consumeContracts == contractsBTT)
                    {
                        code += "B";
                    }
                    else if (consumeContracts == contractsTST)
                    {
                        code += "T";
                    }
                    else { code += "0"; }
                    #endregion
                    #endregion

                    if (!dictBDSiteCode.ContainsKey(BD))
                    {
                        dictBDSiteCode.Add(BD, new string[] { "no", code });

                    }

                    dictBDSiteCode[BD][1] = code;
                    if (dictBDSiteCode[BD][0] == "no")
                    {
                        int countB = 0;
                        int countT = 0;
                        int count0 = 0;
                        string site = "";
                        foreach (char c in code)
                        {
                            if (c == 'B')
                            {
                                countB++;
                            }
                            else if (c == 'T')
                            {
                                countT++;
                            }
                            else if (c == '0')
                            {
                                count0++;
                            }
                        }
                        if (countB > countT && countB > count0)
                        {
                            site = "BTT";
                        }
                        else if (countT > countB && countT > count0)
                        {
                            site = "TST";
                        }
                        else if (count0 > countB && count0 > countT)
                        {
                            site = "None";
                        }
                        else if (countT == countB && count0 == 0)
                        {

                            site = "TST,BTT";
                        }
                        else if (countB == count0 && countT == 0)
                        {

                            site = "BTT";
                        }
                        else if (countT == count0 && countB == 0)
                        {

                            site = "TST";
                        }
                        if (site == "BTT")
                        {
                            resultStringBTT = resultStringBTT + BD + ":-" + code + System.Environment.NewLine;
                            dictBDSiteCode[BD][0] = "BTT";
                        }
                        else if (site == "TST")
                        {
                            resultStringTST = resultStringTST + BD + ":-" + code + System.Environment.NewLine;
                            dictBDSiteCode[BD][0] = "TST";
                        }
                    }
                    else
                    {
                        if (dictBDSiteCode[BD][0] == "BTT")
                        {
                            if (code == "BBBB")
                            { //change complete
                                resultStringBTT = resultStringBTT + BD + ":-" + code + System.Environment.NewLine;

                            }
                            else
                            { //change not complete
                              //  dictBDSiteCode[BD][0] = "TST";
                                resultStringTST = resultStringTST + BD + ":-" + code + System.Environment.NewLine;
                            }
                        }
                        else if (dictBDSiteCode[BD][0] == "TST")
                        {

                            if (code == "TTTT")
                            { //change complete
                                resultStringTST = resultStringTST + BD + ":-" + code + System.Environment.NewLine;
                            }
                            else
                            { //change not complete
                              //  dictBDSiteCode[BD][0] = "TST";
                                resultStringBTT = resultStringBTT + BD + ":-" + code + System.Environment.NewLine;
                            }
                        }
                        dictBDSiteCode[BD][1] = code;
                    }
                    //if (dictBDSiteCode[BD] == "BTT")
                    //{
                    //    resultStringBTT = resultStringBTT + BD +":-"+ code + System.Environment.NewLine;
                    //}
                    //else if (dictBDSite[BD] == "TST")
                    //{
                    //    resultStringTST = resultStringTST + BD + ":-"+code + System.Environment.NewLine;
                    //}



                    //if (dictBDSiteCode[BD][0] == "BTT")
                    //{
                    //    resultStringBTT = resultStringBTT + BD + ":-" + code + System.Environment.NewLine;
                    //}
                    //else if (dictBDSiteCode[BD][0] == "TST")
                    //{
                    //    resultStringTST = resultStringTST + BD + ":-" + code + System.Environment.NewLine;
                    //}
                    //else if (dictBDSiteCode[BD][0] == "None")
                    //{
                    //    dictBDSiteCode[BD][0] = site;
                    //}



                }

                tb_BTT.BeginInvoke(new MethodInvoker(delegate {
                    tb_BTT.Text = resultStringBTT;
                    tb_BTT.Refresh();

                }));
                tb_TST.BeginInvoke(new MethodInvoker(delegate {
                    tb_TST.Text = resultStringTST;
                    tb_TST.Refresh();

                }));


                //dtg_BTT.BeginInvoke(new MethodInvoker(delegate
                //{
                //   // dtg_BTT.DataSource = dt_BTT;
                //    dtg_BTT.Refresh();

                //}));
                //dtg_TST.BeginInvoke(new MethodInvoker(delegate
                //{
                //   // dtg_TST.DataSource = dt_TST;
                //    dtg_TST.Refresh();

                //}));

            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message+": Update bd site");
            }
        }
        #endregion

        #region<result prefix route from snmp  *textchanged>
        private void tb_sladin1Snmp_TextChanged(object sender, EventArgs e)
        {
            // dictPrefixStatusSladin01.Clear();
            string text = tb_sladin1Snmp.Text;
            if (text == "Change")
            {

            }
            else
            {
                text = text.Replace("\r", "");
                string[] prefix = text.Split('\n');
                for (int i = 0; i < prefix.Length - 1; i++)
                {
                    string[] ipStatus = prefix[i].Split(':');
                    if (!dictPrefixStatusSladin01.ContainsKey(ipStatus[0]))
                    {


                        dictPrefixStatusSladin01.Add(ipStatus[0], ipStatus[1]);


                    }
                    else
                    {
                        dictPrefixStatusSladin01[ipStatus[0]] = ipStatus[1];


                    }

                }
            }
            isSladin01Changed = true;
        }
        private void tb_sladin2Snmp_TextChanged(object sender, EventArgs e)
        {
            //  dictPrefixStatusSladin02.Clear();
            string text = tb_sladin2Snmp.Text;
            if (text == "Change")
            {

            }
            else
            {
                text = text.Replace("\r", "");
                string[] prefix = text.Split('\n');
                for (int i = 0; i < prefix.Length - 1; i++)
                {
                    string[] ipStatus = prefix[i].Split(':');
                    if (!dictPrefixStatusSladin02.ContainsKey(ipStatus[0]))
                    {

                        dictPrefixStatusSladin02.Add(ipStatus[0], ipStatus[1]);
                    }
                    else
                    {
                        dictPrefixStatusSladin02[ipStatus[0]] = ipStatus[1];
                    }

                }
            }
            isSladin02Changed = true;
        }

        private void tb_sniper1Snmp_TextChanged(object sender, EventArgs e)
        {
            //  dictPrefixStatusSniper01.Clear();
            string text = tb_sniper1Snmp.Text;

            if (text == "Change")
            {

            }
            else
            {
                text = text.Replace("\r", "");
                string[] prefix = text.Split('\n');
                for (int i = 0; i < prefix.Length - 1; i++)
                {
                    string[] ipStatus = prefix[i].Split(':');
                    if (!dictPrefixStatusSniper01.ContainsKey(ipStatus[0]))
                    {

                        dictPrefixStatusSniper01.Add(ipStatus[0], ipStatus[1]);
                    }
                    else
                    {

                        dictPrefixStatusSniper01[ipStatus[0]] = ipStatus[1];

                    }

                }

            }
            isSniper01Changed = true;

        }

        private void tb_sniper2Snmp_TextChanged(object sender, EventArgs e)
        {
            // dictPrefixStatusSniper02.Clear();
            string text = tb_sniper2Snmp.Text;
            if (text == "Change")
            {

            }
            else
            {
                text = text.Replace("\r", "");
                string[] prefix = text.Split('\n');
                for (int i = 0; i < prefix.Length - 1; i++)
                {
                    string[] ipStatus = prefix[i].Split(':');
                    if (!dictPrefixStatusSniper02.ContainsKey(ipStatus[0]))
                    {

                        dictPrefixStatusSniper02.Add(ipStatus[0], ipStatus[1]);
                    }
                    else
                    {
                        dictPrefixStatusSniper02[ipStatus[0]] = ipStatus[1];

                    }

                }
            }
            isSniper02Changed = true;
        }

        #endregion

        #region<monitor prefix>
        private void bgw_monitorPrefix_DoWork(object sender, DoWorkEventArgs e)
        {
            bool isFirstSladin01Port1 = true;
            bool isFirstSladin01Port2 = true;
            bool isFirstSladin02Port1 = true;
            bool isFirstSladin02Port2 = true;
            bool isFirstSniper01Port1 = true;
            bool isFirstSniper01Port2 = true;
            bool isFirstSniper02Port1 = true;
            bool isFirstSniper02Port2 = true;

            Dictionary<string, int> dictErrorPrefixTime = new Dictionary<string, int>();
            int downInterfaceTime = 0;
            //Dictionary<string, bool> dictErrorPrefixMove = new Dictionary<string, bool>();
            List<string> errorPrefixMove = new List<string>();
            BackgroundWorker worker = sender as BackgroundWorker;
            while (state && !e.Cancel)
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                    return;
                }
                if (this.bgw_monitorPrefix.CancellationPending)
                {
                    //   MessageBox.Show("e cancle");
                    e.Cancel = true;
                    break;
                }
                bool interfaceDown = false;

                string downSite = "";
                #region<interface aci>

                if (saladin01AdminState1 == "down" && saladin01AdminState2 == "down"
                             && saladin02AdminState1 == "down" && saladin02AdminState2 == "down")
                {

                    interfaceDown = true;
                    downSite = "TST";
                }
                else if (sniper01AdminState1 == "down" && sniper01AdminState2 == "down"
                    && sniper02AdminState1 == "down" && sniper02AdminState2 == "down")
                {

                    interfaceDown = true;
                    downSite = "BTT";
                }
                else
                {
                    if (downInterfaceTime > 0)
                    {
                        downInterfaceTime = 0;
                    }

                }

                if (downSite != "TST")
                {
                    if (saladin01AdminState1 == "down")
                    {
                        if (isFirstSladin01Port1)
                        {
                            SendMessage("interface sladin01 port1 is down");
                            isFirstSladin01Port1 = false;
                        }
                    }
                    else if (saladin01AdminState1 == "up")
                    {
                        if (!isFirstSladin01Port1)
                        {
                            SendMessage("interface sladin01 port1 is up");
                            isFirstSladin01Port1 = true;
                        }
                    }


                    if (saladin01AdminState2 == "down")
                    {
                        if (isFirstSladin01Port2)
                        {
                            SendMessage("interface sladin01 port2 is down");
                            isFirstSladin01Port2 = false;
                        }
                    }
                    else if (saladin01AdminState2 == "up")
                    {
                        if (!isFirstSladin01Port2)
                        {
                            isFirstSladin01Port2 = true;
                            SendMessage("interface sladin01 port2 is up");
                        }
                    }


                    if (saladin02AdminState1 == "down")
                    {
                        if (isFirstSladin02Port1)
                        {
                            SendMessage("interface sladin02 port1 is down");
                            isFirstSladin02Port1 = false;
                        }
                    }
                    else if (saladin02AdminState1 == "up")
                    {
                        if (!isFirstSladin02Port1)
                        {
                            isFirstSladin02Port1 = true;
                            SendMessage("interface sladin02 port1 is up");
                        }
                    }


                    if (saladin02AdminState2 == "down")
                    {
                        if (isFirstSladin02Port2)
                        {
                            SendMessage("interface sladin02 port2 is down");
                            isFirstSladin02Port2 = false;
                        }
                    }
                    else if (saladin02AdminState2 == "up")
                    {
                        if (!isFirstSladin02Port2)
                        {
                            isFirstSladin02Port2 = true;
                            SendMessage("interface sladin02 port2 is up");
                        }
                    }
                }


                if (downSite != "BTT")
                {
                    if (sniper01AdminState1 == "down")
                    {
                        if (isFirstSniper01Port1)
                        {
                            SendMessage("interface sniper01 port1 is down");
                            isFirstSniper01Port1 = false;
                        }
                    }
                    else if (sniper01AdminState1 == "up")
                    {
                        if (!isFirstSniper01Port1)
                        {
                            isFirstSniper01Port1 = true;
                            SendMessage("interface sniper01 port1 is up");
                        }
                    }


                    if (sniper01AdminState2 == "down")
                    {
                        if (isFirstSniper01Port2)
                        {
                            SendMessage("interface sniper01 port2 is down");
                            isFirstSniper01Port2 = false;
                        }
                    }
                    else if (sniper01AdminState2 == "up")
                    {
                        if (!isFirstSniper01Port2)
                        {
                            isFirstSniper01Port2 = true;
                            SendMessage("interface sniper01 port2 is up");
                        }
                    }



                    if (sniper02AdminState1 == "down")
                    {
                        if (isFirstSniper02Port1)
                        {
                            SendMessage("interface sniper02 port1 is down");
                            isFirstSniper02Port1 = false;
                        }
                    }
                    else if (sniper02AdminState1 == "up")
                    {
                        if (!isFirstSniper02Port1)
                        {
                            isFirstSniper02Port1 = true;
                            SendMessage("interface sniper02 port1 is up");
                        }
                    }


                    if (sniper02AdminState2 == "down")
                    {

                        if (isFirstSniper02Port2)
                        {
                            SendMessage("interface sniper02 port2 is down");
                            isFirstSniper02Port2 = false;
                        }
                    }
                    else if (sniper02AdminState2 == "up")
                    {
                        if (!isFirstSniper02Port2)
                        {
                            isFirstSniper02Port2 = true;
                            SendMessage("interface sniper02 port2 is up");
                        }
                    }
                }
                #endregion


                if (isSladin01Changed || isSladin02Changed || isSniper01Changed || isSniper02Changed || isUpdatePrefixList)
                {

                    if (prefixList != null && (prefixList.Length == dictPrefixStatusSladin01.Count) && (prefixList.Length == dictPrefixStatusSladin02.Count)
                        && (prefixList.Length == dictPrefixStatusSniper01.Count) && (prefixList.Length == dictPrefixStatusSniper02.Count))
                    {


                        for (int i = 0; i < prefixList.Length; i++)
                        {
                            bool havePrefix = false;

                            int row = dictPrefixIndex[prefixList[i]];

                            if (dictPrefixStatusSladin01[prefixList[i]].Equals("1") || dictPrefixStatusSladin02[prefixList[i]].Equals("1"))
                            {
                                if (dictErrorPrefixTime.ContainsKey(prefixList[i]))
                                {
                                    if (errorPrefixMove.Contains(prefixList[i]))
                                    {
                                        errorPrefixMove.Remove(prefixList[i]);
                                    }
                                    Log.log("Debug", "prefix: " + prefixList[i] + " have route in TST");
                                    SendMessage("\U00002705 prefix: " + prefixList[i] + " have route in TST");
                                    dictErrorPrefixTime.Remove(prefixList[i]);
                                }
                                Image imgStatus = Properties.Resources.normal_icon;
                                DataGridViewImageCell cellStatusImg = new DataGridViewImageCell();
                                cellStatusImg.Value = imgStatus;
                                dtg_prefixList.BeginInvoke(new MethodInvoker(delegate {
                                    dtg_prefixList.Rows[row].Cells[3] = cellStatusImg;
                                    dtg_prefixList.Rows[row].Cells[2].Value = "TST";
                                }));
                                havePrefix = true;

                            }
                            if (dictPrefixStatusSniper01[prefixList[i]].Equals("1") || dictPrefixStatusSniper02[prefixList[i]].Equals("1"))
                            {
                                if (dictErrorPrefixTime.ContainsKey(prefixList[i]))
                                {

                                    if (errorPrefixMove.Contains(prefixList[i]))
                                    {
                                        errorPrefixMove.Remove(prefixList[i]);
                                        //Log.log("Debug", "prefix: " + prefixList[i] + " have route in BTT");
                                        //SendMessage(testLineUser, "\U00002705 prefix: " + prefixList[i] + " have route in BTT");
                                    }
                                    Log.log("Debug", "prefix: " + prefixList[i] + " have route in BTT");
                                    SendMessage("\U00002705 prefix: " + prefixList[i] + " have route in BTT");
                                    dictErrorPrefixTime.Remove(prefixList[i]);
                                }
                                Image imgStatus = Properties.Resources.normal_icon;
                                DataGridViewImageCell cellStatusImg = new DataGridViewImageCell();
                                cellStatusImg.Value = imgStatus;

                                if (havePrefix)
                                {
                                    dtg_prefixList.BeginInvoke(new MethodInvoker(delegate {
                                        dtg_prefixList.Rows[row].Cells[3] = cellStatusImg;
                                        dtg_prefixList.Rows[row].Cells[2].Value = "TST,BTT";
                                    }));
                                }
                                else
                                {
                                    dtg_prefixList.BeginInvoke(new MethodInvoker(delegate {
                                        dtg_prefixList.Rows[row].Cells[3] = cellStatusImg;
                                        dtg_prefixList.Rows[row].Cells[2].Value = "BTT";
                                    }));
                                }
                                havePrefix = true;

                            }
                            if (!havePrefix)
                            {
                                if (dictPrefixStatusSladin01[prefixList[i]].Equals("n") && dictPrefixStatusSladin02[prefixList[i]].Equals("n")
                                    && dictPrefixStatusSniper01[prefixList[i]].Equals("n") && dictPrefixStatusSniper02[prefixList[i]].Equals("n"))
                                {
                                    if (!dictErrorPrefixTime.ContainsKey(prefixList[i]))
                                    {
                                        dictErrorPrefixTime.Add(prefixList[i], 0);
                                    }
                                    Image imgStatus = Properties.Resources.critical_icon;
                                    DataGridViewImageCell cellStatusImg = new DataGridViewImageCell();
                                    cellStatusImg.Value = imgStatus;
                                    //not found
                                    dtg_prefixList.BeginInvoke(new MethodInvoker(delegate {
                                        dtg_prefixList.Rows[row].Cells[3] = cellStatusImg;
                                        dtg_prefixList.Rows[row].Cells[2].Value = "-";
                                    }));
                                }
                                if (dictPrefixStatusSladin01[prefixList[i]].Equals("0") && dictPrefixStatusSladin02[prefixList[i]].Equals("0")
                                  && dictPrefixStatusSniper01[prefixList[i]].Equals("0") && dictPrefixStatusSniper02[prefixList[i]].Equals("0"))
                                {
                                    if (!dictErrorPrefixTime.ContainsKey(prefixList[i]))
                                    {
                                        dictErrorPrefixTime.Add(prefixList[i], 0);
                                    }
                                    Image imgStatus = Properties.Resources.warning_icon;
                                    DataGridViewImageCell cellStatusImg = new DataGridViewImageCell();
                                    cellStatusImg.Value = imgStatus;
                                    dtg_prefixList.BeginInvoke(new MethodInvoker(delegate {
                                        dtg_prefixList.Rows[row].Cells[3] = cellStatusImg;
                                        dtg_prefixList.Rows[row].Cells[2].Value = "-";

                                        //dtg_prefixList.Rows[row].Cells[2].Value = "TST";
                                    }));
                                    //prefix has losted

                                }

                            }

                        }
                        isSladin01Changed = false;
                        isSladin02Changed = false;
                        isSniper01Changed = false;
                        isSniper02Changed = false;
                        isUpdatePrefixList = false;

                    }
                }
                #region<auto move l3out>
                // if (autoMoveL3OUT == "on" && !interfaceDown)
                //  {
                //interface aci down

                foreach (string errorPrefix in dictErrorPrefixTime.Keys.ToList())
                {
                    dictErrorPrefixTime[errorPrefix] += 1;
                    if (dictErrorPrefixTime[errorPrefix] == 2)
                    {
                        SendMessage("\U0000274C Prefix no route on N7K Prefix: " + errorPrefix);
                    }
                    if (autoMoveL3OUT == "on")
                    {
                        if (!interfaceDown)
                        {
                            if (dictErrorPrefixTime[errorPrefix] % 3 == 0 && dictErrorPrefixTime[errorPrefix] < 30)
                            {
                                Log.log("Debug", "prefix: " + errorPrefix + " not have route");
                                //     int row = dictPrefixIndex[errorPrefix];
                                //    if(dtg_prefixList.Rows[row].Cells[2].Value.ToString() == "TST")
                                string[] prefixBD = errorPrefix.Split('.');
                                string errPrefix = prefixBD[0] + "." + prefixBD[1] + "." + prefixBD[2] + "." + prefixBD[3] + "_" + prefixBD[4];

                                if (dictPrefixData.ContainsKey(errPrefix))
                                {
                                    string bd = dictPrefixData[errPrefix][0];
                                    string ap = dictPrefixData[errPrefix][1];
                                    string epg = dictPrefixData[errPrefix][2];
                                    string siteOld = dictBDSiteCode[bd][0];
                                    string siteNew = "";
                                    if (siteOld == "BTT")
                                    {

                                        siteNew = "TST";
                                    }
                                    else if (siteOld == "TST")
                                    {
                                        siteNew = "BTT";
                                    }
                                    dictBDSiteCode[bd][0] = siteNew;
                                    Log.log("Debug", "Start Move L3Out BD: " + bd + "(" + errorPrefix + ")" + " from " + siteOld + " to " + siteNew);
                                    //  MessageBox.Show("เริ่มทำการย้าย L3Out auto ของ " + bd + " (" + errPrefix + ") " + "จาก " + siteOld + " >> " + siteNew);
                                    //  SendMesage(testLineUser, "เริ่มทำการย้าย L3Out auto ของ " + bd + "(" + errPrefix + ")\\n" + "จาก " + siteOld + " >> " + siteNew);
                                    SendMessage("\U000025B6 Start Auto move L3Out BD: " + bd + " Prefix: " + errPrefix + " " + "From " + siteOld + " >> " + siteNew);

                                    bool isMove = AutoMoveL3Out(bd, ap, epg, siteOld, siteNew);
                                    if (isMove)
                                    {
                                        dictErrorPrefixTime[errorPrefix] = 0;
                                        if (!errorPrefixMove.Contains(errorPrefix))
                                        {
                                            errorPrefixMove.Add(errorPrefix);
                                        }
                                        Log.log("Debug", "Move L3Out BD: " + bd + "(" + errorPrefix + ")" + " success!");
                                        SendMessage("\U00002705 Move L3Out Finish! Please wait,Checking route on N7K BD: " + bd + " Prefix: " + errorPrefix + " From " + siteOld + " >> " + siteNew);
                                    }
                                    else
                                    {
                                        dictBDSiteCode[bd][0] = siteOld;
                                        Log.log("Debug", "Move L3Out BD: " + bd + "(" + errorPrefix + ")" + " not success!");

                                        SendMessage("\U0000274C Auto Move L3Out Uncomplete!\\BD: " + bd + " Prefix: " + errPrefix + " From " + siteOld + " >> " + siteNew);
                                    }

                                }
                            }
                        }


                    }

                }
                if (interfaceDown)
                {
                    downInterfaceTime += 1;

                    if (downInterfaceTime % 3 == 0 && downInterfaceTime < 16)
                    {
                        if (downSite == "BTT")
                        {
                            SendMessage("all interface sniper is down!");
                        }
                        if (downSite == "TST")
                        {
                            SendMessage("all interface sladin is down!");
                        }
                        if (autoMoveL3OUT == "on")
                        {
                            bool isMoveAll = true;
                            foreach (string _bd in dictBDSiteCode.Keys)
                            {
                                if (dictBDSiteCode[_bd][0] == downSite)
                                {
                                    if (dictBDPrefix.ContainsKey(_bd))
                                    {
                                        string prefix = dictBDPrefix[_bd];
                                        string bd = dictPrefixData[prefix][0];
                                        string ap = dictPrefixData[prefix][1];
                                        string epg = dictPrefixData[prefix][2];
                                        string siteOld = dictBDSiteCode[bd][0];
                                        string siteNew = "";
                                        if (siteOld == "BTT")
                                        {

                                            siteNew = "TST";
                                        }
                                        else if (siteOld == "TST")
                                        {
                                            siteNew = "BTT";
                                        }
                                        dictBDSiteCode[bd][0] = siteNew;
                                        Log.log("Debug", "Start Move L3Out BD: " + bd + "(" + prefix + ")" + " from " + siteOld + " to " + siteNew);
                                        SendMessage("\U000025B6 Start Auto move L3Out BD: " + bd + " Prefix: " + prefix + " " + "From " + siteOld + " >> " + siteNew);

                                        bool isMove = AutoMoveL3Out(bd, ap, epg, siteOld, siteNew);
                                        if (isMove)
                                        {
                                            isMoveAll = isMoveAll && true;

                                            Log.log("Debug", "Move L3Out BD: " + bd + "(" + prefix + ")" + " success!");
                                            SendMessage("\U00002705 Move L3Out Finish! Please wait,Checking route on N7K BD: " + bd + " Prefix: " + prefix + " From " + siteOld + " >> " + siteNew);
                                        }
                                        else
                                        {
                                            isMoveAll = isMoveAll && false;
                                            dictBDSiteCode[bd][0] = siteOld;
                                            Log.log("Debug", "Move L3Out BD: " + bd + "(" + prefix + ")" + " not success!");

                                            SendMessage("\U0000274C Auto Move L3Out Uncomplete!\\BD: " + bd + " Prefix: " + prefix + " From " + siteOld + " >> " + siteNew);
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
                #endregion

                Thread.Sleep(snmpTimeSleep);
            }

        }


        #endregion

        private void MoveL3out(string bd, string appProfile, string epg, string siteOld, string siteNew)
        {
            string L3OutOld = "";
            string L3OutNew = "";
            string vrfNew = "";
            string contractsOld = "";
            string contractsNew = "";
            bool isFailed = false;

            //   string appProfile = "";
            if (siteOld == "BTT")
            {
                L3OutOld = L3BTT;
                contractsOld = contractsBTT;
            }
            else if (siteOld == "TST")
            {
                L3OutOld = L3TST;
                contractsOld = contractsTST;
            }

            if (siteNew == "BTT")
            {
                L3OutNew = L3BTT;
                vrfNew = vrfBTT;
                contractsNew = contractsBTT;
            }
            else if (siteNew == "TST")
            {
                L3OutNew = L3TST;
                vrfNew = vrfTST;
                contractsNew = contractsTST;
            }

            #region <move L3Out Associated>
            /*delete l3out*/
            string urlDel = "https://" + apicIP + "/api/node/mo/uni/tn-" + tenants + "/BD-" + bd + ".json";
            apicClient.BaseUrl = new System.Uri("https://" + apicIP + "/api/node/mo/uni/tn-" + tenants + "/BD-" + bd + ".json");
            var requestDelL3 = new RestRequest(Method.POST);
            requestDelL3.AddHeader("cache-control", "no-cache");
            requestDelL3.AddHeader("content-type", "application/json");
            string tnbd = "uni/tn-" + tenants + "/BD-" + bd;
            Console.WriteLine(tnbd);
            string commandDelete = "rsBDToOut-" + L3OutOld;
            string tnbdl3 = "uni/tn-" + tenants + "/BD-" + bd + "/" + commandDelete;
            Console.WriteLine(tnbdl3);
            requestDelL3.AddParameter("application/json", "{\"fvBD\":{\"attributes\":{\"dn\":\"uni/tn-" + tenants + "/BD-" + bd + "\",\"status\":\"modified\"},\"children\":[{\"fvRsBDToOut\":{\"attributes\":{\"dn\":\"uni/tn-" + tenants + "/BD-" + bd + "/" + commandDelete + "\",\"status\":\"deleted\"}}}]}}"
                                         , ParameterType.RequestBody);
            var responseDelL3 = apicClient.Execute(requestDelL3);
            if (responseDelL3.ResponseStatus == ResponseStatus.Completed)
            {
                progressBar1.Value = 15;
                lb_StatusUpdate.Text = bd + " delete L3Out (" + L3OutOld + ")";
                lb_StatusUpdate.Refresh();
                // progressBar1.BeginInvoke(new MethodInvoker(delegate { progressBar1.Value = 15; }));
                // lb_StatusUpdate.BeginInvoke(new MethodInvoker(delegate { lb_StatusUpdate.Text = bd+" delete L3Out ("+L3OutOld+")"; }));

            }
            if (responseDelL3.ErrorException != null)
            {
                MessageBox.Show("Fail! " + bd + " cannot delete L3Out (" + L3OutOld + ")");
                isFailed = true;
                lb_StatusUpdate.Text = "Fail!" + bd + " cannot delete L3Out(" + L3OutOld + ")";
                lb_StatusUpdate.Refresh();


            }
            else
            {
                Console.WriteLine(responseDelL3.Content);
            }
            /*add new l3out*/
            if (!isFailed)
            {
                apicClient.BaseUrl = new System.Uri("https://" + apicIP + "/api/node/mo/uni/tn-" + tenants + "/BD-" + bd + ".json");
                var requestAddL3 = new RestRequest(Method.POST);
                requestAddL3.AddHeader("cache-control", "no-cache");
                requestAddL3.AddHeader("content-type", "application/json");
                requestAddL3.AddParameter("application/json", "{\"fvRsBDToOut\":{\"attributes\":{\"tnL3extOutName\":\"" + L3OutNew + "\",\"status\":\"created\"}}}"
                                             , ParameterType.RequestBody);
                var responseAddL3 = apicClient.Execute(requestAddL3);
                if (responseAddL3.ResponseStatus == ResponseStatus.Completed)
                {
                    progressBar1.Value = 30;
                    lb_StatusUpdate.Text = bd + " add L3Out (" + L3OutNew + ").";
                    lb_StatusUpdate.Refresh();
                    // progressBar1.BeginInvoke(new MethodInvoker(delegate { progressBar1.Value = 30; }));
                    // lb_StatusUpdate.BeginInvoke(new MethodInvoker(delegate { lb_StatusUpdate.Text = bd +" add L3Out (" + L3OutNew + ")."; }));

                }
                if (responseAddL3.ErrorException != null)
                {
                    MessageBox.Show("Fail! " + bd + " canot add L3Out(" + L3OutNew + ").");
                    isFailed = true;
                    lb_StatusUpdate.Text = "Fail! " + bd + " cannot add L3Out (" + L3OutNew + ").";
                    lb_StatusUpdate.Refresh();
                }
            }
            #endregion

            #region<move VRF>
            // string urlvrf = ;
            if (!isFailed)
            {
                apicClient.BaseUrl = new System.Uri("https://" + apicIP + "/api/node/mo/uni/tn-" + tenants + "/BD-" + bd + "/rsctx.json");
                var requestMoveVRF = new RestRequest(Method.POST);
                requestMoveVRF.AddHeader("cache-control", "no-cache");
                requestMoveVRF.AddHeader("content-type", "application/json");
                requestMoveVRF.AddParameter("application/json", "{\"fvRsCtx\":{\"attributes\":{\"tnFvCtxName\":\"" + vrfNew + "\"}}}", ParameterType.RequestBody);
                var responseMoveVRF = apicClient.Execute(requestMoveVRF);
                if (responseMoveVRF.ResponseStatus == ResponseStatus.Completed)
                {
                    progressBar1.Value = 60;
                    lb_StatusUpdate.Text = bd + " change VRF (" + vrfNew + ").";
                    lb_StatusUpdate.Refresh();
                    //  progressBar1.BeginInvoke(new MethodInvoker(delegate { progressBar1.Value = 60; }));
                    //  lb_StatusUpdate.BeginInvoke(new MethodInvoker(delegate { lb_StatusUpdate.Text = bd + " change VRF (" + vrfNew + ")."; }));

                }
                if (responseMoveVRF.ErrorException != null)
                {
                    MessageBox.Show("Fail!" + bd + " cannot change VRF (" + vrfNew + ").");
                    isFailed = true;
                }
            }
            #endregion

            #region<move Contracts>
            /*delete contracts provide*/
            if (!isFailed)
            {
                apicClient.BaseUrl = new System.Uri("https://" + apicIP + "/api/node/mo/uni/tn-" + tenants + "/ap-" + appProfile + "/epg-" + epg + ".json");
                var requestDelProvide = new RestRequest(Method.POST);
                requestDelProvide.AddHeader("cache-control", "no-cache");
                requestDelProvide.AddHeader("content-type", "application/json");
                string commanddeleteprovide = "rsprov-" + contractsOld;
                string tnbdepgprovide = "uni/tn-" + tenants + "/ap-" + appProfile + "/epg-" + epg + "/" + commanddeleteprovide;
                Console.WriteLine(tnbdl3);
                requestDelProvide.AddParameter("application/json", "{\"fvAEPg\":{\"attributes\":{\"dn\":\"uni/tn-" + tenants + "/ap-" + appProfile + "/epg-" + epg + "\",\"status\":\"modified\"},\"children\":[{\"fvRsProv\":{\"attributes\":{\"dn\":\"" + tnbdepgprovide + "\",\"status\":\"deleted\"}}}]}}"
                                             , ParameterType.RequestBody);
                var responseDelProvide = apicClient.Execute(requestDelProvide);
                if (responseDelProvide.ResponseStatus == ResponseStatus.Completed)
                {
                    progressBar1.Value = 70;
                    lb_StatusUpdate.Text = bd + " delete provide contracts (" + contractsOld + ").";
                    lb_StatusUpdate.Refresh();
                    // progressBar1.BeginInvoke(new MethodInvoker(delegate { progressBar1.Value = 70; }));
                    // lb_StatusUpdate.BeginInvoke(new MethodInvoker(delegate { lb_StatusUpdate.Text = bd + " delete provide contracts (" + contractsOld + ")."; }));

                }
                if (responseDelProvide.ErrorException != null)
                {
                    MessageBox.Show("Fail! " + bd + " cannot delete provide contracts (" + contractsOld + ").");
                    isFailed = true;
                    lb_StatusUpdate.Text = "Fail! " + bd + " cannot delete provide contracts (" + contractsOld + ").";
                    lb_StatusUpdate.Refresh();
                }
            }
            /*delete contact consume*/

            if (!isFailed)
            {
                apicClient.BaseUrl = new System.Uri("https://" + apicIP + "/api/node/mo/uni/tn-" + tenants + "/ap-" + appProfile + "/epg-" + epg + ".json");
                var requestdeleteconsume = new RestRequest(Method.POST);
                requestdeleteconsume.AddHeader("cache-control", "no-cache");
                requestdeleteconsume.AddHeader("content-type", "application/json");
                string tnapepgcon = "uni/tn-" + tenants + "/ap-" + appProfile + "/epg-" + epg;
                string commanddeleteconsume = "rscons-" + contractsOld;
                string tnbdepgconsume = "uni/tn-" + tenants + "/ap-" + appProfile + "/epg-" + epg + "/" + commanddeleteconsume;
                requestdeleteconsume.AddParameter("application/json", "{\"fvAEPg\":{\"attributes\":{\"dn\":\"" + tnapepgcon + "\",\"status\":\"modified\"},\"children\":[{\"fvRsCons\":{\"attributes\":{\"dn\":\"" + tnbdepgconsume + "\",\"status\":\"deleted\"}}}]}}"
                                             , ParameterType.RequestBody);
                var responsedeleteconsume = apicClient.Execute(requestdeleteconsume);
                if (responsedeleteconsume.ResponseStatus == ResponseStatus.Completed)
                {
                    progressBar1.Value = 80;
                    lb_StatusUpdate.Text = bd + " delete consume contracts (" + contractsOld + ").";
                    lb_StatusUpdate.Refresh();
                    //progressBar1.BeginInvoke(new MethodInvoker(delegate { progressBar1.Value = 80; }));
                    // lb_StatusUpdate.BeginInvoke(new MethodInvoker(delegate { lb_StatusUpdate.Text = bd + " delete consume contracts (" + contractsOld + ")."; }));

                }
                if (responsedeleteconsume.ErrorException != null)
                {
                    MessageBox.Show("Fail! " + bd + " cannot delete consume contracts (" + contractsOld + ").");
                    isFailed = true;
                    lb_StatusUpdate.Text = "Fail! " + bd + " cannot delete consume contracts (" + contractsOld + ").";
                    lb_StatusUpdate.Refresh();
                }
            }
            /*create contact provide*/
            if (!isFailed)
            {
                apicClient.BaseUrl = new System.Uri("https://" + apicIP + "/api/node/mo/uni/tn-" + tenants + "/ap-" + appProfile + "/epg-" + epg + ".json");
                var requestcreateprovide = new RestRequest(Method.POST);
                requestcreateprovide.AddHeader("cache-control", "no-cache");
                requestcreateprovide.AddHeader("content-type", "application/json");
                requestcreateprovide.AddParameter("application/json", "{\"fvRsProv\":{\"attributes\":{\"tnVzBrCPName\":\"" + contractsNew + "\",\"status\":\"created,modified\"}}}"
                                             , ParameterType.RequestBody);
                var responsecreateprovide = apicClient.Execute(requestcreateprovide);
                if (responsecreateprovide.ResponseStatus == ResponseStatus.Completed)
                {
                    progressBar1.Value = 90;
                    lb_StatusUpdate.Text = bd + " create provide contracts (" + contractsNew + ").";
                    lb_StatusUpdate.Refresh();
                    // progressBar1.BeginInvoke(new MethodInvoker(delegate { progressBar1.Value = 90; }));
                    // lb_StatusUpdate.BeginInvoke(new MethodInvoker(delegate { lb_StatusUpdate.Text = bd + " create provide contracts (" + contractsNew + ")."; }));

                }
                if (responsecreateprovide.ErrorException != null)
                {
                    MessageBox.Show("Fail! " + bd + " cannot create provide contracts(" + contractsNew + ").");
                    isFailed = true;
                    lb_StatusUpdate.Text = "Fail! " + bd + " cannot create provide contracts(" + contractsNew + ").";
                    lb_StatusUpdate.Refresh();
                }
            }
            /*create contact consume*/
            if (!isFailed)
            {
                string urlcreateconsume = "https://" + apicIP + "/api/node/mo/uni/tn-" + tenants + "/ap-" + appProfile + "/epg-" + epg + ".json";
                apicClient.BaseUrl = new System.Uri(urlcreateconsume);
                var requestcreateconsume = new RestRequest(Method.POST);
                requestcreateconsume.AddHeader("cache-control", "no-cache");
                requestcreateconsume.AddHeader("content-type", "application/json");
                requestcreateconsume.AddParameter("application/json", "{\"fvRsCons\":{\"attributes\":{\"tnVzBrCPName\":\"" + contractsNew + "\",\"status\":\"created,modified\"}}}"
                                             , ParameterType.RequestBody);
                var responsecreateconsume = apicClient.Execute(requestcreateconsume);
                if (responsecreateconsume.ResponseStatus == ResponseStatus.Completed)
                {
                    progressBar1.Value = 100;
                    lb_StatusUpdate.Text = "Finish!";
                    lb_StatusUpdate.Refresh();
                    // progressBar1.BeginInvoke(new MethodInvoker(delegate { progressBar1.Value = 100; }));
                    // lb_StatusUpdate.BeginInvoke(new MethodInvoker(delegate { lb_StatusUpdate.Text = bd + " create consume contracts (" + contractsNew + ")."; }));
                    // lb_StatusUpdate.BeginInvoke(new MethodInvoker(delegate { lb_StatusUpdate.Text = "Finish!"; }));
                    Thread.Sleep(500);

                }
                if (responsecreateconsume.ErrorException != null)
                {
                    MessageBox.Show("Fail! " + bd + " cannot create consume contracts(" + contractsNew + ").");
                    isFailed = true;
                    lb_StatusUpdate.Text = "Fail! " + bd + " cannot create consume contracts(" + contractsNew + ").";
                    lb_StatusUpdate.Refresh();
                }
            }
            if (!isFailed)
            {
                progressBar1.Value = 0;
                lb_StatusUpdate.Text = "";
                lb_StatusUpdate.Refresh();
                MessageBox.Show("ทำการ Move L3out " + bd + " สำเร็จ" + System.Environment.NewLine +
                    "" + L3OutOld + " => " + L3OutNew);
            }

            //progressBar1.BeginInvoke(new MethodInvoker(delegate { progressBar1.Value = 0; }));
            //UpdateBDSite();
            #endregion


        }

        private bool AutoMoveL3Out(string bd, string appProfile, string epg, string siteOld, string siteNew)
        {
            bool isMove = false;
            string L3OutOld = "";
            string L3OutNew = "";
            string vrfNew = "";
            string contractsOld = "";
            string contractsNew = "";
            bool isFailed = false;

            //   string appProfile = "";
            if (siteOld == "BTT")
            {
                L3OutOld = L3BTT;
                contractsOld = contractsBTT;
            }
            else if (siteOld == "TST")
            {
                L3OutOld = L3TST;
                contractsOld = contractsTST;
            }

            if (siteNew == "BTT")
            {
                L3OutNew = L3BTT;
                vrfNew = vrfBTT;
                contractsNew = contractsBTT;
            }
            else if (siteNew == "TST")
            {
                L3OutNew = L3TST;
                vrfNew = vrfTST;
                contractsNew = contractsTST;
            }

            #region <move L3Out Associated>
            /*delete l3out*/
            string urlDel = "https://" + apicIP + "/api/node/mo/uni/tn-" + tenants + "/BD-" + bd + ".json";
            apicClient.BaseUrl = new System.Uri("https://" + apicIP + "/api/node/mo/uni/tn-" + tenants + "/BD-" + bd + ".json");
            var requestDelL3 = new RestRequest(Method.POST);
            requestDelL3.AddHeader("cache-control", "no-cache");
            requestDelL3.AddHeader("content-type", "application/json");
            string tnbd = "uni/tn-" + tenants + "/BD-" + bd;
            Console.WriteLine(tnbd);
            string commandDelete = "rsBDToOut-" + L3OutOld;
            string tnbdl3 = "uni/tn-" + tenants + "/BD-" + bd + "/" + commandDelete;
            Console.WriteLine(tnbdl3);
            requestDelL3.AddParameter("application/json", "{\"fvBD\":{\"attributes\":{\"dn\":\"uni/tn-" + tenants + "/BD-" + bd + "\",\"status\":\"modified\"},\"children\":[{\"fvRsBDToOut\":{\"attributes\":{\"dn\":\"uni/tn-" + tenants + "/BD-" + bd + "/" + commandDelete + "\",\"status\":\"deleted\"}}}]}}"
                                         , ParameterType.RequestBody);
            var responseDelL3 = apicClient.Execute(requestDelL3);
            if (responseDelL3.ResponseStatus == ResponseStatus.Completed)
            {
            }
            if (responseDelL3.ErrorException != null)
            {
                SendMessage("Auto Move Fail!\\n" + bd + " cannot delete L3Out(" + L3OutOld + ")");
                isFailed = true;
            }
            else
            {
                Console.WriteLine(responseDelL3.Content);
            }
            /*add new l3out*/
            if (!isFailed)
            {
                apicClient.BaseUrl = new System.Uri("https://" + apicIP + "/api/node/mo/uni/tn-" + tenants + "/BD-" + bd + ".json");
                var requestAddL3 = new RestRequest(Method.POST);
                requestAddL3.AddHeader("cache-control", "no-cache");
                requestAddL3.AddHeader("content-type", "application/json");
                requestAddL3.AddParameter("application/json", "{\"fvRsBDToOut\":{\"attributes\":{\"tnL3extOutName\":\"" + L3OutNew + "\",\"status\":\"created\"}}}"
                                             , ParameterType.RequestBody);
                var responseAddL3 = apicClient.Execute(requestAddL3);
                if (responseAddL3.ResponseStatus == ResponseStatus.Completed)
                {
                }
                if (responseAddL3.ErrorException != null)
                {
                    SendMessage("Auto Move Fail!\\n" + bd + " cannot delete L3Out(" + L3OutOld + ")");
                    isFailed = true;
                }
            }
            #endregion

            #region<move VRF>
            // string urlvrf = ;
            if (!isFailed)
            {
                apicClient.BaseUrl = new System.Uri("https://" + apicIP + "/api/node/mo/uni/tn-" + tenants + "/BD-" + bd + "/rsctx.json");
                var requestMoveVRF = new RestRequest(Method.POST);
                requestMoveVRF.AddHeader("cache-control", "no-cache");
                requestMoveVRF.AddHeader("content-type", "application/json");
                requestMoveVRF.AddParameter("application/json", "{\"fvRsCtx\":{\"attributes\":{\"tnFvCtxName\":\"" + vrfNew + "\"}}}", ParameterType.RequestBody);
                var responseMoveVRF = apicClient.Execute(requestMoveVRF);
                if (responseMoveVRF.ResponseStatus == ResponseStatus.Completed)
                {
                }
                if (responseMoveVRF.ErrorException != null)
                {
                    SendMessage("Auto Move Fail!\\n" + bd + " cannot change VRF (" + vrfNew + ").");
                    isFailed = true;
                }
            }
            #endregion

            #region<move Contracts>
            /*delete contracts provide*/
            if (!isFailed)
            {
                apicClient.BaseUrl = new System.Uri("https://" + apicIP + "/api/node/mo/uni/tn-" + tenants + "/ap-" + appProfile + "/epg-" + epg + ".json");
                var requestDelProvide = new RestRequest(Method.POST);
                requestDelProvide.AddHeader("cache-control", "no-cache");
                requestDelProvide.AddHeader("content-type", "application/json");
                string commanddeleteprovide = "rsprov-" + contractsOld;
                string tnbdepgprovide = "uni/tn-" + tenants + "/ap-" + appProfile + "/epg-" + epg + "/" + commanddeleteprovide;
                Console.WriteLine(tnbdl3);
                requestDelProvide.AddParameter("application/json", "{\"fvAEPg\":{\"attributes\":{\"dn\":\"uni/tn-" + tenants + "/ap-" + appProfile + "/epg-" + epg + "\",\"status\":\"modified\"},\"children\":[{\"fvRsProv\":{\"attributes\":{\"dn\":\"" + tnbdepgprovide + "\",\"status\":\"deleted\"}}}]}}"
                                             , ParameterType.RequestBody);
                var responseDelProvide = apicClient.Execute(requestDelProvide);
                if (responseDelProvide.ResponseStatus == ResponseStatus.Completed)
                {
                }
                if (responseDelProvide.ErrorException != null)
                {
                    SendMessage("Auto Move Fail!" + bd + " cannot delete provide contracts(" + contractsOld + ").");
                    isFailed = true;
                }
            }
            /*delete contact consume*/

            if (!isFailed)
            {
                apicClient.BaseUrl = new System.Uri("https://" + apicIP + "/api/node/mo/uni/tn-" + tenants + "/ap-" + appProfile + "/epg-" + epg + ".json");
                var requestdeleteconsume = new RestRequest(Method.POST);
                requestdeleteconsume.AddHeader("cache-control", "no-cache");
                requestdeleteconsume.AddHeader("content-type", "application/json");
                string tnapepgcon = "uni/tn-" + tenants + "/ap-" + appProfile + "/epg-" + epg;
                string commanddeleteconsume = "rscons-" + contractsOld;
                string tnbdepgconsume = "uni/tn-" + tenants + "/ap-" + appProfile + "/epg-" + epg + "/" + commanddeleteconsume;
                requestdeleteconsume.AddParameter("application/json", "{\"fvAEPg\":{\"attributes\":{\"dn\":\"" + tnapepgcon + "\",\"status\":\"modified\"},\"children\":[{\"fvRsCons\":{\"attributes\":{\"dn\":\"" + tnbdepgconsume + "\",\"status\":\"deleted\"}}}]}}"
                                             , ParameterType.RequestBody);
                var responsedeleteconsume = apicClient.Execute(requestdeleteconsume);
                if (responsedeleteconsume.ResponseStatus == ResponseStatus.Completed)
                {
                }
                if (responsedeleteconsume.ErrorException != null)
                {
                    SendMessage("Auto Move Fail! " + bd + " cannot delete consume contracts (" + contractsOld + ").");
                    isFailed = true;
                }
            }
            /*create contact provide*/
            if (!isFailed)
            {
                apicClient.BaseUrl = new System.Uri("https://" + apicIP + "/api/node/mo/uni/tn-" + tenants + "/ap-" + appProfile + "/epg-" + epg + ".json");
                var requestcreateprovide = new RestRequest(Method.POST);
                requestcreateprovide.AddHeader("cache-control", "no-cache");
                requestcreateprovide.AddHeader("content-type", "application/json");
                requestcreateprovide.AddParameter("application/json", "{\"fvRsProv\":{\"attributes\":{\"tnVzBrCPName\":\"" + contractsNew + "\",\"status\":\"created,modified\"}}}"
                                             , ParameterType.RequestBody);
                var responsecreateprovide = apicClient.Execute(requestcreateprovide);
                if (responsecreateprovide.ResponseStatus == ResponseStatus.Completed)
                {
                }
                if (responsecreateprovide.ErrorException != null)
                {
                    SendMessage("Auto Move Fail! " + bd + " cannot create provide contracts(" + contractsNew + ").");
                    isFailed = true;
                }
            }
            /*create contact consume*/
            if (!isFailed)
            {
                string urlcreateconsume = "https://" + apicIP + "/api/node/mo/uni/tn-" + tenants + "/ap-" + appProfile + "/epg-" + epg + ".json";
                apicClient.BaseUrl = new System.Uri(urlcreateconsume);
                var requestcreateconsume = new RestRequest(Method.POST);
                requestcreateconsume.AddHeader("cache-control", "no-cache");
                requestcreateconsume.AddHeader("content-type", "application/json");
                requestcreateconsume.AddParameter("application/json", "{\"fvRsCons\":{\"attributes\":{\"tnVzBrCPName\":\"" + contractsNew + "\",\"status\":\"created,modified\"}}}"
                                             , ParameterType.RequestBody);
                var responsecreateconsume = apicClient.Execute(requestcreateconsume);
                if (responsecreateconsume.ResponseStatus == ResponseStatus.Completed)
                {
                    Thread.Sleep(500);
                }
                if (responsecreateconsume.ErrorException != null)
                {
                    SendMessage("Auto Move Fail!\\n " + bd + " cannot create consume contracts(" + contractsNew + ").");
                    isFailed = true;
                }
            }
            if (!isFailed)
            {
                isMove = true;
            }
            if (siteNew == "TST")
            {

                isFakeSiteTST = true;
                isFakeSiteBTT = false;
            }
            else
            {

                isFakeSiteBTT = true;
                isFakeSiteTST = false;
            }
            #endregion

            return isMove;

        }

        private void Bt_moveTSTtoBTT_Click(object sender, EventArgs e)
        {

            if (dtg_TST.SelectedRows.Count > 0)
            {
                if (dtg_TST.SelectedRows[0].Cells[0].Value.ToString() != "")
                {
                    string bd = dtg_TST.SelectedRows[0].Cells[0].Value.ToString();
                    string epg = dtg_TST.SelectedRows[0].Cells[3].Value.ToString();
                    string ap = dtg_TST.SelectedRows[0].Cells[2].Value.ToString();
                    if (MessageBox.Show("ต้องการ move L3Out TST => BTT ที่ " + bd + " จริงๆใช่ไหมคะ?", "Confirm!!!",
                   MessageBoxButtons.OKCancel, MessageBoxIcon.Question,
                      MessageBoxDefaultButton.Button1) == System.Windows.Forms.DialogResult.OK)
                    {
                        this.Cursor = Cursors.WaitCursor;
                        bt_moveTSTtoBTT.BackColor = Color.Orange;
                        dictBDSiteCode[bd][0] = "BTT";
                        MoveL3out(bd, ap, epg, "TST", "BTT");

                        UpdateBDSite();
                        bt_moveTSTtoBTT.BackColor = Color.DarkSlateGray;
                        this.Cursor = Cursors.Default;
                    }
                }
                else
                {
                    MessageBox.Show("กรุณาเลือก Bridge Domain ฝั่ง TST ที่ต้องการจะย้าย");
                }
            }
            else
            {
                MessageBox.Show("กรุณาเลือก Bridge Domain ฝั่ง TST ที่ต้องการจะย้าย");
            }
        }

        private void Bt_moveBTTtoTST_Click(object sender, EventArgs e)
        {
            if (dtg_BTT.SelectedRows.Count > 0)
            {
                if (dtg_BTT.SelectedRows[0].Cells[0].Value.ToString() != "")
                {


                    string bd = dtg_BTT.SelectedRows[0].Cells[0].Value.ToString();
                    string epg = dtg_BTT.SelectedRows[0].Cells[3].Value.ToString();
                    string ap = dtg_BTT.SelectedRows[0].Cells[2].Value.ToString();
                    if (MessageBox.Show("ต้องการ move L3Out BTT => TST ที่ " + bd + " จริงๆใช่ไหมคะ?", "Confirm!!!",
                   MessageBoxButtons.OKCancel, MessageBoxIcon.Question,
                      MessageBoxDefaultButton.Button1) == System.Windows.Forms.DialogResult.OK)
                    {
                        this.Cursor = Cursors.WaitCursor;
                        bt_moveBTTtoTST.BackColor = Color.Orange;
                        dictBDSiteCode[bd][0] = "TST";
                        MoveL3out(bd, ap, epg, "BTT", "TST");

                        UpdateBDSite();
                        bt_moveBTTtoTST.BackColor = Color.DarkSlateBlue;
                        this.Cursor = Cursors.Default;
                    }
                }
                else { MessageBox.Show("กรุณาเลือก Bridge Domain ฝั่ง BTT ที่ต้องการจะย้าย"); }
            }
            else { MessageBox.Show("กรุณาเลือก Bridge Domain ฝั่ง BTT ที่ต้องการจะย้าย"); }
        }

        private void Tb_TST_TextChanged(object sender, EventArgs e)
        {
            if (tb_TST.Text != "")
            {
                string text = tb_TST.Text;

                text = text.Replace("\r", "");
                string[] BD = text.Split('\n');
                dtg_TST.Rows.Clear();
                int row = 0;
                for (int i = 0; i < BD.Length - 1; i++)
                {
                    string[] _BD = BD[i].Split(new string[] { ":-" }, StringSplitOptions.None); //split code and bd
                    string prefixBD = dictBDPrefix[_BD[0]];
                    string ap = "";
                    string epg = "";
                    if (dictPrefixData.ContainsKey(prefixBD))
                    {/*dictBDData 0 = BD Name , 1 = AP , 2 = EPG*/
                        ap = dictPrefixData[prefixBD][1];
                        epg = dictPrefixData[prefixBD][2];


                        dtg_TST.Rows.Add(_BD[0], "", ap, epg, prefixBD);

                        if (dictBDSiteCode[_BD[0]][1] == "TTTT")
                        {
                            Image imgStatus = Properties.Resources.normal_icon;
                            DataGridViewImageCell cellStatusImg = new DataGridViewImageCell();
                            cellStatusImg.Value = imgStatus;
                            dtg_TST.Rows[row].Cells[5] = cellStatusImg;
                        }
                        else
                        {
                            Image imgStatus = Properties.Resources.warning_icon;
                            DataGridViewImageCell cellStatusImg = new DataGridViewImageCell();
                            cellStatusImg.Value = imgStatus;
                            dtg_TST.Rows[row].Cells[5] = cellStatusImg;
                        }
                        row++;



                    }

                }
                dtg_TST.Refresh();
            }
            else
            {
                dtg_TST.Rows.Clear();
                dtg_TST.Refresh();
            }
        }

        private void Tb_BTT_TextChanged(object sender, EventArgs e)
        {
            if (tb_BTT.Text != "")
            {
                string text = tb_BTT.Text;

                text = text.Replace("\r", "");
                string[] BD = text.Split('\n');
                dtg_BTT.Rows.Clear();
                int row = 0;
                for (int i = 0; i < BD.Length - 1; i++)
                {
                    string[] _BD = BD[i].Split(new string[] { ":-" }, StringSplitOptions.None); //split code and bd
                    string prefixBD = dictBDPrefix[_BD[0]];
                    string ap = "";
                    string epg = "";
                    if (dictPrefixData.ContainsKey(prefixBD))
                    {/*dictBDData 0 = BD Name , 1 = AP , 2 = EPG*/
                        ap = dictPrefixData[prefixBD][1];
                        epg = dictPrefixData[prefixBD][2];


                        dtg_BTT.Rows.Add(_BD[0], "", ap, epg, prefixBD);
                        if (dictBDSiteCode[_BD[0]][1] == "BBBB")
                        {
                            Image imgStatus = Properties.Resources.normal_icon;
                            DataGridViewImageCell cellStatusImg = new DataGridViewImageCell();
                            cellStatusImg.Value = imgStatus;
                            dtg_BTT.Rows[row].Cells[5] = cellStatusImg;
                        }
                        else
                        {
                            Image imgStatus = Properties.Resources.warning_icon;
                            DataGridViewImageCell cellStatusImg = new DataGridViewImageCell();
                            cellStatusImg.Value = imgStatus;
                            dtg_BTT.Rows[row].Cells[5] = cellStatusImg;
                        }
                        row++;
                    }

                }
                dtg_BTT.Refresh();
            }
            else
            {
                dtg_BTT.Rows.Clear();
                dtg_BTT.Refresh();
            }
        }

        private void Dtg_TST_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            MessageBox.Show(e.RowIndex.ToString());
        }

        private void Dtg_TST_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            string tooltipText = "";
            if (dtg_TST.SelectedRows.Count > 0)
            {
                if (dtg_TST.SelectedRows[0].Cells[0].Value.ToString() != "")
                {
                    var cell = dtg_TST.Rows[e.RowIndex].Cells[e.ColumnIndex];
                    string bd = dtg_TST.Rows[e.RowIndex].Cells[0].Value.ToString();
                    tooltipText = "BD: " + bd + System.Environment.NewLine;
                    tooltipText += "---------------------------------------------- " + System.Environment.NewLine;
                    // tooltipText += "VRF: " + dtg_TST.Rows[e.RowIndex].Cells[1].Value.ToString() + System.Environment.NewLine; 
                    tooltipText += "AP: " + dtg_TST.Rows[e.RowIndex].Cells[2].Value.ToString() + System.Environment.NewLine;
                    tooltipText += "EPG: " + dtg_TST.Rows[e.RowIndex].Cells[3].Value.ToString() + System.Environment.NewLine;
                    string[] code = dictBDSiteCode[bd][1].Select(c => c.ToString()).ToArray();
                    if (code[0] == "T")
                    {
                        tooltipText += "L3Out: " + L3TST + System.Environment.NewLine;
                    }
                    else if (code[0] == "B") { tooltipText += "***L3Out: " + L3TST + System.Environment.NewLine; }
                    else { tooltipText += "***L3Out: -" + System.Environment.NewLine; }

                    if (code[1] == "T")
                    {
                        tooltipText += "VRF: " + vrfTST + System.Environment.NewLine;
                    }
                    else if (code[1] == "B") { tooltipText += "***VRF: " + vrfBTT + System.Environment.NewLine; }
                    else { tooltipText += "***VRF: -" + System.Environment.NewLine; }

                    if (code[2] == "T")
                    {
                        tooltipText += "Provide Contract: " + contractsTST + System.Environment.NewLine;
                    }
                    else if (code[2] == "B") { tooltipText += "***Provide Contract: " + contractsBTT + System.Environment.NewLine; }
                    else { tooltipText += "***Provide Contract: -" + System.Environment.NewLine; }

                    if (code[3] == "T")
                    {
                        tooltipText += "Consume Contract: " + contractsTST + System.Environment.NewLine;
                    }
                    else if (code[3] == "B") { tooltipText += "***Consume Contract: " + contractsBTT + System.Environment.NewLine; }
                    else { tooltipText += "***Consume Contract: -" + System.Environment.NewLine; }
                    cell.ToolTipText = tooltipText;
                }

            }
        }

        private void Dtg_BTT_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            string tooltipText = "";
            if (dtg_BTT.SelectedRows.Count > 0)
            {
                if (dtg_BTT.SelectedRows[0].Cells[0].Value.ToString() != "")
                {
                    var cell = dtg_BTT.Rows[e.RowIndex].Cells[e.ColumnIndex];
                    string bd = dtg_BTT.Rows[e.RowIndex].Cells[0].Value.ToString();
                    tooltipText = "BD: " + bd + System.Environment.NewLine;
                    tooltipText += "---------------------------------------------- " + System.Environment.NewLine;
                    tooltipText += "AP: " + dtg_BTT.Rows[e.RowIndex].Cells[2].Value.ToString() + System.Environment.NewLine; ;
                    tooltipText += "EPG: " + dtg_BTT.Rows[e.RowIndex].Cells[3].Value.ToString() + System.Environment.NewLine;
                    string[] code = dictBDSiteCode[bd][1].Select(c => c.ToString()).ToArray();
                    if (code[0] == "T")
                    {
                        tooltipText += "***L3Out: " + L3TST + System.Environment.NewLine;
                    }
                    else if (code[0] == "B") { tooltipText += "L3Out: " + L3BTT + System.Environment.NewLine; }
                    else { tooltipText += "***L3Out: -" + System.Environment.NewLine; }

                    if (code[1] == "T")
                    {
                        tooltipText += "***VRF: " + vrfTST + System.Environment.NewLine;
                    }
                    else if (code[1] == "B") { tooltipText += "VRF: " + vrfBTT + System.Environment.NewLine; }
                    else { tooltipText += "***VRF: -" + System.Environment.NewLine; }

                    if (code[2] == "T")
                    {
                        tooltipText += "***Provide Contract: " + contractsTST + System.Environment.NewLine;
                    }
                    else if (code[2] == "B") { tooltipText += "Provide Contract: " + contractsBTT + System.Environment.NewLine; }
                    else { tooltipText += "***Provide Contract: -" + System.Environment.NewLine; }

                    if (code[3] == "T")
                    {
                        tooltipText += "***Consume Contract: " + contractsTST + System.Environment.NewLine;
                    }
                    else if (code[3] == "B") { tooltipText += "Consume Contract: " + contractsBTT + System.Environment.NewLine; }
                    else { tooltipText += "***Consume Contract: -" + System.Environment.NewLine; }
                    cell.ToolTipText = tooltipText;
                }

            }
        }

        private void SystemSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Frm_systemSetting frm_systemSetting = new Frm_systemSetting();
            frm_systemSetting.ShowDialog();

            if (frm_systemSetting.isChange)
            {
                MessageBox.Show("มีการแก้ไขข้อมูล System Setting โปรแกรมกำลังทำการ Restart ใหม่อีกครั้ง");
                Application.Restart();
                Environment.Exit(0);
            }
        }

        private void Frm_Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            state = false;
        }

        private void Frm_Main_FormClosing(object sender, FormClosingEventArgs e)
        {

            state = false;
            try
            {
                if (bgw_apicBDUpdate.IsBusy)
                {
                    bgw_apicBDUpdate.CancelAsync();
                }
                if (bgw_monitorPrefix.IsBusy)
                {
                    bgw_monitorPrefix.CancelAsync();
                }
                if (bgw_sladin1Snmp.IsBusy)
                {
                    bgw_sladin1Snmp.CancelAsync();
                }
                if (bgw_sladin2Snmp.IsBusy)
                {

                    bgw_sladin2Snmp.CancelAsync();
                }
                if (bgw_sniper1Snmp.IsBusy)
                {
                    bgw_sniper1Snmp.CancelAsync();
                }
                if (bgw_sniper2Snmp.IsBusy)
                {
                    bgw_sniper2Snmp.CancelAsync();
                }
            }
            catch { }

        }

        public string AdminState(string pod, string node, string ift)
        {
            Console.WriteLine("adminstate-----" + pod + " " + node + " " + ift);
            string state = "";
            try
            {
                RestClient client = new RestClient("https://" + apicIP + "/api/aaaLogin.json");
                client.Timeout = 2000;
                client.CookieContainer = new System.Net.CookieContainer();
                var request = new RestRequest(Method.POST);
                ServicePointManager.ServerCertificateValidationCallback += (RestClient, certificate, chain, sslPolicyErrors) => true;
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("content-type", "application/json");
                request.AddParameter("application/json", "{\r\n  \"aaaUser\" : {\r\n    \"attributes\" : {\r\n      \"name\" : \"" + apicUsername + "\",\r\n  " +
                    "    \"pwd\" : \"" + apicPassword + "\"\r\n    }\r\n  }\r\n}", ParameterType.RequestBody);
                var response = client.Execute(request);
                if (response.ErrorException != null)
                {
                }
                else
                {
                    Console.WriteLine(response.Content);
                    //var serviceTicket = deserializer.Deserialize<ServiceTicket>(response).response;
                    client.BaseUrl = new System.Uri("https://" + apicIP + "/api/node/mo/topology/" + pod + "/node-" + node + "/sys/phys-[" + ift + "].json?query-target=children&target-subtree-class=ethpmPhysIf&rsp-subtree=no");
                    var request1 = new RestRequest(Method.GET);
                    ServicePointManager.ServerCertificateValidationCallback += (RestRequest, certificate, chain, sslPolicyErrors) => true;
                    request1.AddHeader("cache-control", "no-cache");
                    IRestResponse response1 = client.Execute(request1);
                    JObject datastat = JObject.Parse(response1.Content);
                    //Console.WriteLine(datastat);
                    state = (datastat["imdata"][0]["ethpmPhysIf"]["attributes"]["operSt"].ToString());
                    Console.WriteLine("state" + state);
                }
            }
            catch (Exception)
            {

            }
            //string state = "";
            ////var serviceTicket = deserializer.Deserialize<ServiceTicket>(response).response;
            //apicClient.BaseUrl = new System.Uri("https://" + apicIP + "/api/node/mo/topology/" + pod + "/node-" + node + "/sys/phys-[" + ift + "].json");
            //var request1 = new RestRequest(Method.GET);
            //ServicePointManager.ServerCertificateValidationCallback += (RestRequest, certificate, chain, sslPolicyErrors) => true;
            //request1.AddHeader("cache-control", "no-cache");
            //IRestResponse response1 = apicClient.Execute(request1);
            //JObject datastat = JObject.Parse(response1.Content);
            //state = (datastat["imdata"][0]["l1PhysIf"]["attributes"]["adminSt"].ToString());
            //Console.WriteLine("state" + state);
            return state;
        }
    }
}