﻿using System;
using log4net;
using System.Globalization;

namespace N7K_MonitorPrefix
{
    class Log { 
 
        private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static void log(string strLogType, string strMessage)
        {
            strLogType = strLogType.ToUpper();
            if (strLogType == "DEBUG")
            {
                logger.Debug(strMessage);
            }
            else if (strLogType == "WARN")
            {
                logger.Warn(strMessage);
            }
            else if (strLogType == "ERROR")
            {
                logger.Error(strMessage);
            }
            else if (strLogType == "FATAL")
            {
                logger.Fatal(strMessage);
            }
            else
            {
                logger.Info(strMessage);
            }
        }

        public static string GetLastUpdateTime()
        {
            return DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", new CultureInfo("en-US"));
        }


  
}
}
