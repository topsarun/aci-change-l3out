﻿namespace N7K_MonitorPrefix
{
    partial class Frm_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Main));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.addPrefixToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.systemSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lb_tst = new System.Windows.Forms.Label();
            this.img_sladin01 = new System.Windows.Forms.PictureBox();
            this.dtg_TST = new System.Windows.Forms.DataGridView();
            this.img_sladin02 = new System.Windows.Forms.PictureBox();
            this.img_sniper02 = new System.Windows.Forms.PictureBox();
            this.img_sniper01 = new System.Windows.Forms.PictureBox();
            this.dtg_BTT = new System.Windows.Forms.DataGridView();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.dataGridView4 = new System.Windows.Forms.DataGridView();
            this.tb_sladin1Snmp = new System.Windows.Forms.TextBox();
            this.tb_sladin2Snmp = new System.Windows.Forms.TextBox();
            this.tb_sniper1Snmp = new System.Windows.Forms.TextBox();
            this.tb_sniper2Snmp = new System.Windows.Forms.TextBox();
            this.bgw_sladin1Snmp = new System.ComponentModel.BackgroundWorker();
            this.bgw_sladin2Snmp = new System.ComponentModel.BackgroundWorker();
            this.bgw_sniper1Snmp = new System.ComponentModel.BackgroundWorker();
            this.bgw_sniper2Snmp = new System.ComponentModel.BackgroundWorker();
            this.timerStart = new System.Windows.Forms.Timer(this.components);
            this.tb_sladin01Connect = new System.Windows.Forms.TextBox();
            this.tb_sladin02Connect = new System.Windows.Forms.TextBox();
            this.tb_sniper01Connect = new System.Windows.Forms.TextBox();
            this.tb_sniper02Connect = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.tb_adminstatus_saladin = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.tb_saladin01PingConnect = new System.Windows.Forms.TextBox();
            this.panel11 = new System.Windows.Forms.Panel();
            this.tb_saladin02PingConnect = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_adminSaladin01_1 = new System.Windows.Forms.Button();
            this.btn_adminSaladin01_2 = new System.Windows.Forms.Button();
            this.btn_adminSaladin02_1 = new System.Windows.Forms.Button();
            this.btn_adminSaladin02_2 = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.tb_adminstatus_sniper = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.tb_sniper01PingConnect = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.tb_sniper02PingConnect = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_adminSniper01_1 = new System.Windows.Forms.Button();
            this.btn_adminSniper01_2 = new System.Windows.Forms.Button();
            this.btn_adminSniper02_1 = new System.Windows.Forms.Button();
            this.btn_adminSniper02_2 = new System.Windows.Forms.Button();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.bgw_apicBDUpdate = new System.ComponentModel.BackgroundWorker();
            this.bgw_loginApic = new System.ComponentModel.BackgroundWorker();
            this.label5 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.lb_StatusUpdate = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.bt_moveBTTtoTST = new System.Windows.Forms.Button();
            this.bt_moveTSTtoBTT = new System.Windows.Forms.Button();
            this.bgw_monitorPrefix = new System.ComponentModel.BackgroundWorker();
            this.dtg_prefixList = new System.Windows.Forms.DataGridView();
            this.label4 = new System.Windows.Forms.Label();
            this.tb_TST = new System.Windows.Forms.TextBox();
            this.tb_BTT = new System.Windows.Forms.TextBox();
            this.bgw_autoMoveL3out = new System.ComponentModel.BackgroundWorker();
            this.bgw_sladin1Ping = new System.ComponentModel.BackgroundWorker();
            this.bgw_sladin2Ping = new System.ComponentModel.BackgroundWorker();
            this.bgw_sniper1Ping = new System.ComponentModel.BackgroundWorker();
            this.bgw_sniper2Ping = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorker2 = new System.ComponentModel.BackgroundWorker();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.errorProvider2 = new System.Windows.Forms.ErrorProvider(this.components);
            this.errorProvider3 = new System.Windows.Forms.ErrorProvider(this.components);
            this.errorProvider4 = new System.Windows.Forms.ErrorProvider(this.components);
            this.bgw_adminstate = new System.ComponentModel.BackgroundWorker();
            this.errorProvider5 = new System.Windows.Forms.ErrorProvider(this.components);
            this.errorProvider6 = new System.Windows.Forms.ErrorProvider(this.components);
            this.errorProvider7 = new System.Windows.Forms.ErrorProvider(this.components);
            this.errorProvider8 = new System.Windows.Forms.ErrorProvider(this.components);
            this.errorProvider9 = new System.Windows.Forms.ErrorProvider(this.components);
            this.errorProvider10 = new System.Windows.Forms.ErrorProvider(this.components);
            this.errorProvider11 = new System.Windows.Forms.ErrorProvider(this.components);
            this.errorProvider12 = new System.Windows.Forms.ErrorProvider(this.components);
            this.errorProvider13 = new System.Windows.Forms.ErrorProvider(this.components);
            this.errorProvider14 = new System.Windows.Forms.ErrorProvider(this.components);
            this.errorProvider15 = new System.Windows.Forms.ErrorProvider(this.components);
            this.errorProvider16 = new System.Windows.Forms.ErrorProvider(this.components);
            this.errorProvider17 = new System.Windows.Forms.ErrorProvider(this.components);
            this.errorProvider18 = new System.Windows.Forms.ErrorProvider(this.components);
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.img_sladin01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtg_TST)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.img_sladin02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.img_sniper02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.img_sniper01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtg_BTT)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).BeginInit();
            this.panel4.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtg_prefixList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripDropDownButton1});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1339, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addPrefixToolStripMenuItem,
            this.systemSettingsToolStripMenuItem});
            this.toolStripDropDownButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton1.Image")));
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(58, 22);
            this.toolStripDropDownButton1.Text = "System";
            // 
            // addPrefixToolStripMenuItem
            // 
            this.addPrefixToolStripMenuItem.Name = "addPrefixToolStripMenuItem";
            this.addPrefixToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.addPrefixToolStripMenuItem.Text = "Add Prefix...";
            this.addPrefixToolStripMenuItem.Click += new System.EventHandler(this.AddPrefixToolStripMenuItem_Click);
            // 
            // systemSettingsToolStripMenuItem
            // 
            this.systemSettingsToolStripMenuItem.Name = "systemSettingsToolStripMenuItem";
            this.systemSettingsToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.systemSettingsToolStripMenuItem.Text = "System Settings";
            this.systemSettingsToolStripMenuItem.Click += new System.EventHandler(this.SystemSettingsToolStripMenuItem_Click);
            // 
            // lb_tst
            // 
            this.lb_tst.AutoSize = true;
            this.lb_tst.BackColor = System.Drawing.Color.Ivory;
            this.lb_tst.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_tst.Location = new System.Drawing.Point(57, 4);
            this.lb_tst.Name = "lb_tst";
            this.lb_tst.Size = new System.Drawing.Size(85, 21);
            this.lb_tst.TabIndex = 3;
            this.lb_tst.Text = "SLADIN 01";
            // 
            // img_sladin01
            // 
            this.img_sladin01.BackColor = System.Drawing.Color.Ivory;
            this.img_sladin01.Image = global::N7K_MonitorPrefix.Properties.Resources.router_grey_small;
            this.img_sladin01.Location = new System.Drawing.Point(67, 28);
            this.img_sladin01.Name = "img_sladin01";
            this.img_sladin01.Size = new System.Drawing.Size(65, 46);
            this.img_sladin01.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.img_sladin01.TabIndex = 14;
            this.img_sladin01.TabStop = false;
            // 
            // dtg_TST
            // 
            this.dtg_TST.AllowUserToAddRows = false;
            this.dtg_TST.AllowUserToDeleteRows = false;
            this.dtg_TST.AllowUserToResizeColumns = false;
            this.dtg_TST.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtg_TST.BackgroundColor = System.Drawing.Color.MintCream;
            this.dtg_TST.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dtg_TST.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.Color.DarkSlateGray;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtg_TST.DefaultCellStyle = dataGridViewCellStyle17;
            this.dtg_TST.Location = new System.Drawing.Point(522, 37);
            this.dtg_TST.Name = "dtg_TST";
            this.dtg_TST.RowHeadersVisible = false;
            this.dtg_TST.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtg_TST.Size = new System.Drawing.Size(460, 379);
            this.dtg_TST.TabIndex = 18;
            this.dtg_TST.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.Dtg_TST_CellFormatting);
            // 
            // img_sladin02
            // 
            this.img_sladin02.BackColor = System.Drawing.Color.Ivory;
            this.img_sladin02.Image = global::N7K_MonitorPrefix.Properties.Resources.router_grey_small;
            this.img_sladin02.Location = new System.Drawing.Point(66, 28);
            this.img_sladin02.Name = "img_sladin02";
            this.img_sladin02.Size = new System.Drawing.Size(65, 46);
            this.img_sladin02.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.img_sladin02.TabIndex = 20;
            this.img_sladin02.TabStop = false;
            // 
            // img_sniper02
            // 
            this.img_sniper02.Image = global::N7K_MonitorPrefix.Properties.Resources.router_grey_small;
            this.img_sniper02.Location = new System.Drawing.Point(60, 29);
            this.img_sniper02.Name = "img_sniper02";
            this.img_sniper02.Size = new System.Drawing.Size(68, 46);
            this.img_sniper02.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.img_sniper02.TabIndex = 21;
            this.img_sniper02.TabStop = false;
            // 
            // img_sniper01
            // 
            this.img_sniper01.Image = global::N7K_MonitorPrefix.Properties.Resources.router_grey_small;
            this.img_sniper01.Location = new System.Drawing.Point(52, 29);
            this.img_sniper01.Name = "img_sniper01";
            this.img_sniper01.Size = new System.Drawing.Size(68, 46);
            this.img_sniper01.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.img_sniper01.TabIndex = 22;
            this.img_sniper01.TabStop = false;
            // 
            // dtg_BTT
            // 
            this.dtg_BTT.AllowUserToAddRows = false;
            this.dtg_BTT.AllowUserToDeleteRows = false;
            this.dtg_BTT.AllowUserToResizeColumns = false;
            this.dtg_BTT.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtg_BTT.BackgroundColor = System.Drawing.Color.MintCream;
            this.dtg_BTT.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dtg_BTT.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle18.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.Color.DarkSlateBlue;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtg_BTT.DefaultCellStyle = dataGridViewCellStyle18;
            this.dtg_BTT.Location = new System.Drawing.Point(11, 37);
            this.dtg_BTT.Name = "dtg_BTT";
            this.dtg_BTT.RowHeadersVisible = false;
            this.dtg_BTT.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtg_BTT.Size = new System.Drawing.Size(460, 379);
            this.dtg_BTT.TabIndex = 19;
            this.dtg_BTT.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.Dtg_BTT_CellFormatting);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.LightSlateGray;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.label6);
            this.panel3.Location = new System.Drawing.Point(11, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(460, 31);
            this.panel3.TabIndex = 24;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label6.Location = new System.Drawing.Point(160, 6);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(107, 21);
            this.label6.TabIndex = 0;
            this.label6.Text = "L3Out <BTT>";
            // 
            // dataGridView3
            // 
            this.dataGridView3.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dataGridView3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.GridColor = System.Drawing.SystemColors.ControlLight;
            this.dataGridView3.Location = new System.Drawing.Point(1013, 28);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.Size = new System.Drawing.Size(331, 687);
            this.dataGridView3.TabIndex = 25;
            // 
            // dataGridView4
            // 
            this.dataGridView4.BackgroundColor = System.Drawing.SystemColors.ButtonShadow;
            this.dataGridView4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView4.Location = new System.Drawing.Point(1065, 28);
            this.dataGridView4.Name = "dataGridView4";
            this.dataGridView4.Size = new System.Drawing.Size(3, 656);
            this.dataGridView4.TabIndex = 26;
            this.dataGridView4.Visible = false;
            // 
            // tb_sladin1Snmp
            // 
            this.tb_sladin1Snmp.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tb_sladin1Snmp.Location = new System.Drawing.Point(1176, 400);
            this.tb_sladin1Snmp.Multiline = true;
            this.tb_sladin1Snmp.Name = "tb_sladin1Snmp";
            this.tb_sladin1Snmp.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tb_sladin1Snmp.Size = new System.Drawing.Size(156, 107);
            this.tb_sladin1Snmp.TabIndex = 27;
            this.tb_sladin1Snmp.Visible = false;
            this.tb_sladin1Snmp.TextChanged += new System.EventHandler(this.tb_sladin1Snmp_TextChanged);
            // 
            // tb_sladin2Snmp
            // 
            this.tb_sladin2Snmp.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tb_sladin2Snmp.Location = new System.Drawing.Point(1024, 513);
            this.tb_sladin2Snmp.Multiline = true;
            this.tb_sladin2Snmp.Name = "tb_sladin2Snmp";
            this.tb_sladin2Snmp.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tb_sladin2Snmp.Size = new System.Drawing.Size(156, 107);
            this.tb_sladin2Snmp.TabIndex = 28;
            this.tb_sladin2Snmp.Visible = false;
            this.tb_sladin2Snmp.TextChanged += new System.EventHandler(this.tb_sladin2Snmp_TextChanged);
            // 
            // tb_sniper1Snmp
            // 
            this.tb_sniper1Snmp.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tb_sniper1Snmp.Location = new System.Drawing.Point(1026, 400);
            this.tb_sniper1Snmp.Multiline = true;
            this.tb_sniper1Snmp.Name = "tb_sniper1Snmp";
            this.tb_sniper1Snmp.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tb_sniper1Snmp.Size = new System.Drawing.Size(156, 107);
            this.tb_sniper1Snmp.TabIndex = 29;
            this.tb_sniper1Snmp.Visible = false;
            this.tb_sniper1Snmp.TextChanged += new System.EventHandler(this.tb_sniper1Snmp_TextChanged);
            // 
            // tb_sniper2Snmp
            // 
            this.tb_sniper2Snmp.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tb_sniper2Snmp.Location = new System.Drawing.Point(1181, 513);
            this.tb_sniper2Snmp.Multiline = true;
            this.tb_sniper2Snmp.Name = "tb_sniper2Snmp";
            this.tb_sniper2Snmp.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tb_sniper2Snmp.Size = new System.Drawing.Size(158, 107);
            this.tb_sniper2Snmp.TabIndex = 30;
            this.tb_sniper2Snmp.Visible = false;
            this.tb_sniper2Snmp.TextChanged += new System.EventHandler(this.tb_sniper2Snmp_TextChanged);
            // 
            // bgw_sladin1Snmp
            // 
            this.bgw_sladin1Snmp.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgw_sladin1Snmp_DoWork);
            // 
            // bgw_sladin2Snmp
            // 
            this.bgw_sladin2Snmp.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgw_sladin2Snmp_DoWork);
            // 
            // bgw_sniper1Snmp
            // 
            this.bgw_sniper1Snmp.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgw_sniper1Snmp_DoWork);
            // 
            // bgw_sniper2Snmp
            // 
            this.bgw_sniper2Snmp.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgw_sniper2Snmp_DoWork);
            // 
            // timerStart
            // 
            this.timerStart.Interval = 500;
            this.timerStart.Tick += new System.EventHandler(this.timerStart_Tick);
            // 
            // tb_sladin01Connect
            // 
            this.tb_sladin01Connect.BackColor = System.Drawing.Color.Ivory;
            this.tb_sladin01Connect.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_sladin01Connect.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_sladin01Connect.Location = new System.Drawing.Point(40, 80);
            this.tb_sladin01Connect.Multiline = true;
            this.tb_sladin01Connect.Name = "tb_sladin01Connect";
            this.tb_sladin01Connect.Size = new System.Drawing.Size(116, 32);
            this.tb_sladin01Connect.TabIndex = 31;
            this.tb_sladin01Connect.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_sladin01Connect.TextChanged += new System.EventHandler(this.Tb_sladinConnect_TextChanged);
            // 
            // tb_sladin02Connect
            // 
            this.tb_sladin02Connect.BackColor = System.Drawing.Color.Ivory;
            this.tb_sladin02Connect.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_sladin02Connect.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_sladin02Connect.Location = new System.Drawing.Point(37, 80);
            this.tb_sladin02Connect.Multiline = true;
            this.tb_sladin02Connect.Name = "tb_sladin02Connect";
            this.tb_sladin02Connect.Size = new System.Drawing.Size(116, 32);
            this.tb_sladin02Connect.TabIndex = 32;
            this.tb_sladin02Connect.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_sladin02Connect.TextChanged += new System.EventHandler(this.Tb_sladin02Connect_TextChanged);
            // 
            // tb_sniper01Connect
            // 
            this.tb_sniper01Connect.BackColor = System.Drawing.Color.Ivory;
            this.tb_sniper01Connect.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_sniper01Connect.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_sniper01Connect.Location = new System.Drawing.Point(32, 81);
            this.tb_sniper01Connect.Multiline = true;
            this.tb_sniper01Connect.Name = "tb_sniper01Connect";
            this.tb_sniper01Connect.Size = new System.Drawing.Size(116, 32);
            this.tb_sniper01Connect.TabIndex = 33;
            this.tb_sniper01Connect.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_sniper01Connect.TextChanged += new System.EventHandler(this.Tb_sniper01Connect_TextChanged);
            // 
            // tb_sniper02Connect
            // 
            this.tb_sniper02Connect.BackColor = System.Drawing.Color.Ivory;
            this.tb_sniper02Connect.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_sniper02Connect.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_sniper02Connect.Location = new System.Drawing.Point(40, 81);
            this.tb_sniper02Connect.Multiline = true;
            this.tb_sniper02Connect.Name = "tb_sniper02Connect";
            this.tb_sniper02Connect.Size = new System.Drawing.Size(116, 32);
            this.tb_sniper02Connect.TabIndex = 34;
            this.tb_sniper02Connect.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_sniper02Connect.TextChanged += new System.EventHandler(this.Tb_sniper02Connect_TextChanged);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Cornsilk;
            this.panel4.Controls.Add(this.tb_adminstatus_saladin);
            this.panel4.Controls.Add(this.panel10);
            this.panel4.Controls.Add(this.panel11);
            this.panel4.Controls.Add(this.btn_adminSaladin01_1);
            this.panel4.Controls.Add(this.btn_adminSaladin01_2);
            this.panel4.Controls.Add(this.btn_adminSaladin02_1);
            this.panel4.Controls.Add(this.btn_adminSaladin02_2);
            this.panel4.Location = new System.Drawing.Point(503, 60);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(496, 197);
            this.panel4.TabIndex = 35;
            // 
            // tb_adminstatus_saladin
            // 
            this.tb_adminstatus_saladin.AutoSize = true;
            this.tb_adminstatus_saladin.Location = new System.Drawing.Point(202, 167);
            this.tb_adminstatus_saladin.Name = "tb_adminstatus_saladin";
            this.tb_adminstatus_saladin.Size = new System.Drawing.Size(91, 13);
            this.tb_adminstatus_saladin.TabIndex = 43;
            this.tb_adminstatus_saladin.Text = "<< Oper status >>";
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.Ivory;
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel10.Controls.Add(this.tb_saladin01PingConnect);
            this.panel10.Controls.Add(this.img_sladin01);
            this.panel10.Controls.Add(this.tb_sladin01Connect);
            this.panel10.Controls.Add(this.lb_tst);
            this.panel10.Location = new System.Drawing.Point(47, 5);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(200, 147);
            this.panel10.TabIndex = 38;
            // 
            // tb_saladin01PingConnect
            // 
            this.tb_saladin01PingConnect.BackColor = System.Drawing.Color.Ivory;
            this.tb_saladin01PingConnect.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_saladin01PingConnect.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_saladin01PingConnect.Location = new System.Drawing.Point(40, 114);
            this.tb_saladin01PingConnect.Multiline = true;
            this.tb_saladin01PingConnect.Name = "tb_saladin01PingConnect";
            this.tb_saladin01PingConnect.Size = new System.Drawing.Size(116, 32);
            this.tb_saladin01PingConnect.TabIndex = 35;
            this.tb_saladin01PingConnect.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_saladin01PingConnect.TextChanged += new System.EventHandler(this.Tb_saladin01PingConnect_TextChanged);
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.Ivory;
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel11.Controls.Add(this.tb_saladin02PingConnect);
            this.panel11.Controls.Add(this.tb_sladin02Connect);
            this.panel11.Controls.Add(this.img_sladin02);
            this.panel11.Controls.Add(this.label1);
            this.panel11.Location = new System.Drawing.Point(253, 5);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(200, 147);
            this.panel11.TabIndex = 39;
            // 
            // tb_saladin02PingConnect
            // 
            this.tb_saladin02PingConnect.BackColor = System.Drawing.Color.Ivory;
            this.tb_saladin02PingConnect.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_saladin02PingConnect.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_saladin02PingConnect.Location = new System.Drawing.Point(37, 114);
            this.tb_saladin02PingConnect.Multiline = true;
            this.tb_saladin02PingConnect.Name = "tb_saladin02PingConnect";
            this.tb_saladin02PingConnect.Size = new System.Drawing.Size(116, 32);
            this.tb_saladin02PingConnect.TabIndex = 36;
            this.tb_saladin02PingConnect.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_saladin02PingConnect.TextChanged += new System.EventHandler(this.Tb_saladin02PingConnect_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Ivory;
            this.label1.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(53, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 21);
            this.label1.TabIndex = 33;
            this.label1.Text = "SLADIN 02";
            // 
            // btn_adminSaladin01_1
            // 
            this.btn_adminSaladin01_1.Location = new System.Drawing.Point(109, 150);
            this.btn_adminSaladin01_1.Margin = new System.Windows.Forms.Padding(0);
            this.btn_adminSaladin01_1.Name = "btn_adminSaladin01_1";
            this.btn_adminSaladin01_1.Size = new System.Drawing.Size(19, 47);
            this.btn_adminSaladin01_1.TabIndex = 43;
            this.btn_adminSaladin01_1.UseVisualStyleBackColor = true;
            // 
            // btn_adminSaladin01_2
            // 
            this.btn_adminSaladin01_2.Location = new System.Drawing.Point(161, 150);
            this.btn_adminSaladin01_2.Margin = new System.Windows.Forms.Padding(0);
            this.btn_adminSaladin01_2.Name = "btn_adminSaladin01_2";
            this.btn_adminSaladin01_2.Size = new System.Drawing.Size(19, 47);
            this.btn_adminSaladin01_2.TabIndex = 44;
            this.btn_adminSaladin01_2.UseVisualStyleBackColor = true;
            // 
            // btn_adminSaladin02_1
            // 
            this.btn_adminSaladin02_1.Location = new System.Drawing.Point(320, 150);
            this.btn_adminSaladin02_1.Margin = new System.Windows.Forms.Padding(0);
            this.btn_adminSaladin02_1.Name = "btn_adminSaladin02_1";
            this.btn_adminSaladin02_1.Size = new System.Drawing.Size(19, 47);
            this.btn_adminSaladin02_1.TabIndex = 45;
            this.btn_adminSaladin02_1.UseVisualStyleBackColor = true;
            // 
            // btn_adminSaladin02_2
            // 
            this.btn_adminSaladin02_2.Location = new System.Drawing.Point(366, 150);
            this.btn_adminSaladin02_2.Margin = new System.Windows.Forms.Padding(0);
            this.btn_adminSaladin02_2.Name = "btn_adminSaladin02_2";
            this.btn_adminSaladin02_2.Size = new System.Drawing.Size(19, 47);
            this.btn_adminSaladin02_2.TabIndex = 46;
            this.btn_adminSaladin02_2.UseVisualStyleBackColor = true;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Cornsilk;
            this.panel5.Controls.Add(this.tb_adminstatus_sniper);
            this.panel5.Controls.Add(this.panel7);
            this.panel5.Controls.Add(this.panel8);
            this.panel5.Controls.Add(this.btn_adminSniper01_1);
            this.panel5.Controls.Add(this.btn_adminSniper01_2);
            this.panel5.Controls.Add(this.btn_adminSniper02_1);
            this.panel5.Controls.Add(this.btn_adminSniper02_2);
            this.panel5.Location = new System.Drawing.Point(9, 60);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(496, 197);
            this.panel5.TabIndex = 36;
            // 
            // tb_adminstatus_sniper
            // 
            this.tb_adminstatus_sniper.AutoSize = true;
            this.tb_adminstatus_sniper.Location = new System.Drawing.Point(173, 167);
            this.tb_adminstatus_sniper.Name = "tb_adminstatus_sniper";
            this.tb_adminstatus_sniper.Size = new System.Drawing.Size(88, 13);
            this.tb_adminstatus_sniper.TabIndex = 42;
            this.tb_adminstatus_sniper.Text = "<<Oper status >>";
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.Ivory;
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.tb_sniper01PingConnect);
            this.panel7.Controls.Add(this.img_sniper01);
            this.panel7.Controls.Add(this.tb_sniper01Connect);
            this.panel7.Controls.Add(this.label3);
            this.panel7.Location = new System.Drawing.Point(24, 5);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(200, 147);
            this.panel7.TabIndex = 36;
            // 
            // tb_sniper01PingConnect
            // 
            this.tb_sniper01PingConnect.BackColor = System.Drawing.Color.Ivory;
            this.tb_sniper01PingConnect.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_sniper01PingConnect.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_sniper01PingConnect.Location = new System.Drawing.Point(32, 115);
            this.tb_sniper01PingConnect.Multiline = true;
            this.tb_sniper01PingConnect.Name = "tb_sniper01PingConnect";
            this.tb_sniper01PingConnect.Size = new System.Drawing.Size(116, 32);
            this.tb_sniper01PingConnect.TabIndex = 34;
            this.tb_sniper01PingConnect.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_sniper01PingConnect.TextChanged += new System.EventHandler(this.Tb_sniper01PingConnect_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Ivory;
            this.label3.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(48, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 21);
            this.label3.TabIndex = 34;
            this.label3.Text = "SNIPER 01";
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.Ivory;
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.tb_sniper02PingConnect);
            this.panel8.Controls.Add(this.img_sniper02);
            this.panel8.Controls.Add(this.tb_sniper02Connect);
            this.panel8.Controls.Add(this.label2);
            this.panel8.Location = new System.Drawing.Point(230, 5);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(200, 147);
            this.panel8.TabIndex = 37;
            // 
            // tb_sniper02PingConnect
            // 
            this.tb_sniper02PingConnect.BackColor = System.Drawing.Color.Ivory;
            this.tb_sniper02PingConnect.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_sniper02PingConnect.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_sniper02PingConnect.Location = new System.Drawing.Point(40, 115);
            this.tb_sniper02PingConnect.Multiline = true;
            this.tb_sniper02PingConnect.Name = "tb_sniper02PingConnect";
            this.tb_sniper02PingConnect.Size = new System.Drawing.Size(116, 32);
            this.tb_sniper02PingConnect.TabIndex = 35;
            this.tb_sniper02PingConnect.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_sniper02PingConnect.TextChanged += new System.EventHandler(this.Tb_sniper02PingConnect_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Ivory;
            this.label2.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(56, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 21);
            this.label2.TabIndex = 35;
            this.label2.Text = "SNIPER 02";
            // 
            // btn_adminSniper01_1
            // 
            this.btn_adminSniper01_1.Location = new System.Drawing.Point(76, 150);
            this.btn_adminSniper01_1.Margin = new System.Windows.Forms.Padding(0);
            this.btn_adminSniper01_1.Name = "btn_adminSniper01_1";
            this.btn_adminSniper01_1.Size = new System.Drawing.Size(19, 47);
            this.btn_adminSniper01_1.TabIndex = 38;
            this.btn_adminSniper01_1.UseVisualStyleBackColor = true;
            // 
            // btn_adminSniper01_2
            // 
            this.btn_adminSniper01_2.Location = new System.Drawing.Point(125, 150);
            this.btn_adminSniper01_2.Margin = new System.Windows.Forms.Padding(0);
            this.btn_adminSniper01_2.Name = "btn_adminSniper01_2";
            this.btn_adminSniper01_2.Size = new System.Drawing.Size(19, 47);
            this.btn_adminSniper01_2.TabIndex = 39;
            this.btn_adminSniper01_2.UseVisualStyleBackColor = true;
            // 
            // btn_adminSniper02_1
            // 
            this.btn_adminSniper02_1.Location = new System.Drawing.Point(288, 150);
            this.btn_adminSniper02_1.Margin = new System.Windows.Forms.Padding(0);
            this.btn_adminSniper02_1.Name = "btn_adminSniper02_1";
            this.btn_adminSniper02_1.Size = new System.Drawing.Size(19, 47);
            this.btn_adminSniper02_1.TabIndex = 40;
            this.btn_adminSniper02_1.UseVisualStyleBackColor = true;
            // 
            // btn_adminSniper02_2
            // 
            this.btn_adminSniper02_2.Location = new System.Drawing.Point(336, 150);
            this.btn_adminSniper02_2.Margin = new System.Windows.Forms.Padding(0);
            this.btn_adminSniper02_2.Name = "btn_adminSniper02_2";
            this.btn_adminSniper02_2.Size = new System.Drawing.Size(19, 47);
            this.btn_adminSniper02_2.TabIndex = 41;
            this.btn_adminSniper02_2.UseVisualStyleBackColor = true;
            // 
            // bgw_apicBDUpdate
            // 
            this.bgw_apicBDUpdate.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgw_apicBDUpdate_DoWork);
            // 
            // bgw_loginApic
            // 
            this.bgw_loginApic.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgw_loginApic_DoWork);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label5.Location = new System.Drawing.Point(189, 6);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(107, 21);
            this.label5.TabIndex = 0;
            this.label5.Text = "L3Out <TST>";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.LightSlateGray;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.label5);
            this.panel2.Location = new System.Drawing.Point(522, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(460, 31);
            this.panel2.TabIndex = 24;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DimGray;
            this.panel1.Controls.Add(this.panel6);
            this.panel1.Controls.Add(this.bt_moveBTTtoTST);
            this.panel1.Controls.Add(this.bt_moveTSTtoBTT);
            this.panel1.Controls.Add(this.dtg_BTT);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.dtg_TST);
            this.panel1.Location = new System.Drawing.Point(9, 253);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(993, 462);
            this.panel1.TabIndex = 37;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.White;
            this.panel6.Controls.Add(this.lb_StatusUpdate);
            this.panel6.Controls.Add(this.progressBar1);
            this.panel6.Location = new System.Drawing.Point(15, 428);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(962, 28);
            this.panel6.TabIndex = 26;
            // 
            // lb_StatusUpdate
            // 
            this.lb_StatusUpdate.AutoSize = true;
            this.lb_StatusUpdate.Location = new System.Drawing.Point(205, 6);
            this.lb_StatusUpdate.Name = "lb_StatusUpdate";
            this.lb_StatusUpdate.Size = new System.Drawing.Size(0, 13);
            this.lb_StatusUpdate.TabIndex = 26;
            // 
            // progressBar1
            // 
            this.progressBar1.BackColor = System.Drawing.Color.White;
            this.progressBar1.ForeColor = System.Drawing.Color.MediumTurquoise;
            this.progressBar1.Location = new System.Drawing.Point(9, 6);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(185, 17);
            this.progressBar1.TabIndex = 25;
            // 
            // bt_moveBTTtoTST
            // 
            this.bt_moveBTTtoTST.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.bt_moveBTTtoTST.ForeColor = System.Drawing.Color.White;
            this.bt_moveBTTtoTST.Location = new System.Drawing.Point(477, 136);
            this.bt_moveBTTtoTST.Name = "bt_moveBTTtoTST";
            this.bt_moveBTTtoTST.Size = new System.Drawing.Size(37, 35);
            this.bt_moveBTTtoTST.TabIndex = 1;
            this.bt_moveBTTtoTST.Text = ">>>";
            this.bt_moveBTTtoTST.UseVisualStyleBackColor = false;
            this.bt_moveBTTtoTST.Click += new System.EventHandler(this.Bt_moveBTTtoTST_Click);
            // 
            // bt_moveTSTtoBTT
            // 
            this.bt_moveTSTtoBTT.BackColor = System.Drawing.Color.DarkSlateGray;
            this.bt_moveTSTtoBTT.ForeColor = System.Drawing.Color.White;
            this.bt_moveTSTtoBTT.Location = new System.Drawing.Point(477, 196);
            this.bt_moveTSTtoBTT.Name = "bt_moveTSTtoBTT";
            this.bt_moveTSTtoBTT.Size = new System.Drawing.Size(37, 35);
            this.bt_moveTSTtoBTT.TabIndex = 0;
            this.bt_moveTSTtoBTT.Text = "<<<";
            this.bt_moveTSTtoBTT.UseVisualStyleBackColor = false;
            this.bt_moveTSTtoBTT.Click += new System.EventHandler(this.Bt_moveTSTtoBTT_Click);
            // 
            // bgw_monitorPrefix
            // 
            this.bgw_monitorPrefix.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgw_monitorPrefix_DoWork);
            // 
            // dtg_prefixList
            // 
            this.dtg_prefixList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtg_prefixList.BackgroundColor = System.Drawing.Color.White;
            this.dtg_prefixList.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dtg_prefixList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle19.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle19.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtg_prefixList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle19;
            this.dtg_prefixList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle20.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle20.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle20.Padding = new System.Windows.Forms.Padding(1, 1, 0, 0);
            dataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle20.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtg_prefixList.DefaultCellStyle = dataGridViewCellStyle20;
            this.dtg_prefixList.Location = new System.Drawing.Point(1022, 37);
            this.dtg_prefixList.Name = "dtg_prefixList";
            this.dtg_prefixList.ReadOnly = true;
            this.dtg_prefixList.RowHeadersVisible = false;
            this.dtg_prefixList.Size = new System.Drawing.Size(310, 599);
            this.dtg_prefixList.TabIndex = 38;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Gainsboro;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(22, 37);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(107, 20);
            this.label4.TabIndex = 34;
            this.label4.Text = "Monitor N7K";
            // 
            // tb_TST
            // 
            this.tb_TST.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tb_TST.Location = new System.Drawing.Point(1024, 626);
            this.tb_TST.Multiline = true;
            this.tb_TST.Name = "tb_TST";
            this.tb_TST.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tb_TST.Size = new System.Drawing.Size(156, 75);
            this.tb_TST.TabIndex = 39;
            this.tb_TST.Visible = false;
            this.tb_TST.TextChanged += new System.EventHandler(this.Tb_TST_TextChanged);
            // 
            // tb_BTT
            // 
            this.tb_BTT.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tb_BTT.Location = new System.Drawing.Point(1181, 626);
            this.tb_BTT.Multiline = true;
            this.tb_BTT.Name = "tb_BTT";
            this.tb_BTT.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tb_BTT.Size = new System.Drawing.Size(156, 75);
            this.tb_BTT.TabIndex = 40;
            this.tb_BTT.Visible = false;
            this.tb_BTT.TextChanged += new System.EventHandler(this.Tb_BTT_TextChanged);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // errorProvider2
            // 
            this.errorProvider2.ContainerControl = this;
            // 
            // errorProvider3
            // 
            this.errorProvider3.ContainerControl = this;
            // 
            // errorProvider4
            // 
            this.errorProvider4.ContainerControl = this;
            // 
            // bgw_adminstate
            // 
            this.bgw_adminstate.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgw_adminstate_DoWork);
            // 
            // errorProvider5
            // 
            this.errorProvider5.ContainerControl = this;
            // 
            // errorProvider6
            // 
            this.errorProvider6.ContainerControl = this;
            // 
            // errorProvider7
            // 
            this.errorProvider7.ContainerControl = this;
            // 
            // errorProvider8
            // 
            this.errorProvider8.ContainerControl = this;
            // 
            // errorProvider9
            // 
            this.errorProvider9.ContainerControl = this;
            // 
            // errorProvider10
            // 
            this.errorProvider10.ContainerControl = this;
            // 
            // errorProvider11
            // 
            this.errorProvider11.ContainerControl = this;
            // 
            // errorProvider12
            // 
            this.errorProvider12.ContainerControl = this;
            // 
            // errorProvider13
            // 
            this.errorProvider13.ContainerControl = this;
            // 
            // errorProvider14
            // 
            this.errorProvider14.ContainerControl = this;
            // 
            // errorProvider15
            // 
            this.errorProvider15.ContainerControl = this;
            // 
            // errorProvider16
            // 
            this.errorProvider16.ContainerControl = this;
            // 
            // errorProvider17
            // 
            this.errorProvider17.ContainerControl = this;
            // 
            // errorProvider18
            // 
            this.errorProvider18.ContainerControl = this;
            // 
            // Frm_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(1339, 713);
            this.Controls.Add(this.tb_BTT);
            this.Controls.Add(this.tb_TST);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.dtg_prefixList);
            this.Controls.Add(this.tb_sniper2Snmp);
            this.Controls.Add(this.tb_sniper1Snmp);
            this.Controls.Add(this.tb_sladin2Snmp);
            this.Controls.Add(this.tb_sladin1Snmp);
            this.Controls.Add(this.dataGridView4);
            this.Controls.Add(this.dataGridView3);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Name = "Frm_Main";
            this.Text = "CLOUD 4 NINE";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Frm_Main_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Frm_Main_FormClosed);
            this.Load += new System.EventHandler(this.Frm_Main_Load);
            this.Shown += new System.EventHandler(this.Frm_Main_Shown);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.img_sladin01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtg_TST)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.img_sladin02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.img_sniper02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.img_sniper01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtg_BTT)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtg_prefixList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem addPrefixToolStripMenuItem;
        private System.Windows.Forms.Label lb_tst;
        private System.Windows.Forms.PictureBox img_sladin01;
        private System.Windows.Forms.DataGridView dtg_TST;
        private System.Windows.Forms.PictureBox img_sladin02;
        private System.Windows.Forms.PictureBox img_sniper02;
        private System.Windows.Forms.PictureBox img_sniper01;
        private System.Windows.Forms.DataGridView dtg_BTT;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.DataGridView dataGridView4;
        private System.Windows.Forms.TextBox tb_sladin1Snmp;
        private System.Windows.Forms.TextBox tb_sladin2Snmp;
        private System.Windows.Forms.TextBox tb_sniper1Snmp;
        private System.Windows.Forms.TextBox tb_sniper2Snmp;
        private System.ComponentModel.BackgroundWorker bgw_sladin1Snmp;
        private System.ComponentModel.BackgroundWorker bgw_sladin2Snmp;
        private System.ComponentModel.BackgroundWorker bgw_sniper1Snmp;
        private System.ComponentModel.BackgroundWorker bgw_sniper2Snmp;
        private System.Windows.Forms.Timer timerStart;
        private System.Windows.Forms.TextBox tb_sladin01Connect;
        private System.Windows.Forms.TextBox tb_sladin02Connect;
        private System.Windows.Forms.TextBox tb_sniper01Connect;
        private System.Windows.Forms.TextBox tb_sniper02Connect;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.ComponentModel.BackgroundWorker bgw_apicBDUpdate;
        private System.ComponentModel.BackgroundWorker bgw_loginApic;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.ComponentModel.BackgroundWorker bgw_monitorPrefix;
        private System.Windows.Forms.DataGridView dtg_prefixList;
        private System.Windows.Forms.Button bt_moveBTTtoTST;
        private System.Windows.Forms.Button bt_moveTSTtoBTT;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label lb_StatusUpdate;
        private System.Windows.Forms.TextBox tb_TST;
        private System.Windows.Forms.TextBox tb_BTT;
        private System.Windows.Forms.ToolStripMenuItem systemSettingsToolStripMenuItem;
        private System.ComponentModel.BackgroundWorker bgw_autoMoveL3out;
        private System.ComponentModel.BackgroundWorker bgw_sladin1Ping;
        private System.ComponentModel.BackgroundWorker bgw_sladin2Ping;
        private System.ComponentModel.BackgroundWorker bgw_sniper1Ping;
        private System.ComponentModel.BackgroundWorker bgw_sniper2Ping;
        private System.ComponentModel.BackgroundWorker backgroundWorker2;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.ErrorProvider errorProvider2;
        private System.Windows.Forms.TextBox tb_sniper01PingConnect;
        private System.Windows.Forms.TextBox tb_sniper02PingConnect;
        private System.Windows.Forms.ErrorProvider errorProvider3;
        private System.Windows.Forms.ErrorProvider errorProvider4;
        private System.ComponentModel.BackgroundWorker bgw_adminstate;
        private System.Windows.Forms.Button btn_adminSniper02_2;
        private System.Windows.Forms.Button btn_adminSniper02_1;
        private System.Windows.Forms.Button btn_adminSniper01_2;
        private System.Windows.Forms.Button btn_adminSniper01_1;
        private System.Windows.Forms.Label tb_adminstatus_sniper;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Button btn_adminSaladin02_2;
        private System.Windows.Forms.Button btn_adminSaladin02_1;
        private System.Windows.Forms.Button btn_adminSaladin01_2;
        private System.Windows.Forms.Button btn_adminSaladin01_1;
        private System.Windows.Forms.Label tb_adminstatus_saladin;
        private System.Windows.Forms.TextBox tb_saladin01PingConnect;
        private System.Windows.Forms.TextBox tb_saladin02PingConnect;
        private System.Windows.Forms.ErrorProvider errorProvider5;
        private System.Windows.Forms.ErrorProvider errorProvider6;
        private System.Windows.Forms.ErrorProvider errorProvider7;
        private System.Windows.Forms.ErrorProvider errorProvider8;
        private System.Windows.Forms.ErrorProvider errorProvider9;
        private System.Windows.Forms.ErrorProvider errorProvider10;
        private System.Windows.Forms.ErrorProvider errorProvider11;
        private System.Windows.Forms.ErrorProvider errorProvider12;
        private System.Windows.Forms.ErrorProvider errorProvider13;
        private System.Windows.Forms.ErrorProvider errorProvider14;
        private System.Windows.Forms.ErrorProvider errorProvider15;
        private System.Windows.Forms.ErrorProvider errorProvider16;
        private System.Windows.Forms.ErrorProvider errorProvider17;
        private System.Windows.Forms.ErrorProvider errorProvider18;
        private System.Windows.Forms.BindingSource bindingSource1;
    }
}

