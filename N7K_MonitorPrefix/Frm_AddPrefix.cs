﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
namespace N7K_MonitorPrefix
{
    public partial class Frm_AddPrefix : Form
    {
        public Frm_AddPrefix()
        {
            InitializeComponent();
        }

        public void AddXmlPrefix(string CustomerName, string Prefix)
        {
            DataSet dsXML = new DataSet();
            dsXML.ReadXml(Application.StartupPath + @"\PrefixList.xml");
            if (dsXML.Tables.Count < 1)
            {
                dsXML.Tables.Add("Prefix");
                dsXML.Tables["Prefix"].Columns.Add("CustomerName");
                dsXML.Tables["Prefix"].Columns.Add("PrefixID");
                dsXML.Tables["Prefix"].Rows.Add(CustomerName, Prefix);
                dsXML.WriteXml(Application.StartupPath + @"\PrefixList.xml");
            }
            else
            {
               
                dsXML.Tables["Prefix"].Rows.Add(CustomerName, Prefix);

                dsXML.WriteXml(Application.StartupPath + @"\PrefixList.xml");
            }
        }
        private void Frm_AddPrefix_Load(object sender, EventArgs e)
        {

        }

        private void bt_add_Click(object sender, EventArgs e)
        {
            if (tb_custName.Text == null || tb_prefix.Text == null)
            {
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบ!");
            }
            else {
                IPAddress _ip = null;
                if (tb_prefix.Text.Contains('/'))
                {
                    string[] ip = tb_prefix.Text.Split('/');
                    if (ip.GetLength(0) == 2)
                    {
                        if (IPAddress.TryParse(ip[0], out _ip) && (Convert.ToInt32(ip[1]) > 0 && Convert.ToInt32(ip[1]) <= 32))
                        {
                            AddXmlPrefix(tb_custName.Text, tb_prefix.Text);
                            this.Close();

                        }
                        else
                        {
                            MessageBox.Show("กรุณากรอก Prefix ให้ถูกต้อง!");
                        }
                    }
                    else { MessageBox.Show("กรุณากรอก Prefix ให้ถูกต้อง!"); }
                }
                else
                {
                    MessageBox.Show("กรุณากรอก Prefix ให้ถูกต้อง!");
                }
            }
        }
    }
}
